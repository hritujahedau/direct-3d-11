#pragma once
#include <windows.h>


#define MYICON		1287
#define IDI_MYICON MAKEINTRESOURCE(MYICON)

#define KUNDALI		1288
#define ID_KUNDALI	MAKEINTRESOURCE(KUNDALI)

#define STONE		1289
#define ID_STONE	MAKEINTRESOURCE(STONE)

typedef struct tagBITMAPFILEHEADER BITMAPFILEHEADER;

BITMAPFILEHEADER* ReadBMFileHeader(FILE* fp);
BITMAPINFOHEADER* ReadBMInfoHeader(FILE* fp);
BITMAPCOREHEADER* ReadBMCoreHeader(FILE* fp);

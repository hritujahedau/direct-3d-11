// header file declaration
#include <windows.h>
#include <stdio.h>
#include <math.h>

#include "d3d.h"
#include <d3d11.h>
#include <d3dcompiler.h>

#pragma warning(disable: 4838)
#include "XNAMath/xnamath.h"
#include "WICTextureLoader.h"

#pragma comment(lib , "d3d11.lib")
#pragma comment(lib , "dxgi.lib")
#pragma comment(lib, "D3dcompiler.lib")
#pragma comment(lib , "DirectXTK.lib")

#define WIN_WIDTH 1000
#define WIN_HEIGHT 800

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int width, hight;

float gClearColor[4]; // RGBA
IDXGISwapChain* gpIDXGISwapChain = NULL;
ID3D11Device* gpID3D11Device = NULL;
ID3D11DeviceContext* gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView* gpID3D11RenderTargetView = NULL;


ID3D11Buffer* gpID3D11Buffer_Cube_VertexBuffer_position = NULL;
ID3D11Buffer* gpID3D11Buffer_Cube_VertexBuffer_color = NULL;
ID3D11Buffer* gpID3D11Buffer_Cube_VertexBuffer_texcoord = NULL;
ID3D11Buffer* gpID3D11Buffer_Cube_VertexBuffer_normals = NULL;

ID3D11InputLayout* gpID3D11InputLayout = NULL;
ID3D11Buffer* gpID3D11BufferConstantBuffer = NULL;
ID3D11RasterizerState* gpID3D11RasterizerState = NULL;
ID3D11DepthStencilView* gpID3D11DepthStencilView = NULL;

struct CBUFFER_WATER
{
	XMMATRIX WorldMatrix;			//XMMATRIX - xna math matrix 
	XMMATRIX ViewMatrix;
	XMMATRIX ProjectionMatrix;
	
	XMVECTOR LightAmbient;
	XMVECTOR LightDiffuse;
	XMVECTOR LightSpecular;
	XMVECTOR LightPosition;

	XMVECTOR MaterialAmbient;
	XMVECTOR MaterialDiffuse;
	XMVECTOR MaterialSpecular;
	float MaterialShininess;
	XMVECTOR cameraPosition;
	float moveFactor;
};

XMMATRIX gPerspectiveProjectionMatrix;

IDXGIFactory* pIDXGIFactory = NULL;
IDXGIAdapter* pIDXGIAdapter = NULL;

DXGI_ADAPTER_DESC dxgiAdapterDesc;
CHAR str[255];

bool isColor, isTexture, isLight;

ID3D11ShaderResourceView* gpID3D11ShaderResourceView_texture_marble = NULL;
ID3D11SamplerState* gpID3D11SamplerStateWaterTexture = NULL;

float LightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
float LightDiffuse[] = { 0.5f, 0.5f, 0.5f, 1.0f };
float LightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
float LightPosition[] = { 100.0f, 100.0f , -100.0f, 1.0f };

float MaterialAmbient[] = { 0.0f, 0.0f, 0.0f };
float MaterialDiffuse[] = { 1.0f, 1.0f, 1.0f };
float MaterialSpecular[] = { 1.0f, 1.0f, 1.0f };
float MaterialShininess = 128.0f;

D3D11_INPUT_ELEMENT_DESC inputElementDesc[4];

float rotateAngle = 0.0f;

// FBO
// variables for render to texture
ID3D11RenderTargetView* gpID3D11RenderTargetView_fbo_reflection = NULL;
ID3D11ShaderResourceView* gpID3D11ShaderResourceView_fbo_reflection_ColorTexture = NULL;

ID3D11RenderTargetView* gpID3D11RenderTargetView_fbo_refraction = NULL;
ID3D11ShaderResourceView* gpID3D11ShaderResourceView_fbo_refraction_ColorTexture = NULL;

ID3D11VertexShader* gpID3D11VertexShader_fbo = NULL;
ID3D11PixelShader* gpID3D11PixelShader_fbo = NULL;
ID3DBlob* pID3DBlob_VertexShaderCode_fbo = NULL;			//irrespective version

// water
ID3D11VertexShader* gpID3D11VertexShader_water = NULL;
ID3D11PixelShader* gpID3D11PixelShader_water = NULL;
ID3DBlob* pID3DBlob_VertexShaderCode_water = NULL;			//irrespective version

ID3D11ShaderResourceView* gpID3D11ShaderResourceView_texture_dudvMap = NULL;
ID3D11ShaderResourceView* gpID3D11ShaderResourceView_texture_normal = NULL;

float gMoveFactor = 0.0f;
float wave_speed = 0.03f;

float cameraPosition[3];

// Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) 
{
	// function declaration
	HRESULT Initialize();
	void Uninitialize();
	void Display();
	void Update();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("h_direct3D11");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 
	if (fopen_s(&gpFile, "windows.log", "w") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("Direct3D11 Window"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIN_WIDTH/2 ,
		monitorRC.bottom / 2 - WIN_HEIGHT/2 ,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	HRESULT hr;
	hr = Initialize();
	if (FAILED(hr))
	{
		fprintf(gpFile, "\ninitialize() failed.Exiting Now..\n");
		fclose(gpFile);
		DestroyWindow(ghwnd);
		hwnd = NULL;
	}

	// game loop
	while (gbDone == false) 
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 
		{
			if (msg.message == WM_QUIT) 
			{
				gbDone = true;
			}
			else 
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else 
		{
			Display();
			Update();
			if (gbActiveWindow) 
			{
				if (gbEscapeKeyIsPressed == true)
				{
					gbDone = true;
				}
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	HRESULT Resize(int,int);

	HRESULT hr;

	// code
	switch (uMsg) {
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return 0L;
	case WM_SIZE:
		if (gpID3D11DeviceContext)
		{
			hr = Resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(hr))
			{
				fprintf(gpFile, "\nResize() failed");
				fflush(gpFile);
				DestroyWindow(ghwnd);
				return hr;
			}
			else
			{
				fprintf(gpFile, "\nResize() successfull");
				fflush(gpFile);
			}
		}
		break;
		case WM_CHAR:
			switch (wParam)
			{
			case 'c':
			case 'C':
				isColor = !isColor;
				break;
			case 't':
			case 'T':
				isTexture = !isTexture;
				break;
			case 'l':
			case 'L':
				isLight = !isLight;
				break;
			}
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				if (gbEscapeKeyIsPressed == false)
				{
					gbEscapeKeyIsPressed = true;
				}
				//DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

HRESULT Initialize() {
	// function declaration
	HRESULT Resize(int, int);
	HRESULT LoadD3DTexture(const wchar_t*, ID3D11ShaderResourceView**);
	HRESULT RenderToTargetShader(ID3D11Device * , ID3DBlob ** , ID3D11VertexShader **, ID3D11PixelShader ** );
	HRESULT waterShader(ID3D11Device *, ID3DBlob **, ID3D11VertexShader **, ID3D11PixelShader **);
	void Uninitialize();

	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE, D3D_DRIVER_TYPE_WARP, D3D_DRIVER_TYPE_REFERENCE
	};

	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0; // default, lowest

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;	//based upon d3dFeatureLevel_required

	// code
	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]); // calculation size of array
	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;

	ZeroMemory((void*)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	dxgiSwapChainDesc.BufferCount = 1;
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd;
	// MSAA - Multi Sampling Ani Alizing 
	dxgiSwapChainDesc.SampleDesc.Count = 1; // Max 8 2^8 sampling
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,								// adapter
			d3dDriverType,						// Driver Type
			NULL,								// Software
			createDeviceFlags,					// Flags
			&d3dFeatureLevel_required,			// Feature Levels
			numFeatureLevels,					// Num Feature Levels
			D3D11_SDK_VERSION,					// sdk verstion
			&dxgiSwapChainDesc,					// Swap Chain Desc
			&gpIDXGISwapChain,					// Swap chain
			&gpID3D11Device,					// Device
			&d3dFeatureLevel_acquired,			// Feature Level
			&gpID3D11DeviceContext				// device conext
		);

		if (SUCCEEDED(hr))
		{
			break;
		}
	}

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nD3D11CretaeDeviceAndSwapChain() Failed.");
		fflush(gpFile);
		//DestroyWindow(ghwnd);
		return hr;
	}
	else
	{
		fprintf(gpFile, "\nD3D11CreateDeviceAndSwapChain() Successded");
		fprintf(gpFile, "\nThe chosen driver is of");
		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf(gpFile, "\nHardware type.");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf(gpFile, "\nWarp type");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf(gpFile, "\nUnkown type");
		}

		fprintf(gpFile, "\nThe supported highest feature level is ");
		if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf(gpFile, "11.0");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf(gpFile, "10.1");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf(gpFile, "10.0");
		}
		else
		{
			fprintf(gpFile, "Unknown version");
		}
		fflush(gpFile);
	} // else closed

	hr = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&pIDXGIFactory);
	if (FAILED(hr))
	{
		fprintf(gpFile, "\nFailed to get pIDXGIFactory interface");
		fflush(gpFile);
		return hr;
	}

	if (pIDXGIFactory->EnumAdapters(1, &pIDXGIAdapter) == DXGI_ERROR_NOT_FOUND)
	{
		fprintf(gpFile, "\nFailed to get dxgi adapter");
		fflush(gpFile);
		return hr;
	}

	memset((void**)&dxgiAdapterDesc, 0, sizeof(DXGI_ADAPTER_DESC));

	hr = pIDXGIAdapter->GetDesc(&dxgiAdapterDesc);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\ndxgiAdapterDesc Failed to get");
		fflush(gpFile);
		return hr;
	}

	WideCharToMultiByte(CP_ACP, 0, dxgiAdapterDesc.Description, 255, str, 255, NULL, NULL);

	fprintf(gpFile, "\nGraphics card name is %s", str);
	fprintf(gpFile, "\nVideo Memory Size of graphics card is %d gb", int(ceil(dxgiAdapterDesc.DedicatedVideoMemory / 1024.0 / 1024.0 / 1024.0)));
	fflush(gpFile);
		

	hr = RenderToTargetShader(gpID3D11Device, &pID3DBlob_VertexShaderCode_fbo, &gpID3D11VertexShader_fbo, &gpID3D11PixelShader_fbo);
	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::RenderToTargetShader() Failed.");
		fflush(gpFile);
		return hr;
	}

	hr = waterShader(gpID3D11Device, &pID3DBlob_VertexShaderCode_water, &gpID3D11VertexShader_water, &gpID3D11PixelShader_water);
	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::waterShader() Failed.");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nID3D11Device::waterShader() Successfull.");
	fflush(gpFile);

	// create and set input layout
	// 1st
	ZeroMemory((void*)&inputElementDesc, sizeof(D3D11_INPUT_ELEMENT_DESC));

	inputElementDesc[0].SemanticName = "POSITION";
	inputElementDesc[0].SemanticIndex = 0;
	inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[0].InputSlot = 0;
	inputElementDesc[0].AlignedByteOffset = 0;
	inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[0].InstanceDataStepRate = 0;

	// 2nd

	inputElementDesc[1].SemanticName = "COLOR";
	inputElementDesc[1].SemanticIndex = 0;
	inputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[1].InputSlot = 1;
	inputElementDesc[1].AlignedByteOffset = 0;
	inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[1].InstanceDataStepRate = 0;

	inputElementDesc[2].SemanticName = "TEXCOORD";
	inputElementDesc[2].SemanticIndex = 0;
	inputElementDesc[2].Format = DXGI_FORMAT_R32G32_FLOAT;
	inputElementDesc[2].InputSlot = 2;
	inputElementDesc[2].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;  // glPixelStorei unpack align
	inputElementDesc[2].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[2].InstanceDataStepRate = 0;

	inputElementDesc[3].SemanticName = "NORMAL";
	inputElementDesc[3].SemanticIndex = 0;
	inputElementDesc[3].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[3].InputSlot = 3;
	inputElementDesc[3].AlignedByteOffset = 0;
	inputElementDesc[3].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[3].InstanceDataStepRate = 0;
	
	/////////////////////////////////////////////// RECTANGLE ////////////////////////////////////////////////////

	float cubeVertices[] =
	{
		-1.0f, +1.0f, +1.0f,
		+1.0f, +1.0f, +1.0f,
		-1.0f, +1.0f, -1.0f,

		-1.0f, +1.0f, -1.0f,
		+1.0f, +1.0f, +1.0f,
		+1.0f, +1.0f, -1.0f,

		// SIDE 2 ( BOTTOM )
		+1.0f, -1.0f, -1.0f,
		+1.0f, -1.0f, +1.0f,
		-1.0f, -1.0f, -1.0f,

		-1.0f, -1.0f, -1.0f,
		+1.0f, -1.0f, +1.0f,
		-1.0f, -1.0f, +1.0f,

		// SIDE 3 ( FRONT )
		-1.0f, +1.0f, -1.0f,
		+1.0f, +1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,

		-1.0f, -1.0f, -1.0f,
		+1.0f, +1.0f, -1.0f,
		+1.0f, -1.0f, -1.0f,

		// SIDE 4 ( BACK )
		+1.0f, -1.0f, +1.0f,
		+1.0f, +1.0f, +1.0f,
		-1.0f, -1.0f, +1.0f,

		-1.0f, -1.0f, +1.0f,
		+1.0f, +1.0f, +1.0f,
		-1.0f, +1.0f, +1.0f,

		// SIDE 5 ( LEFT )
		-1.0f, +1.0f, +1.0f,
		-1.0f, +1.0f, -1.0f,
		-1.0f, -1.0f, +1.0f,

		-1.0f, -1.0f, +1.0f,
		-1.0f, +1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,

		// SIDE 6 ( RIGHT )
		+1.0f, -1.0f, -1.0f,
		+1.0f, +1.0f, -1.0f,
		+1.0f, -1.0f, +1.0f,

		+1.0f, -1.0f, +1.0f,
		+1.0f, +1.0f, -1.0f,
		+1.0f, +1.0f, +1.0f,
	};

	float cubeColor[] =
	{
		+1.0f, +0.0f, +0.0f,
		+1.0f, +0.0f, +0.0f,
		+1.0f, +0.0f, +0.0f,

		+1.0f, +0.0f, +0.0f,
		+1.0f, +0.0f, +0.0f,
		+1.0f, +0.0f, +0.0f,


		+0.0f, +1.0f, +0.0f,
		+0.0f, +1.0f, +0.0f,
		+0.0f, +1.0f, +0.0f,

		+0.0f, +1.0f, +0.0f,
		+0.0f, +1.0f, +0.0f,
		+0.0f, +1.0f, +0.0f,


		+0.0f, +0.0f, +1.0f,
		+0.0f, +0.0f, +1.0f,
		+0.0f, +0.0f, +1.0f,

		+0.0f, +0.0f, +1.0f,
		+0.0f, +0.0f, +1.0f,
		+0.0f, +0.0f, +1.0f,


		+0.0f, +1.0f, +1.0f,
		+0.0f, +1.0f, +1.0f,
		+0.0f, +1.0f, +1.0f,

		+0.0f, +1.0f, +1.0f,
		+0.0f, +1.0f, +1.0f,
		+0.0f, +1.0f, +1.0f,


		+1.0f, +0.0f, +1.0f,
		+1.0f, +0.0f, +1.0f,
		+1.0f, +0.0f, +1.0f,

		+1.0f, +0.0f, +1.0f,
		+1.0f, +0.0f, +1.0f,
		+1.0f, +0.0f, +1.0f,


		+1.0f, +1.0f, +0.0f,
		+1.0f, +1.0f, +0.0f,
		+1.0f, +1.0f, +0.0f,

		+1.0f, +1.0f, +0.0f,
		+1.0f, +1.0f, +0.0f,
		+1.0f, +1.0f, +0.0f,
	};

	float cubeTexcoord[] =
	{
		+0.0f, +0.0f,
		+0.0f, +1.0f,
		+1.0f, +0.0f,

		+1.0f, +0.0f,
		+0.0f, +1.0f,
		+1.0f, +1.0f,


		+0.0f, +0.0f,
		+0.0f, +1.0f,
		+1.0f, +0.0f,

		+1.0f, +0.0f,
		+0.0f, +1.0f,
		+1.0f, +1.0f,


		+0.0f, +0.0f,
		+0.0f, +1.0f,
		+1.0f, +0.0f,

		+1.0f, +0.0f,
		+0.0f, +1.0f,
		+1.0f, +1.0f,


		+0.0f, +0.0f,
		+0.0f, +1.0f,
		+1.0f, +0.0f,

		+1.0f, +0.0f,
		+0.0f, +1.0f,
		+1.0f, +1.0f,


		+0.0f, +0.0f,
		+0.0f, +1.0f,
		+1.0f, +0.0f,

		+1.0f, +0.0f,
		+0.0f, +1.0f,
		+1.0f, +1.0f,


		+0.0f, +0.0f,
		+0.0f, +1.0f,
		+1.0f, +0.0f,

		+1.0f, +0.0f,
		+0.0f, +1.0f,
		+1.0f, +1.0f,
	};

	float cubeNormals[] =
	{
		+0.0f, +1.0f, +0.0f,
		+0.0f, +1.0f, +0.0f,
		+0.0f, +1.0f, +0.0f,

		+0.0f, +1.0f, +0.0f,
		+0.0f, +1.0f, +0.0f,
		+0.0f, +1.0f, +0.0f,


		+0.0f, -1.0f, +0.0f,
		+0.0f, -1.0f, +0.0f,
		+0.0f, -1.0f, +0.0f,

		+0.0f, -1.0f, +0.0f,
		+0.0f, -1.0f, +0.0f,
		+0.0f, -1.0f, +0.0f,


		+0.0f, +0.0f, -1.0f,
		+0.0f, +0.0f, -1.0f,
		+0.0f, +0.0f, -1.0f,

		+0.0f, +0.0f, -1.0f,
		+0.0f, +0.0f, -1.0f,
		+0.0f, +0.0f, -1.0f,


		+0.0f, +0.0f, +1.0f,
		+0.0f, +0.0f, +1.0f,
		+0.0f, +0.0f, +1.0f,

		+0.0f, +0.0f, +1.0f,
		+0.0f, +0.0f, +1.0f,
		+0.0f, +0.0f, +1.0f,


		-1.0f, +0.0f, +0.0f,
		-1.0f, +0.0f, +0.0f,
		-1.0f, +0.0f, +0.0f,

		-1.0f, +0.0f, +0.0f,
		-1.0f, +0.0f, +0.0f,
		-1.0f, +0.0f, +0.0f,


		+1.0f, +0.0f, +0.0f,
		+1.0f, +0.0f, +0.0f,
		+1.0f, +0.0f, +0.0f,

		+1.0f, +0.0f, +0.0f,
		+1.0f, +0.0f, +0.0f,
		+1.0f, +0.0f, +0.0f,
	};

	D3D11_BUFFER_DESC bufferDesc;
	ZeroMemory((void*)&bufferDesc, sizeof(D3D11_BUFFER_DESC));
	
	bufferDesc.ByteWidth = sizeof(float) * _ARRAYSIZE(cubeVertices);
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11Buffer_Cube_VertexBuffer_position);
	
	if (FAILED(hr))
	{
		fprintf(gpFile, "\n CreateBuffer() for rectangle position failed");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\n CretaeBuffer() successfull for rectangle position");
	fflush(gpFile);

	ZeroMemory((void*)&bufferDesc, sizeof(D3D11_BUFFER_DESC));

	bufferDesc.ByteWidth = sizeof(float) * _ARRAYSIZE(cubeColor);
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11Buffer_Cube_VertexBuffer_color);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\n CreateBuffer() for rectangle color failed");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\n CretaeBuffer() successfull for rectangle color");
	fflush(gpFile);

	ZeroMemory((void*)&bufferDesc, sizeof(D3D11_BUFFER_DESC));

	bufferDesc.ByteWidth = sizeof(float) * _ARRAYSIZE(cubeTexcoord);
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11Buffer_Cube_VertexBuffer_texcoord);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\n CreateBuffer() for rectangle texcoord failed");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\n CretaeBuffer() successfull for rectangle texcoord");
	fflush(gpFile);

	ZeroMemory((void*)&bufferDesc, sizeof(D3D11_BUFFER_DESC));

	// cube normals
	bufferDesc.ByteWidth = sizeof(float) * _ARRAYSIZE(cubeNormals);
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11Buffer_Cube_VertexBuffer_normals);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\n CreateBuffer() for rectangle normals failed");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\n CreateBuffer() for rectangle normals Successfull");
	fflush(gpFile);
	
	D3D11_MAPPED_SUBRESOURCE mappedSubresource;

	// pass rectangle vertices and color to gpu
	ZeroMemory((void*)&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	gpID3D11DeviceContext->Map(gpID3D11Buffer_Cube_VertexBuffer_position, NULL, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, cubeVertices, sizeof(cubeVertices));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_Cube_VertexBuffer_position, NULL);

	ZeroMemory((void*)&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	gpID3D11DeviceContext->Map(gpID3D11Buffer_Cube_VertexBuffer_color, NULL, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, cubeColor, sizeof(cubeColor));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_Cube_VertexBuffer_color, NULL);

	ZeroMemory((void*)&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	gpID3D11DeviceContext->Map(gpID3D11Buffer_Cube_VertexBuffer_texcoord, NULL, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, cubeTexcoord, sizeof(cubeTexcoord));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_Cube_VertexBuffer_texcoord, NULL);

	ZeroMemory((void*)&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	gpID3D11DeviceContext->Map(gpID3D11Buffer_Cube_VertexBuffer_normals, NULL, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, cubeNormals, sizeof(cubeNormals));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_Cube_VertexBuffer_normals, NULL);

	// define and set constant buffer
	// uniform buffer
	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
	ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(D3D11_BUFFER_DESC));
	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER_WATER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	
	hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer, nullptr, &gpID3D11BufferConstantBuffer);
	
	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreateBuffer() Failed For Constant Buffer.");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nID3D11Device::CreateBuffer() Successfull For Constant Buffer.");
	fflush(gpFile);

	gpID3D11DeviceContext->VSSetConstantBuffers(0, 1, &gpID3D11BufferConstantBuffer);
	gpID3D11DeviceContext->PSSetConstantBuffers(0, 1, &gpID3D11BufferConstantBuffer);

	// CREATE AND SET RASTERIZER STATE

	D3D11_RASTERIZER_DESC d3d11_rasterizer_desc;

	ZeroMemory((void*)&d3d11_rasterizer_desc, sizeof(D3D11_RASTERIZER_DESC));

	d3d11_rasterizer_desc.AntialiasedLineEnable = FALSE;
	d3d11_rasterizer_desc.CullMode = D3D11_CULL_NONE;
	d3d11_rasterizer_desc.DepthBias = 0;
	d3d11_rasterizer_desc.DepthBiasClamp = 0.0f;
	d3d11_rasterizer_desc.DepthClipEnable = TRUE;
	d3d11_rasterizer_desc.FillMode = D3D11_FILL_SOLID;
	d3d11_rasterizer_desc.FrontCounterClockwise = FALSE;
	d3d11_rasterizer_desc.MultisampleEnable = FALSE;
	d3d11_rasterizer_desc.ScissorEnable = FALSE;
	d3d11_rasterizer_desc.SlopeScaledDepthBias = 0.0f;

	hr = gpID3D11Device->CreateRasterizerState(&d3d11_rasterizer_desc, &gpID3D11RasterizerState);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\n CreateRasterizerState() Failed");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\n CreateRasterizerState() sucsess");
	fflush(gpFile);

	gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState);

	hr = LoadD3DTexture(L"marble.bmp", &gpID3D11ShaderResourceView_texture_marble);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\n Failed to LoadD3DTexture()");
		fflush(gpFile);
		return hr;
	}

	hr = LoadD3DTexture(L"waterDUDV.bmp", &gpID3D11ShaderResourceView_texture_dudvMap);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\n Failed to LoadD3DTexture()");
		fflush(gpFile);
		return hr;
	}

	hr = LoadD3DTexture(L"normalMap.bmp", &gpID3D11ShaderResourceView_texture_normal);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\n Failed to LoadD3DTexture()");
		fflush(gpFile);
		return hr;
	}

	D3D11_SAMPLER_DESC d3d11SamplerDesc;

	ZeroMemory((void**)&d3d11SamplerDesc, sizeof(D3D11_SAMPLER_DESC));

	d3d11SamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	d3d11SamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP; // s
	d3d11SamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP; // t
	d3d11SamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP; // r

	hr = gpID3D11Device->CreateSamplerState(&d3d11SamplerDesc, &gpID3D11SamplerStateWaterTexture);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nCreateSamplerState() failed");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nCreateSamplerState() successfull");
	fflush(gpFile);

	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 0.0f;
	gClearColor[3] = 1.0f;

	gPerspectiveProjectionMatrix = XMMatrixIdentity();

	hr = Resize(WIN_WIDTH, WIN_HEIGHT);
	if (FAILED(hr))
	{
		fprintf(gpFile, "\nResize() Failed");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nResize successfull");
	fflush(gpFile);

	return S_OK;
}

HRESULT LoadD3DTexture(const wchar_t* textureFileName, ID3D11ShaderResourceView** ppID3D11ShaderResourceView)
{
	// code
	HRESULT hr;

	hr = DirectX::CreateWICTextureFromFile(gpID3D11Device, gpID3D11DeviceContext, textureFileName, NULL, ppID3D11ShaderResourceView);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nFailed to CreateWICTextureFromFile");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nSuccessfull to CreateWICTextureFromFile");
	fflush(gpFile);

	return hr;
}

HRESULT Resize(int width, int height) 
{
	// function declarations
	HRESULT InitializeD3DRenderToTexture(int, int, ID3D11RenderTargetView**, ID3D11ShaderResourceView**);

	HRESULT hr = S_OK;

	if (height == 0)
	{
		height = 1;
	}

	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	gpIDXGISwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

	ID3D11Texture2D* pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*) &pID3D11Texture2D_BackBuffer);

	// again get render target view from d3d11 using above back buffer
	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL, &gpID3D11RenderTargetView);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreateTargetView() Failed.");
		fflush(gpFile);
		return hr;
	}
	else
	{
		fprintf(gpFile, "\nID3D11Device::CreateTargetView() Successded.");
		fflush(gpFile);
	}

	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	// just like RTV ,  DSV also need texture buffer

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	// create and intialize
	D3D11_TEXTURE2D_DESC d3d11_texture2D_desc;

	ZeroMemory((void*)&d3d11_texture2D_desc, sizeof(D3D11_TEXTURE2D_DESC));

	d3d11_texture2D_desc.Width = (UINT) width;
	d3d11_texture2D_desc.Height = (UINT) height;
	d3d11_texture2D_desc.Format = DXGI_FORMAT_D32_FLOAT;
	d3d11_texture2D_desc.Usage = D3D11_USAGE_DEFAULT;
	d3d11_texture2D_desc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	d3d11_texture2D_desc.SampleDesc.Count = 1;	// this can be 1-4 as per needed capacity
	d3d11_texture2D_desc.SampleDesc.Quality = 0; // default
	d3d11_texture2D_desc.ArraySize = 1;
	d3d11_texture2D_desc.MipLevels = 1; // default;
	d3d11_texture2D_desc.CPUAccessFlags = 0; // default
	d3d11_texture2D_desc.MiscFlags = 0;

	// create texture2D as depth buffer
	ID3D11Texture2D* pID3D11Texture2D_depthbuffer;

	hr = gpID3D11Device->CreateTexture2D(&d3d11_texture2D_desc, NULL, &pID3D11Texture2D_depthbuffer);
	if (FAILED(hr))
	{
		fprintf(gpFile, "\n CreateTexture2D failed for depth");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\n CreateTexture2D successfully for depth");
	fflush(gpFile);

	// intialize depth stencil view desc
	D3D11_DEPTH_STENCIL_VIEW_DESC d3d11_depth_stencil_view_desc;

	ZeroMemory((void*)&d3d11_depth_stencil_view_desc, sizeof(d3d11_depth_stencil_view_desc));

	d3d11_depth_stencil_view_desc.Format = DXGI_FORMAT_D32_FLOAT;
	d3d11_depth_stencil_view_desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS; // TEXTURE2D MS - multi sample

	// create dsv
	hr = gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D_depthbuffer, &d3d11_depth_stencil_view_desc, &gpID3D11DepthStencilView);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\n CretaeDepthstencilView() failed");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\n CretaeDepthstencilView() successfully");
	fflush(gpFile);

	// set render target view as render target
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, gpID3D11DepthStencilView);

	// set viewport target view
	D3D11_VIEWPORT d3dViewPort;
	
	ZeroMemory((void*)&d3dViewPort, sizeof(D3D11_VIEWPORT));

	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f; // glClearDepth(1.0)
	
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);

	// intialization for render to texture
	hr = InitializeD3DRenderToTexture(width, height, &gpID3D11RenderTargetView_fbo_reflection, &gpID3D11ShaderResourceView_fbo_reflection_ColorTexture);
	if (FAILED(hr))
	{
		fprintf(gpFile, "\n InitializeD3DRenderToTexture() failed for reflection");
		fflush(gpFile);
		return hr;
	}

	hr = InitializeD3DRenderToTexture(width, height, &gpID3D11RenderTargetView_fbo_refraction, &gpID3D11ShaderResourceView_fbo_refraction_ColorTexture);
	if (FAILED(hr))
	{
		fprintf(gpFile, "\n InitializeD3DRenderToTexture() failed for refraction");
		fflush(gpFile);
		return hr;
	}
	
	// set orthographic matrix
	gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)width / (float)height, 0.1f, 100.0f);

	return hr;
}

HRESULT InitializeD3DRenderToTexture(int width, int height, ID3D11RenderTargetView** ppID3D11RenderTargetView, ID3D11ShaderResourceView** ppID3D11ShaderResourceView)
{
	// function declarations

	// varaible declarations
	HRESULT hr;
	D3D11_TEXTURE2D_DESC d3d11Texture2DDesc;
	ID3D11Texture2D* renderTargetTexture = NULL;
	D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;

	// code
	fprintf(gpFile, "\nEntering from LoadD3DTexture");
	fflush(gpFile);

	ZeroMemory((void**)&d3d11Texture2DDesc, sizeof(D3D11_TEXTURE2D_DESC));

	fprintf(gpFile, "\nZero Memory to d3d11Texture2DDesc Successfull");
	fflush(gpFile);

	d3d11Texture2DDesc.Width = (UINT)width;
	d3d11Texture2DDesc.Height = (UINT)height;
	d3d11Texture2DDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	d3d11Texture2DDesc.Usage = D3D11_USAGE_DEFAULT;
	d3d11Texture2DDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	d3d11Texture2DDesc.SampleDesc.Count = 1;
	d3d11Texture2DDesc.SampleDesc.Quality = 0;
	d3d11Texture2DDesc.ArraySize = 1;
	d3d11Texture2DDesc.MipLevels = 1;
	d3d11Texture2DDesc.CPUAccessFlags = 0;
	d3d11Texture2DDesc.MiscFlags = 0;

	fprintf(gpFile, "\nd3d11Texture2DDesc filled Successfull");
	fflush(gpFile);


	hr = gpID3D11Device->CreateTexture2D(&d3d11Texture2DDesc, NULL, &renderTargetTexture);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nFailed to CreateTexture2D");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nSuccessfull to CreateTexture2D");
	fflush(gpFile);

	// Setup the description of the render target view.
	renderTargetViewDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	renderTargetViewDesc.Texture2D.MipSlice = 0;

	hr = gpID3D11Device->CreateRenderTargetView(renderTargetTexture, &renderTargetViewDesc, ppID3D11RenderTargetView);
	if (FAILED(hr))
	{
		fprintf(gpFile, "\nFailed to CreateRenderTargetView");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nSuccessfull to CreateRenderTargetView");
	fflush(gpFile);

	D3D11_SHADER_RESOURCE_VIEW_DESC d3d11ShaderResouceViewDesc;
	ZeroMemory((void**)&d3d11ShaderResouceViewDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC));
	d3d11ShaderResouceViewDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	d3d11ShaderResouceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	d3d11ShaderResouceViewDesc.Texture2D.MostDetailedMip = 0;
	d3d11ShaderResouceViewDesc.Texture2D.MipLevels = 1;

	hr = gpID3D11Device->CreateShaderResourceView(renderTargetTexture, &d3d11ShaderResouceViewDesc, ppID3D11ShaderResourceView);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nFailed to CreateShaderResourceView");
		fflush(gpFile);
		renderTargetTexture->Release();
		renderTargetTexture = NULL;
		return hr;
	}

	fprintf(gpFile, "\nSuccessfull to CreateShaderResourceView");
	fflush(gpFile);

	renderTargetTexture->Release();
	renderTargetTexture = NULL;

	fprintf(gpFile, "\nExiting from LoadD3DTexture");
	fflush(gpFile);

	return hr;
}


void Display()
{
	// function declarations
	void RendertToTexture(ID3D11ShaderResourceView*);
	void RenderNormalScene(ID3D11ShaderResourceView*, ID3D11ShaderResourceView*);

	// code
	RendertToTexture(gpID3D11ShaderResourceView_texture_marble);
	RenderNormalScene(gpID3D11ShaderResourceView_fbo_reflection_ColorTexture, gpID3D11ShaderResourceView_fbo_refraction_ColorTexture);
	
	gpIDXGISwapChain->Present(0, 0);
}

void RendertToTexture(ID3D11ShaderResourceView* texture)
{
	// Function declaration
	void RenderCubeWithTexture(ID3D11ShaderResourceView*);

	// Code
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView_fbo_reflection, gpID3D11DepthStencilView);
	gClearColor[0] = 0.0;
	gClearColor[1] = 0.5;
	gClearColor[2] = 0.5;
	gClearColor[3] = 1.0;
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView_fbo_reflection, gClearColor);
	gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, D3D11_CLEAR_DEPTH);

	RenderCubeWithTexture(texture);

	// refraction
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView_fbo_refraction, gpID3D11DepthStencilView);
	gClearColor[0] = 0.0;
	gClearColor[1] = 0.5;
	gClearColor[2] = 0.5;
	gClearColor[3] = 1.0;
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView_fbo_refraction, gClearColor);
	gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, D3D11_CLEAR_DEPTH);

	RenderCubeWithTexture(texture);
}

void RenderNormalScene(ID3D11ShaderResourceView* reflectionTexture, ID3D11ShaderResourceView* refractionTexture)
{
	// Function declaration
	void RenderQuad(ID3D11ShaderResourceView*, ID3D11ShaderResourceView*);
	void Uninitialize();

	// Varible Declarations
	HRESULT hr;

	// Code
	hr = gpID3D11Device->CreateInputLayout(
		inputElementDesc,
		_ARRAYSIZE(inputElementDesc),
		pID3DBlob_VertexShaderCode_water->GetBufferPointer(),
		pID3DBlob_VertexShaderCode_water->GetBufferSize(),
		&gpID3D11InputLayout
	);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreateInputLayout() Failed");
		fflush(gpFile);

		pID3DBlob_VertexShaderCode_water->Release();
		pID3DBlob_VertexShaderCode_water = NULL;

		Uninitialize();
	}

	fprintf(gpFile, "\nUID3D11Device::CreateInputLayout() Successfull");
	fflush(gpFile);

	// set parameters
	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);
	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader_water, 0, 0);
	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader_water, 0, 0);
	gpID3D11DeviceContext->VSSetConstantBuffers(0, 1, &gpID3D11BufferConstantBuffer);
	gpID3D11DeviceContext->PSSetConstantBuffers(0, 1, &gpID3D11BufferConstantBuffer);

	// set render target view to normal 
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, gpID3D11DepthStencilView);
	gClearColor[0] = 0.2;
	gClearColor[1] = 0.2;
	gClearColor[2] = 0.2;
	gClearColor[3] = 1.0;
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);
	gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, D3D11_CLEAR_DEPTH);

	RenderQuad(reflectionTexture, refractionTexture);
}

void RenderCubeWithTexture(ID3D11ShaderResourceView* texture)
{
	// Function declaration
	void RenderCube(ID3D11ShaderResourceView*);
	void Uninitialize();

	// Varible Declarations
	HRESULT hr;

	// Code
	hr = gpID3D11Device->CreateInputLayout(
		inputElementDesc,
		_ARRAYSIZE(inputElementDesc),
		pID3DBlob_VertexShaderCode_fbo->GetBufferPointer(),
		pID3DBlob_VertexShaderCode_fbo->GetBufferSize(),
		&gpID3D11InputLayout
	);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreateInputLayout() Failed");
		fflush(gpFile);

		pID3DBlob_VertexShaderCode_fbo->Release();
		pID3DBlob_VertexShaderCode_fbo = NULL;

		Uninitialize();
	}

	fprintf(gpFile, "\nUID3D11Device::CreateInputLayout() Successfull");
	fflush(gpFile);

	// set parameters
	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);
	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader_fbo, 0, 0);
	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader_fbo, 0, 0);
	gpID3D11DeviceContext->VSSetConstantBuffers(0, 1, &gpID3D11BufferConstantBuffer);
	gpID3D11DeviceContext->PSSetConstantBuffers(0, 1, &gpID3D11BufferConstantBuffer);

	RenderCube(texture);

}

void RenderCubeWithTextureAndLight(ID3D11ShaderResourceView* texture)
{
	// Function declaration
	void RenderCube(ID3D11ShaderResourceView*);
	void Uninitialize();

	// Varible Declarations
	HRESULT hr;

	// Code
	hr = gpID3D11Device->CreateInputLayout(
		inputElementDesc,
		_ARRAYSIZE(inputElementDesc),
		pID3DBlob_VertexShaderCode_fbo->GetBufferPointer(),
		pID3DBlob_VertexShaderCode_fbo->GetBufferSize(),
		&gpID3D11InputLayout
	);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreateInputLayout() Failed");
		fflush(gpFile);

		pID3DBlob_VertexShaderCode_fbo->Release();
		pID3DBlob_VertexShaderCode_fbo = NULL;

		Uninitialize();
	}

	fprintf(gpFile, "\nUID3D11Device::CreateInputLayout() Successfull");
	fflush(gpFile);

	// set parameters
	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);
	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader_fbo, 0, 0);
	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader_fbo, 0, 0);
	gpID3D11DeviceContext->VSSetConstantBuffers(0, 1, &gpID3D11BufferConstantBuffer);
	gpID3D11DeviceContext->PSSetConstantBuffers(0, 1, &gpID3D11BufferConstantBuffer);

	RenderCube(texture);
	
}

void RenderCube(ID3D11ShaderResourceView* texture)
{
	UINT stride = sizeof(float) * 3;
	UINT offset = 0;
	gpID3D11DeviceContext->IASetVertexBuffers(0, 1, &gpID3D11Buffer_Cube_VertexBuffer_position, &stride, &offset);

	stride = sizeof(float) * 2;																							// jump
	offset = 0;
	gpID3D11DeviceContext->IASetVertexBuffers(2, 1, &gpID3D11Buffer_Cube_VertexBuffer_texcoord, &stride, &offset);

	stride = sizeof(float) * 3;
	offset = 0;
	gpID3D11DeviceContext->IASetVertexBuffers(3, 1, &gpID3D11Buffer_Cube_VertexBuffer_normals, &stride, &offset);

	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX translateMatrix = XMMatrixTranslation(0.0f, 0.0f, 3.0f);
	XMMATRIX rotateMatrix_x = XMMatrixRotationX(-rotateAngle);
	XMMATRIX rotateMatrix_y = XMMatrixRotationY(-rotateAngle);
	XMMATRIX rotateMatrix_z = XMMatrixRotationZ(-rotateAngle);
	XMMATRIX rotateMatrix = rotateMatrix_z * rotateMatrix_y * rotateMatrix_x;
	XMMATRIX scaleMatrix = XMMatrixScaling(0.8f, 0.8f, 0.8f);
	XMMATRIX viewMatrix = scaleMatrix * rotateMatrix * translateMatrix;

	CBUFFER_WATER constantBuffer;

	ZeroMemory((void*)&constantBuffer, sizeof(CBUFFER_WATER));

	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;

	constantBuffer.LightAmbient = XMVectorSet(LightAmbient[0], LightAmbient[1], LightAmbient[2], LightAmbient[3]);
	constantBuffer.LightDiffuse = XMVectorSet(LightDiffuse[0], LightDiffuse[1], LightDiffuse[2], LightDiffuse[3]);
	constantBuffer.LightSpecular = XMVectorSet(LightSpecular[0], LightSpecular[1], LightSpecular[2], LightSpecular[3]);
	constantBuffer.LightPosition = XMVectorSet(LightPosition[0], LightPosition[1], LightPosition[2], LightPosition[3]);

	constantBuffer.MaterialAmbient = XMVectorSet(MaterialAmbient[0], MaterialAmbient[1], MaterialAmbient[2], MaterialAmbient[3]);
	constantBuffer.MaterialDiffuse = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
	constantBuffer.MaterialSpecular = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
	constantBuffer.MaterialShininess = MaterialShininess;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11BufferConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	gpID3D11DeviceContext->PSSetShaderResources(0, 1, &texture);

	gpID3D11DeviceContext->PSSetSamplers(0, 1, &gpID3D11SamplerStateWaterTexture);

	gpID3D11DeviceContext->Draw(6 * 6, 0);
}

void RenderQuad(ID3D11ShaderResourceView* reflectionTexture, ID3D11ShaderResourceView* refractionTexture)
{
	UINT stride = sizeof(float) * 3;
	UINT offset = 0;
	gpID3D11DeviceContext->IASetVertexBuffers(0, 1, &gpID3D11Buffer_Cube_VertexBuffer_position, &stride, &offset);

	stride = sizeof(float) * 2;																							// jump
	offset = 0;
	gpID3D11DeviceContext->IASetVertexBuffers(2, 1, &gpID3D11Buffer_Cube_VertexBuffer_texcoord, &stride, &offset);

	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	
	XMMATRIX translateMatrix = XMMatrixTranslation(0.0f, 0.0f, 3.0f);
	//XMMATRIX viewMatrix = XMMatrixLookAtLH(
	//	XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f),
	//	XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f),
	//	XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f)
	//);
	XMMATRIX viewMatrix = XMMatrixIdentity();
	XMMATRIX rotateMatrix = XMMatrixRotationX(90.0f);
	XMMATRIX worldMatrix = rotateMatrix * translateMatrix;

	CBUFFER_WATER constantBuffer;

	ZeroMemory((void*)&constantBuffer, sizeof(CBUFFER_WATER));

	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;
	constantBuffer.moveFactor = gMoveFactor;
	constantBuffer.LightPosition = XMVectorSet(LightPosition[0], LightPosition[1], LightPosition[2], LightPosition[3]);
	constantBuffer.LightDiffuse = XMVectorSet(LightDiffuse[0], LightDiffuse[1], LightDiffuse[2], LightDiffuse[3]);
	constantBuffer.cameraPosition = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11BufferConstantBuffer, 0, NULL, &constantBuffer, 0, 0);

	gpID3D11DeviceContext->PSSetShaderResources(0, 1, &reflectionTexture);
	gpID3D11DeviceContext->PSSetShaderResources(1, 1, &refractionTexture);
	gpID3D11DeviceContext->PSSetShaderResources(2, 1, &gpID3D11ShaderResourceView_texture_dudvMap);
	gpID3D11DeviceContext->PSSetShaderResources(3, 1, &gpID3D11ShaderResourceView_texture_normal);

	gpID3D11DeviceContext->PSSetSamplers(0, 1, &gpID3D11SamplerStateWaterTexture);

	gpID3D11DeviceContext->Draw(6 , 0);
}

void Update()
{
	if (rotateAngle > 360.0f)
	{
		rotateAngle = 0.0f;
	}
	rotateAngle = rotateAngle + 0.00005;

	gMoveFactor = gMoveFactor + 0.00005;
}

void Uninitialize() {

	//code	
	if (pID3DBlob_VertexShaderCode_fbo)
	{
		pID3DBlob_VertexShaderCode_fbo->Release();
		pID3DBlob_VertexShaderCode_fbo = NULL;
	}

	if (gpID3D11BufferConstantBuffer)
	{
		gpID3D11BufferConstantBuffer->Release();
		gpID3D11BufferConstantBuffer = NULL;
	}

	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}

	if (gpID3D11Buffer_Cube_VertexBuffer_position)
	{
		gpID3D11Buffer_Cube_VertexBuffer_position->Release();
		gpID3D11Buffer_Cube_VertexBuffer_position = NULL;
	}

	if (gpID3D11PixelShader_fbo)
	{
		gpID3D11PixelShader_fbo->Release();
		gpID3D11PixelShader_fbo = NULL;
	}

	if (gpID3D11VertexShader_fbo)
	{
		gpID3D11VertexShader_fbo->Release();
		gpID3D11VertexShader_fbo = NULL;
	}

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	if (pIDXGIAdapter)
	{
		pIDXGIAdapter->Release();
		pIDXGIAdapter = NULL;
	}

	if (pIDXGIFactory)
	{
		pIDXGIFactory->Release();
		pIDXGIFactory = NULL;
	}

	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}

	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}

	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
	}

	if (gpFile) {
		fprintf(gpFile, "\nClosing File Successfully");
		fclose(gpFile);
	}

}

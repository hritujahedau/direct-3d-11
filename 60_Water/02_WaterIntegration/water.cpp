#include <windows.h>
#include <stdio.h>
#include <math.h>

#include "d3d.h"
#include <d3d11.h>
#include <d3dcompiler.h>

#pragma warning(disable: 4838)
#include "XNAMath/xnamath.h"
#include "WICTextureLoader.h"

extern FILE* gpFile;

HRESULT waterShader(ID3D11Device* gpID3D11Device, ID3DBlob** pID3DBlob_VertexShaderCode,
	ID3D11VertexShader** gpID3D11VertexShader, ID3D11PixelShader** gpID3D11PixelShader)
{
	// Variable Declarations
	HRESULT hr;

	// 1: declare vertex shader source code
	const char* vertexShaderSourceCode =
		" cbuffer ConstantBuffer																																					\n " \
		" {																																											\n " \
		"		float4x4 worldMatrix;																																				\n " \
		"		float4x4 viewMatrix;																																				\n " \
		"		float4x4 projectionMatrix;																																			\n " \
		"		float4 lightAmbient;																																				\n " \
		"		float4 lightDiffuse;																																				\n " \
		"		float4 lightSpecular;																																				\n " \
		"		float4 lightPosition;																																				\n " \
		"		float4 materialAmbient;																																				\n " \
		"		float4 materialDiffuse;																																				\n " \
		"		float4 materialSpecular;																																			\n " \
		"		float materialShininess;																																			\n " \
		"		float4 cameraPosition;																																				\n " \
		" }																																											\n " \
		" 																																											\n " \
		" struct vertex_out{																																						\n " \
		"		float4 position: SV_POSITION ;																																		\n " \
		"		float3 light_direction : NORMAL1;																																	\n " \
		"		float4 clipSpace : CLIP_SPACE;																																					\n " \
		"		float3 toCameraVector : CAMERA_VECTOR;																																				\n " \
		"		float2 textureCoords: TEXCOORD;																																			\n " \
		" };																																										\n " \
		" 																																											\n " \
		" static const float tiling = 1.0;																																					\n " \
		" 																																											\n " \
		" vertex_out main(float4 pos: POSITION, float4 col: COLOR, float2 tex: TEXCOORD, float3 normals : NORMAL)																	\n " \
		" {																																											\n " \
		"		struct vertex_out output;																																			\n " \
		"																																											\n " \
		"		float4 eye_position = mul ( worldMatrix , pos);																														\n " \
		"		eye_position = mul( viewMatrix, eye_position);																														\n " \
		"		output.light_direction = normalize ( (float3) (lightPosition - eye_position) );																						\n " \
		"																																											\n " \
		"		float4 worldPosition = mul(worldMatrix, pos);																														\n " \
		"		float4 position = mul ( viewMatrix, worldPosition);																													\n " \
		"		position = mul ( projectionMatrix, position);																														\n " \
		"		output.clipSpace = position;																																		\n " \
		"		output.position = position;																																			\n " \
		"		output.textureCoords = float2(position.x/2.0 + 0.5, position.y/2.0 + 0.5) * tiling;																																				\n " \
		"		output.toCameraVector = cameraPosition.xyz - worldPosition.xyz;																										\n " \
		"																																											\n " \
		"		return (output);																																					\n " \
		" }																																											\n " ;


	ID3DBlob* pID3DBlob_Error = NULL;

	// 2. compile vertex shader source code and get compiler converted vertex
	// D3DCompile not only compile shaders but also compiles
	hr = D3DCompile(
		vertexShaderSourceCode,
		lstrlenA(vertexShaderSourceCode) + 1,
		"VS",
		NULL,									// pass MACRO OF D3D_SHADER_MACRO*
		D3D_COMPILE_STANDARD_FILE_INCLUDE,		// take your standard file which neeed, we are not passing 
		"main",									// entry point function name
		"vs_5_0",								// vertex shader feature level is vs_5_0
		0,										//	no deugging , profileing , optimization, row column measurement
		0,										// don't consider it as special effect (effect is advance version of shader)
		pID3DBlob_VertexShaderCode,			// consider as shader
		&pID3DBlob_Error						// get errors if any
	);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fprintf(gpFile, "\nD3DCompile() Failed for vertex shader : %s.\n", (char*)pID3DBlob_Error->GetBufferPointer());
			fflush(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return hr;
		}
		else
		{
			// com error
			fprintf(gpFile, "\nSome COM error");
			fflush(gpFile);
		}
	}

	fprintf(gpFile, "\nD3DCompile() successfull for Vertex Shader");
	fflush(gpFile);

	hr = gpID3D11Device->CreateVertexShader(
		(*pID3DBlob_VertexShaderCode)->GetBufferPointer(),
		(*pID3DBlob_VertexShaderCode)->GetBufferSize(),
		NULL,											// ID3D11ClassInstance*
		gpID3D11VertexShader
	);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreateVertexShader() Failed\n");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nID3D11Device::CreateVertexShader() Successfull");
	fflush(gpFile);

	// gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader, 0, 0);
	// 2 - ID3D11ClassInstance how many elements
	// 3 - if 2 array give count

	const char* pixelShaderSourceCode =
		" cbuffer ConstantBuffer																																								\n " \
		" {																																														\n " \
		"		float4x4 worldMatrix;																																							\n " \
		"		float4x4 viewMatrix;																																							\n " \
		"		float4x4 projectionMatrix;																																						\n " \
		"		float4 lightAmbient;																																							\n " \
		"		float4 lightDiffuse;																																							\n " \
		"		float4 lightSpecular;																																							\n " \
		"		float4 lightPosition;																																							\n " \
		"		float4 materialAmbient;																																							\n " \
		"		float4 materialDiffuse;																																							\n " \
		"		float4 materialSpecular;																																						\n " \
		"		float materialShininess;																																						\n " \
		"		float4 cameraPosition;																																							\n " \
		"		float moveFactor;																																								\n " \
		" }																																														\n " \
		" struct vertex_out{																																									\n " \
		"		float4 position: SV_POSITION ;																																					\n " \
		"		float3 light_direction : NORMAL1;																																				\n " \
		"		float4 clipSpace : CLIP_SPACE;																																					\n " \
		"		float3 toCameraVector : CAMERA_VECTOR;																																			\n " \
		"		float2 textureCoords: TEXCOORD;																																					\n " \
		" };																																													\n " \
		" 																																													\n " \
		" Texture2D reflectionTexture;																																							\n " \
		" Texture2D refractionTexture;																																							\n " \
		" Texture2D dudvMapTexture;																																								\n " \
		" Texture2D normalMapTexture;																																							\n " \
		"																																														\n " \
		" static const float waveStrength = 0.02;																																				\n " \
		" static const float shineDamper = 20.0;																																				\n " \
		" static const float reflectivity = 0.6;																																				\n " \
		"																																														\n " \
		" SamplerState mySamplerState;																																							\n " \
		"																																														\n " \
		" float4 main(vertex_out input) : SV_TARGET																																				\n " \
		" {																																														\n " \
		"	float2 ndc = (input.clipSpace.xy/input.clipSpace.w)/2.0 + 0.5;																														\n "
		" 	float2 refractTexCoords = float2(ndc.x, ndc.y);																																		\n " \
		" 	float2 reflectTexCoords = float2(ndc.x, -ndc.y);																																	\n " \
		"																																														\n " \
		" 	float2 distortedTexCoords = (dudvMapTexture.Sample(mySamplerState, float2(input.textureCoords.x + moveFactor, input.textureCoords.y)).rg * 2.0 - 1.0) * waveStrength;				\n " \
		" 	distortedTexCoords = input.textureCoords + float2(distortedTexCoords.x + distortedTexCoords.y + moveFactor, 0.0);																	\n " \
		" 	float2 totalDistortion = (dudvMapTexture.Sample(mySamplerState, distortedTexCoords).rg * 2.0 - 1.0) * waveStrength;																	\n " \
		"																																														\n " \
		" 	refractTexCoords += totalDistortion;																																				\n " \
		" 	refractTexCoords = clamp(refractTexCoords, 0.001, 0.999);																															\n " \
		"																																														\n " \
		" 	reflectTexCoords += totalDistortion;																																				\n " \
		" 	reflectTexCoords.x = clamp(reflectTexCoords.x, 0.001, 0.999);																														\n " \
		" 	reflectTexCoords.y = clamp(reflectTexCoords.y, -0.999, -0.001);																														\n " \
		" 																																														\n " \
		"	float4 reflectionColor = reflectionTexture.Sample(mySamplerState, reflectTexCoords);																								\n " \
		"	float4 refractionColor = refractionTexture.Sample(mySamplerState, refractTexCoords);																								\n " \
		"																																														\n " \
		"	float3 viewVector = normalize(input.toCameraVector);																																		\n " \
		"	float refractiveFactor = dot(viewVector, float3(0.0, 1.0, 0.0));																														\n " \
		"	refractiveFactor = pow(refractiveFactor, 0.5);																																		\n " \
		"																																														\n " \
		"	float4 normalMapColor = normalMapTexture.Sample(mySamplerState, distortedTexCoords);																												\n " \
		"	float3 normal = float3(normalMapColor.r * 2.0 - 1.0, normalMapColor.b, normalMapColor.g * 2.0 - 1.0);																					\n " \
		"	normal = normalize(normal);																																							\n " \
		"																																														\n " \
		"	float3 reflectedLight = reflect(normalize(input.light_direction), normal);																													\n " \
		"	float specular = max(dot(reflectedLight, viewVector), 0.0);																															\n " \
		"	specular = pow(specular, shineDamper);																																				\n " \
		"	float3 specularHighLights = lightDiffuse * specular * reflectivity;																													\n " \
		"																																														\n " \
		" 	float4 out_Color = lerp(reflectionColor , refractionColor, refractiveFactor);																												\n " \
		" 	out_Color = lerp(out_Color , float4(0.0, 0.3, 0.5, 1.0), 0.2) + float4(specularHighLights, 0.0);																							\n " \
		"																																														\n " \
		"																																														\n " \
		"	return out_Color;																																										\n " \
		" }																																														\n " ;

	ID3DBlob* pID3DBlob_PixelShaderCode = NULL;
	pID3DBlob_Error = NULL;
	hr = D3DCompile(pixelShaderSourceCode,
		lstrlenA(pixelShaderSourceCode) + 1,
		"PS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&pID3DBlob_PixelShaderCode,
		&pID3DBlob_Error
	);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fprintf(gpFile, "\nD3DCompile() Failed For Pixel Shader : %s \n", (char*)pID3DBlob_Error->GetBufferPointer());
			fflush(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return hr;
		}
		else
		{
			// com error
			fprintf(gpFile, "\nSome COM error");
			fflush(gpFile);
		}
	}

	fprintf(gpFile, "\nD3DCompile() Successful For Pixel Shader");

	hr = gpID3D11Device->CreatePixelShader(
		pID3DBlob_PixelShaderCode->GetBufferPointer(),
		pID3DBlob_PixelShaderCode->GetBufferSize(),
		NULL,
		gpID3D11PixelShader
	);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreatePixelShader() Failed.");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nID3D11Device::CreatePixelShader() Successfull.");
	fflush(gpFile);

	//gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader, 0, 0);
	if (pID3DBlob_PixelShaderCode)
	{
		pID3DBlob_PixelShaderCode->Release();
		pID3DBlob_PixelShaderCode = NULL;
	}

	if (pID3DBlob_Error)
	{
		pID3DBlob_Error->Release();
		pID3DBlob_Error = NULL;
	}
	return hr;
}

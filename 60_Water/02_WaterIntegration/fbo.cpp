#include <windows.h>
#include <stdio.h>
#include <math.h>

#include "d3d.h"
#include <d3d11.h>
#include <d3dcompiler.h>

#pragma warning(disable: 4838)
#include "XNAMath/xnamath.h"
#include "WICTextureLoader.h"

extern FILE* gpFile;


HRESULT RenderToTargetShader(ID3D11Device* gpID3D11Device, ID3DBlob** pID3DBlob_VertexShaderCode, 
	ID3D11VertexShader** gpID3D11VertexShader, ID3D11PixelShader** gpID3D11PixelShader)
{
	// Variable Declarations
	HRESULT hr;

	// Code
	fprintf(gpFile, "\nEntering in RenderToTargetShader\n");
	fflush(gpFile);

	// 1: declare vertex shader source code

	const char* vertexShaderSourceCode =
		" cbuffer ConstantBuffer																																		" \
		" {																																								" \
		"		float4x4 worldMatrix;																																	" \
		"		float4x4 viewMatrix;																																	" \
		"		float4x4 projectionMatrix;																																" \
		"		float4 lightAmbient;																																	" \
		"		float4 lightDiffuse;																																	" \
		"		float4 lightSpecular;																																	" \
		"		float4 lightPosition;																																	" \
		"		float4 materialAmbient;																																	" \
		"		float4 materialDiffuse;																																	" \
		"		float4 materialSpecular;																																" \
		"		float materialShininess;																																" \
		" }																																								" \
		" struct vertex_out{																																						" \
		"		float4 position: SV_POSITION ;																																		" \
		"		float2 texcoord: TEXCOORD;																																			" \
		" };																																										" \
		" vertex_out main(float4 pos: POSITION, float4 col: COLOR, float2 tex: TEXCOORD, float3 normals : NORMAL)																	" \
		" {																																											" \
		"		struct vertex_out output;																																					" \
		"																																										" \
		"		float4 position = mul(worldMatrix, pos);																										" \
		"		position = mul ( viewMatrix, position);																											" \
		"		position = mul ( projectionMatrix, position);																									" \
		"		output.position = position;																																			" \
		"		output.texcoord = tex;																																				" \
		"																																						" \
		"		return (output);																																					" \
		" }																																											";

	//float4 position = mul(worldViewProjectionMatrix, pos);

	ID3DBlob* pID3DBlob_Error = NULL;

	// 2. compile vertex shader source code and get compiler converted vertex
	// D3DCompile not only compile shaders but also compiles
	hr = D3DCompile(
		vertexShaderSourceCode,
		lstrlenA(vertexShaderSourceCode) + 1,
		"VS",
		NULL,									// pass MACRO OF D3D_SHADER_MACRO*
		D3D_COMPILE_STANDARD_FILE_INCLUDE,		// take your standard file which neeed, we are not passing 
		"main",									// entry point function name
		"vs_5_0",								// vertex shader feature level is vs_5_0
		0,										//	no deugging , profileing , optimization, row column measurement
		0,										// don't consider it as special effect (effect is advance version of shader)
		pID3DBlob_VertexShaderCode,			// consider as shader
		&pID3DBlob_Error						// get errors if any
	);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fprintf(gpFile, "\nD3DCompile() Failed for vertex shader : %s.\n", (char*)pID3DBlob_Error->GetBufferPointer());
			fflush(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return hr;
		}
		else
		{
			// com error
			fprintf(gpFile, "\nSome COM error");
			fflush(gpFile);
		}
	}

	fprintf(gpFile, "\nD3DCompile() successfull for Vertex Shader");
	fflush(gpFile);

	hr = gpID3D11Device->CreateVertexShader(
		(*pID3DBlob_VertexShaderCode)->GetBufferPointer(),
		(*pID3DBlob_VertexShaderCode)->GetBufferSize(),
		NULL,											// ID3D11ClassInstance*
		gpID3D11VertexShader
	);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreateVertexShader() Failed\n");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nID3D11Device::CreateVertexShader() Successfull");
	fflush(gpFile);

	// gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader, 0, 0);
	// 2 - ID3D11ClassInstance how many elements
	// 3 - if 2 array give count

	const char* pixelShaderSourceCode =
		" cbuffer ConstantBuffer																																		" \
		" {																																								" \
		"		float4x4 worldMatrix;																																	" \
		"		float4x4 viewMatrix;																																	" \
		"		float4x4 projectionMatrix;																																" \
		"		float4 lightAmbient;																																	" \
		"		float4 lightDiffuse;																																	" \
		"		float4 lightSpecular;																																	" \
		"		float4 lightPosition;																																	" \
		"		float4 materialAmbient;																																	" \
		"		float4 materialDiffuse;																																	" \
		"		float4 materialSpecular;																																" \
		"		float materialShininess;																																" \
		" }																																								" \
		" struct vertex_out{																																			" \
		"		float4 position: SV_POSITION ;																															" \
		"		float2 texcoord: TEXCOORD;																																" \
		" };																																							" \
		" Texture2D myTexture2D;																																		" \
		" SamplerState mySamplerState;																																	" \
		" float4 main(vertex_out input) : SV_TARGET																														" \
		" {																																								" \
		"	float4 color =  myTexture2D.Sample( mySamplerState, input.texcoord);																						" \
		"	return color;																																				" \
		" }																																								";

	ID3DBlob* pID3DBlob_PixelShaderCode = NULL;
	pID3DBlob_Error = NULL;
	hr = D3DCompile(pixelShaderSourceCode,
		lstrlenA(pixelShaderSourceCode) + 1,
		"PS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&pID3DBlob_PixelShaderCode,
		&pID3DBlob_Error
	);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fprintf(gpFile, "\nD3DCompile() Failed For Pixel Shader : %s \n", (char*)pID3DBlob_Error->GetBufferPointer());
			fflush(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return hr;
		}
		else
		{
			// com error
			fprintf(gpFile, "\nSome COM error");
			fflush(gpFile);
		}
	}

	fprintf(gpFile, "\nD3DCompile() Successful For Pixel Shader");

	hr = gpID3D11Device->CreatePixelShader(
		pID3DBlob_PixelShaderCode->GetBufferPointer(),
		pID3DBlob_PixelShaderCode->GetBufferSize(),
		NULL,
		gpID3D11PixelShader
	);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreatePixelShader() Failed.");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nID3D11Device::CreatePixelShader() Successfull.");
	fflush(gpFile);

	//gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader, 0, 0);
	if (pID3DBlob_PixelShaderCode)
	{
		pID3DBlob_PixelShaderCode->Release();
		pID3DBlob_PixelShaderCode = NULL;
	}

	if (pID3DBlob_Error)
	{
		pID3DBlob_Error->Release();
		pID3DBlob_Error = NULL;
	}

	fprintf(gpFile, "\nExiting from RenderToTargetShader\n");
	fflush(gpFile);

	return hr;
}



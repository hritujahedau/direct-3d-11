copy 2_3D_Shape_rotation program
copy DirectXTK.lib, WICTextureLoader.h and 2 texture files in this directory
-----Global changes-------
1. #include "WICTextureLoader.h"
2. #pragma comment(lib , "DirectXTK.lib")

3.  rename gpID3D11Buffer_Triangle_VertexBuffer_position to gpID3D11Buffer_Pyramid_VertexBuffer_position
	add new variable gpID3D11Buffer_Pyramid_VertexBuffer_texcoord 
	remove gpID3D11Buffer_Pyramid_VertexBuffer_Color
4.  gpID3D11Buffer_Rectangle_VertexBuffer_position to gpID3D11Buffer_Cube_VertexBuffer_position
    add new variable gpID3D11Buffer_Rectangle_VertexBuffer_texcoord 
	remove gpID3D11Buffer_Cube_VertexBuffer_Color
5. add below 3 variables
	ID3D11ShaderResourceView* gpID3D11ShaderResourceView_texture_stone = NULL;
	ID3D11ShaderResourceView* gpID3D11ShaderResourceView_texture_kundali = NULL;
	ID3D11SamplerState* gpID3D11SamplerState_texture = NULL;


-----In Initialize()---

1. Declare LoadD3DTexture() as below:
	HRESULT LoadD3DTexture(const wchar_t*, ID3D11ShaderResourceView**);

2. Vertex shader changes

i) Change in struct vertex_out

	" struct vertex_out{																						" \
	"		float4 position: SV_POSITION ;																		" \
	"		float2 texcoord: TEXCOORD;																			" \
	" };																										" \

ii)  Change in main() declarations 

		" vertex_out main(float4 pos: POSITION, float2 tex: TEXCOORD)											" \

iii) set texcoord and return vertex_out object 

3. Pixel Shader changes
i) Change in struct vertex_out

	" struct vertex_out{																		" \
	"		float4 position: SV_POSITION ;														" \
	"		float2 texcoord: TEXCOORD;															" \
	" };																						" \
	" Texture2D myTexture2D;																	" \
	" SamplerState mySamplerState;																" \

ii) set color 
    "		float4 color = myTexture2D.Sample( mySamplerState, input.texcoord);					" \

4. in inputElementDesc 
	i)   set semantic name from COLOR to TEXCOORD
	ii)  set Format DXGI_FORMAT_R32G32_FLOAT
	iii) set AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT
		
For Pyramid
5. Add new array pyramidTexcoord
6. Create vertex buffer for texcoord and coy data to buffer for pyramid

For Cube
7. Add cubeTexcoord array
8. Create vertex buffer for texcoord and copy data to buffer for cube

Texture Changes

9. call LoadD3DTexture() and do error checking
	hr = LoadD3DTexture(L"Stone.bmp", &gpID3D11ShaderResourceView_texture);
	hr = LoadD3DTexture(L"Vijay_Kundali_flipped.bmp", &gpID3D11ShaderResourceView_texture_cube);
NOTE: For cube use flipeed image

10. Create sampler state for texture
	i) create D3D11_SAMPLER_DESC  object and intialize it

	D3D11_SAMPLER_DESC d3d11SamplerDesc;

	ZeroMemory((void**)&d3d11SamplerDesc, sizeof(D3D11_SAMPLER_DESC));

	d3d11SamplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	d3d11SamplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP; // s
	d3d11SamplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP; // t
	d3d11SamplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP; // r

	ii) Get ID3D11SamplerState interface
		hr = gpID3D11Device->CreateSamplerState(&d3d11SamplerDesc, &gpID3D11SamplerState_texture);

-------Add new Function LoadD3DTexture()------

HRESULT LoadD3DTexture(const wchar_t* textureFileName, ID3D11ShaderResourceView** ppID3D11ShaderResourceView)
{
	// code
	HRESULT hr;

	hr = DirectX::CreateWICTextureFromFile(gpID3D11Device, gpID3D11DeviceContext, textureFileName, NULL, ppID3D11ShaderResourceView);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\n Failed to CreateWICTextureFromFile");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\n Successfull to CreateWICTextureFromFile");
	fflush(gpFile);

	return hr;
}

-------In Display()---------
For Pyramid
1. For texcoord 
	stride = sizeof(float) * 2
2. Set vertex texcord buffer
	gpID3D11DeviceContext->IASetVertexBuffers(1, 1, &gpID3D11Buffer_Pyramid_VertexBuffer_texcoord, &stride, &offset);
3. Set texture and sampler to pipeline
	gpID3D11DeviceContext->PSSetShaderResources(0, 1, &gpID3D11ShaderResourceView_texture);
	gpID3D11DeviceContext->PSSetSamplers(0, 1, &gpID3D11SamplerState_texture);
	
For Cube
4. stride = sizeof(float) * 2
5. Set vertex buffer of texcoord
	gpID3D11DeviceContext->IASetVertexBuffers(1, 1, &gpID3D11Buffer_Cube_VertexBuffer_texcoord, &stride, &offset);
6. Set texture and sampler to pipeline 
	gpID3D11DeviceContext->PSSetShaderResources(0, 1, &gpID3D11ShaderResourceView_texture_cube);
	gpID3D11DeviceContext->PSSetSamplers(0, 1, &gpID3D11SamplerState_texture);

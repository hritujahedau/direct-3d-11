1. Copy Perspective Triangle program

Globle Variable Changes

1. change gpID3D11Buffer_VertexBuffer -> gpID3D11Buffer_VertexBuffer_position
2. add new variable 
ID3D11Buffer*	gpID3D11Buffer_VertexBuffer_Color = NULL;

In Initialization()

1. Following Changes in vertex shader
i)
	struct vertex_out
	{						
			float4 position: SV_POSITION ;
			float4 color: COLOR;			
	 };									
ii) vertex_out main(float4 pos: POSITION, float4 col: COLOR)	
IMP - remove ' : SV_POSITION '
iii) set position and color in vertex_out object and return from main

2 Following chnages in pixel shader
i)
	struct vertex_out
	{						
			float4 position: SV_POSITION ;
			float4 color: COLOR;			
	 };
ii)
	float4 main(vertex_out input) : SV_TARGET	
	IMP = do NOT remove ' : SV_TARGET '

iii)
	return input.color;
	
3. create D3D11_INPUT_ELEMENT_DESC inputElementDesc[2]; 
   1 for position - No change in position
   2 for color - i) semantic name "COLOR" ii) InputSlot = 1
   
4. in gpID3D11Device->CreateInputLayout() - i) 1st parameter pass adress of array i.e array name , 
											ii) 2nd parameter count of inputlayout ' _ARRAYSIZE(inputElementDesc) '
5. add color array
6. Repeat Create buffer and copy data to buffer steps for color
7. Set Backgroud color to black
In Display()

1. gpID3D11DeviceContext->IASetVertexBuffers() repeat for color but 1st parameter as 1 becauze now its 1st slot
2. Set z to 5 or 6 in translation

In Uninitialize()

1. Release gpID3D11Buffer_VertexBuffer_position and gpID3D11Buffer_VertexBuffer_color. 
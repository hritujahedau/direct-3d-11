Copy white cube assigment

---- Gloable Changes ----------
1. ID3D11Buffer* gpID3D11Buffer_Cube_VertexBuffer_normals = NULL;
2. Change in CBUFFER
struct CBUFFER
{
	XMMATRIX WorldViewMatrix;			//XMMATRIX - xna math matrix 
	XMMATRIX ProjectionMatrix;
	XMVECTOR LightDiffuse;
	XMVECTOR MaterialDiffuse;
	XMVECTOR LightPosition;
	unsigned int KeyPressed;
};

3. add new variables

bool gbLight = false, gbAnimate = false;

----- In WndProc() -------
1. add light toggle on 'l' or 'L' key

---------- In Initialize() ----------
1. Vertex shader change

i) change in cbuffer
	" cbuffer ConstantBuffer																									" \
		" {																															" \
		"		float4x4 worldViewMatrix;																							" \
		"		float4x4 projectionMatrix;																							" \
		"		float4 lightDiffuse;																								" \
		"		float4 materialDiffuse;																								" \
		"		float4 lightPosition;																								" \
		"		uint keyPressed;																									" \
		" }																															" \
	
ii) change in vertex out

	" struct vertex_out{																						" \
	"		float4 position: SV_POSITION ;																		" \
	"		float4 diffuse_light: COLOR;																		" \
	" };																										" \
iii) Change in main() 
		" vertex_out main(float4 pos: POSITION, float4 normals: NORMAL)																" \
		" {																															" \
		"		vertex_out output;																									" \
		"		if(keyPressed == 1)																									" \
		"		{																													" \
		"				float4 eye_position = mul(worldViewMatrix, pos);															" \
		"				float3 transformed_normals = normalize( mul ( (float3x3) worldViewMatrix, (float3) normals));				" \
		"				float3 source = (float3) normalize( lightPosition - eye_position) ;											" \
		"				output.diffuse_light = lightDiffuse * materialDiffuse * max( dot(source, transformed_normals), 0.0);		" \
		"		}																													" \
		"		else																												" \
		"		{																													" \
		"				output.diffuse_light = float4(1.0, 1.0, 1.0, 1.0);															" \
		"		}																													" \
		"		float4 position = mul ( projectionMatrix , mul( worldViewMatrix, pos));												" \
		"       output.position = position;																							" \
		"		return (output);																									" \
		" }																															" ;

2) Pixel shader changes
i) change in vertex out

	" struct vertex_out{																						" \
	"		float4 position: SV_POSITION ;																		" \
	"		float4 diffuse_light: COLOR;																		" \
	" };																										" \

ii) Set color = diffuse light as follow
	"		float4 color = input.diffuse_light;													" \
	
3) in inputElementDesc structure
	i)   SemanticName = "NORMAL"
	ii)  InputSlot = 1

4) add new array cubeNormals
5) Create vrtex buffer for normals and copy data as per position vertex buffer

-------- In Display() -------
1) Fill constant buffer
	if (gbLight == true)
	{
		constantBuffer.keyPressed = 1;
		constantBuffer.ld = XMVectorSet(1.0f, 1.0f, 1.0f, 1.0f);
		constantBuffer.kd = XMVectorSet(0.5, 0.5, 0.5, 1.0f);
		constantBuffer.lightPosition = XMVectorSet(0.0f, 0.0f, -2.0f, 1.0f);
	}
	else
	{
		constantBuffer.keyPressed = 0;
	}

	constantBuffer.WorldViewMatrix = wvMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;
	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	
-------- In Unintialize() ---------
1) Release all interface

// header file declaration
#include <windows.h>
#include <stdio.h>
#include <math.h>

#include "d3d.h"
#include <d3d11.h>
#include <d3dcompiler.h>

#pragma warning(disable: 4838)
#include "XNAMath/xnamath.h"

#include "Sphere.h"

#pragma comment(lib , "d3d11.lib")
#pragma comment(lib , "dxgi.lib")
#pragma comment(lib, "D3dcompiler.lib")
#pragma comment(lib, "Sphere.lib")

#define WIN_WIDTH 1000
#define WIN_HEIGHT 800
#define RADIAN_VALUE 3.14159/180

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int width, hight;

float gClearColor[4]; // RGBA
IDXGISwapChain* gpIDXGISwapChain = NULL;
ID3D11Device* gpID3D11Device = NULL;
ID3D11DeviceContext* gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView* gpID3D11RenderTargetView = NULL;

ID3D11VertexShader* gpID3D11VertexShader_PerVertex = NULL;
ID3D11PixelShader* gpID3D11PixelShader_PerVertex = NULL;

ID3D11VertexShader* gpID3D11VertexShader_PerPixel = NULL;
ID3D11PixelShader* gpID3D11PixelShader_PerPixel = NULL;

D3D11_INPUT_ELEMENT_DESC inputElementDesc[2];

ID3DBlob* pID3DBlob_VertexShaderCode_PerPixel = NULL;
ID3DBlob* pID3DBlob_VertexShaderCode_PerVertex = NULL;


ID3D11Buffer* gpID3D11Buffer_Sphere_VertexBuffer_position = NULL;
ID3D11Buffer* gpID3D11Buffer_Sphere_VertexBuffer_normals = NULL;
ID3D11Buffer* gpID3D11Buffer_Sphere_IndexBuffer = NULL;

ID3D11InputLayout* gpID3D11InputLayout = NULL;
ID3D11Buffer* gpID3D11Buffer_ConstantBuffer = NULL;

ID3D11RasterizerState* gpID3D11RasterizerState = NULL;

ID3D11DepthStencilView* gpID3D11DepthStencilView = NULL;

struct CBUFFER
{
	XMMATRIX WorldMatrix;
	XMMATRIX ViewMatrix;
	XMMATRIX ProjectionMatrix;
	
	XMVECTOR LightAmbient;
	XMVECTOR LightDiffuse;
	XMVECTOR LightSpecular;
	XMVECTOR LightPosition;

	XMVECTOR MaterialAmbient;
	XMVECTOR MaterialDiffuse;
	XMVECTOR MaterialSpecular;
	float MaterialShininess;

	unsigned int KeyPressed;
};

bool gbLight = false, gbAnimate = false;
bool isPerVertexLight = true, isPerPixelLight = false;

XMMATRIX gPerspectiveProjectionMatrix;

IDXGIFactory* pIDXGIFactory = NULL;
IDXGIAdapter* pIDXGIAdapter = NULL;

DXGI_ADAPTER_DESC dxgiAdapterDesc;
CHAR str[255];

float sphere_vertices[1146];
float Sphere_normals[1146];
float Sphere_texture[764];
unsigned short sphere_elements[2280];

int gNumVertices;
int gNumElements;

float LightAmbient [] = { 0.0f, 0.0f, 0.0f , 1.0};
float LightDiffuse [] = { 1.0f, 1.0f, 1.0f , 1.0f};
float LightSpecular[]  = { 1.0f, 1.0f, 1.0f , 1.0f};
float LightPosition[]  = { 0.0f, 0.0f, 0.0f, 1.0f };

float MaterialAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
float MaterialDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
float MaterialSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
float MaterialShininess = 58.0f;

int key_x = 0, key_y = 0, key_z = 0;
float sin_angle = 0.0f, cos_angle = 0.0f, radius = 10;
static float angle_for_light = 360.0f;

// first column
// 1
float materialAmbient_emerald[] = { 0.0215f, 0.1745f, 0.0215f, 1.0f };
float materialDefuse_emerald[] = { 0.07568f, 0.61424f, 0.07568f, 1.0f };
float materialSpecular_emerald[] = { 0.633f, 0.727811f, 0.633f, 1.0f };
float materialShinyness_emerald = 0.6 * 128;

// 2
float materialAmbient_jade[] = { 0.135f, 0.2225f, 0.1575f, 1.0f };
float materialDefuse_jade[] = { 0.54f, 0.89f, 0.63f, 1.0f };
float materialSpecular_jade[] = { 0.316228f, 0.316228f, 0.316228f, 1.0f };
float materialShinyness_jade = 0.1 * 128;

// 3
float materialAmbient_obsidian[] = { 0.05375f, 0.05f, 0.06625f, 1.0f };
float materialDefuse_obsidian[] = { 0.18275f, 0.17f, 0.22525f, 1.0f };
float materialSpecular_obsidian[] = { 0.332741f, 0.328634f, 0.346435f, 1.0f };
float materialShinyness_obsidian = 0.3 * 128;

// 4 pearl
float materialAmbient_pearl[] = { 0.25f, 0.20725f, 0.20725f, 1.0f };
float materialDefuse_pearl[] = { 1.0f, 0.829f, 0.829f, 1.0f };
float materialSpecular_pearl[] = { 0.296648f, 0.296648f, 0.296648f, 1.0f };
float materialShinyness_pearl = 0.088 * 128;

// 5 ruby 
float materialAmbient_ruby[] = { 0.1745f, 0.01175f, 0.01175f, 1.0f };
float materialDefuse_ruby[] = { 0.61424f, 0.04136f, 0.04136f, 1.0f };
float materialSpecular_ruby[] = { 0.727811f, 0.626959f, 0.626959f, 1.0f };
float materialShinyness_ruby = 0.6 * 128;

// 6 turquoise
float materialAmbient_turquoise[] = { 0.1f, 0.18725f, 0.1745f, 1.0f };
float materialDefuse_turquoise[] = { 0.396f, 0.74151f, 0.69102f, 1.0f };
float materialSpecular_turquoise[] = { 0.297254f, 0.30829f, 0.306678f, 1.0f };
float materialShinyness_turquoise = 0.1 * 128;

// second column
// 7 brass 
float materialAmbient_brass[] = { 0.329412f, 0.223529f, 0.027451f, 1.0f };
float materialDefuse_brass[] = { 0.780392f, 0.568627f, 0.113725f, 1.0f };
float materialSpecular_brass[] = { 0.992157f, 0.941176f, 0.807843f, 1.0f };
float materialShinyness_brass = 0.21794872 * 128;

// 8 bronze
float materialAmbient_bronze[] = { 0.2125f, 0.1275f, 0.054f, 1.0f };
float materialDefuse_bronze[] = { 0.714f, 0.4284f, 0.18144f, 1.0f };
float materialSpecular_bronze[] = { 0.393548f, 0.271906f, 0.166721f, 1.0f };
float materialShinyness_bronze = 0.2 * 128;

// 9 chrome
float materialAmbient_chrome[] = { 0.25f, 0.25f, 0.25f, 1.0f };
float materialDefuse_chrome[] = { 0.4f, 0.4f, 0.4f, 1.0f };
float materialSpecular_chrome[] = { 0.774597f, 0.774597f, 0.774597f, 1.0f };
float materialShinyness_chrome = 0.2 * 128;

// 10 4th sphere on 2nd column, copper  
float materialAmbient_copper[] = { 0.19125f, 0.19125f, 0.0225f, 1.0f };
float materialDefuse_copper[] = { 0.7038f, 0.27048f, 0.0828f, 1.0f };
float materialSpecular_copper[] = { 0.256777f, 0.137622f, 0.086014f, 1.0f };
float materialShinyness_copper = 0.1 * 128;

// 11 5th sphere on 2nd column, gold
float materialAmbient_gold[] = { 0.24725f, 0.1995f, 0.0745f, 1.0f };
float materialDefuse_gold[] = { 0.75164f, 0.60648f, 0.22648f, 1.0f };
float materialSpecular_gold[] = { 0.628281f, 0.555802f, 0.366065f, 1.0f };
float materialShinyness_gold = 0.4 * 128;

// 12 6th sphere on 2nd column, silver
float materialAmbient_silver[] = { 0.19225, 0.1995f, 0.19225f, 1.0f };
float materialDefuse_silver[] = { 0.50754f, 0.50754f, 0.50754f, 1.0f };
float materialSpecular_silver[] = { 0.508273f, 0.508273f, 0.508273f, 1.0f };
float materialShinyness_silver = 0.4 * 128;

// 13 1st sphere on 3rd column, black 
float materialAmbient_black[] = { 0.0, 0.0f, 0.0f, 1.0f };
float materialDefuse_black[] = { 0.01f, 0.01f, 0.01f, 1.0f };
float materialSpecular_black[] = { 0.5f, 0.5f, 0.5f, 1.0f };
float materialShinyness_black = 0.25 * 128;

// 14 2nd sphere on 3rd column, cyan
float materialAmbient_cyan[] = { 0.0, 0.1f, 0.06f, 1.0f };
float materialDefuse_cyan[] = { 0.0f, 0.50980392f, 0.50980392f, 1.0f };
float materialSpecular_cyan[] = { 0.50196078f, 0.50196078f, 0.50196078f, 1.0f };
float materialShinyness_cyan = 0.25 * 128;

// 15 3rd sphere on 2nd column, green
float materialAmbient_green[] = { 0.0, 0.0f, 0.0f, 1.0f };
float materialDefuse_green[] = { 0.1f, 0.35f, 0.1f, 1.0f };
float materialSpecular_green[] = { 0.45f, 0.55f, 0.45f, 1.0f };
float materialShinyness_green = 0.25 * 128;

// 16 4th sphere on 3rd column, red
float materialAmbient_red[] = { 0.0, 0.0f, 0.0f, 1.0f };
float materialDefuse_red[] = { 0.5, 0.0f, 0.0f, 1.0f };
float materialSpecular_red[] = { 0.7, 0.6f, 0.6f, 1.0f };
float materialShinyness_red = 0.25 * 128;

// 17 5th sphere on 3rd column, white
float materialAmbient_white[] = { 0.0, 0.0f, 0.0f, 1.0f };
float materialDefuse_white[] = { 0.55, 0.55f, 0.55f, 1.0f };
float materialSpecular_white[] = { 0.7, 0.7f, 0.7f, 1.0f };
float materialShinyness_white = 0.25 * 128;

// 18 6th sphere on 3rd column, yellow plastic
float materialAmbient_plastic[] = { 0.0, 0.0f, 0.0f, 1.0f };
float materialDefuse_plastic[] = { 0.5, 0.5f, 0.0f, 1.0f };
float materialSpecular_plastic[] = { 0.60, 0.60f, 0.50f, 1.0f };
float materialShinyness_plastic = 0.25 * 128;

// 19  1st sphere on 4th column, black
float materialAmbient_black_2[] = { 0.02, 0.02f, 0.02f, 1.0f };
float materialDefuse_black_2[] = { 0.01, 0.01f, 0.01f, 1.0f };
float materialSpecular_black_2[] = { 0.4f, 0.4f, 0.4f, 1.0f };
float materialShinyness_black_2 = 0.078125 * 128;

// 20  2nd sphere on 4th column, cyan
float materialAmbient_cyan_2[] = { 0.0, 0.05f, 0.05f, 1.0f };
float materialDefuse_cyan_2[] = { 0.4, 0.5f, 0.5f, 1.0f };
float materialSpecular_cyan_2[] = { 0.04f, 0.7f, 0.7f, 1.0f };
float materialShinyness_cyan_2 = 0.078125 * 128;

// 21  3rd sphere on 4th column, green
float materialAmbient_green_2[] = { 0.0, 0.05f, 0.05f, 1.0f };
float materialDefuse_green_2[] = { 0.4, 0.5f, 0.4f, 1.0f };
float materialSpecular_green_2[] = { 0.04f, 0.7f, 0.04f, 1.0f };
float materialShinyness_green_2 = 0.078125 * 128;

// 22   4th sphere on 4th column, red 
float materialAmbient_red_2[] = { 0.05, 0.0f, 0.0f, 1.0f };
float materialDefuse_red_2[] = { 0.5, 0.4f, 0.4f, 1.0f };
float materialSpecular_red_2[] = { 0.7f, 0.04f, 0.04f, 1.0f };
float materialShinyness_red_2 = 0.078125 * 128;

// 23   5th sphere on 4th column, white
float materialAmbient_white_2[] = { 0.05, 0.05f, 0.05f, 1.0f };
float materialDefuse_white_2[] = { 0.5, 0.5f, 0.5f, 1.0f };
float materialSpecular_white_2[] = { 0.7f, 0.7f, 0.7f, 1.0f };
float materialShinyness_white_2 = 0.078125 * 128;

// 24   6th sphere on 4th column, yellow rubber
float materialAmbient_rubber[] = { 0.05, 0.05f, 0.0f, 1.0f };
float materialDefuse_rubber[] = { 0.5, 0.5f, 0.4f, 1.0f };
float materialSpecular_rubber[] = { 0.7f, 0.7f, 0.04f, 1.0f };
float materialShinyness_rubber = 0.078125 * 128;


float translate_x = 0.0f, translate_y = 2.0f, translate_z = 0.0f, diiference_y = 0.7;


//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	HRESULT Initialize();
	void Uninitialize();
	void Display();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("h_direct3D11");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 
	if (fopen_s(&gpFile, "windows.log", "w") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("Direct3D11 Window"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIN_WIDTH/2 ,
		monitorRC.bottom / 2 - WIN_HEIGHT/2 ,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	HRESULT hr;
	hr = Initialize();
	if (FAILED(hr))
	{
		fprintf(gpFile, "\ninitialize() failed.Exiting Now..\n");
		fclose(gpFile);
		DestroyWindow(ghwnd);
		hwnd = NULL;
	}

	// game loop
	while (gbDone == false) 
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 
		{
			if (msg.message == WM_QUIT) 
			{
				gbDone = true;
			}
			else 
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else 
		{
			Display();
			if (gbActiveWindow) 
			{
				if (gbEscapeKeyIsPressed == true)
				{
					gbDone = true;
				}
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	HRESULT Resize(int,int);

	HRESULT hr;

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			if (gpID3D11DeviceContext)
			{
				hr = Resize(LOWORD(lParam), HIWORD(lParam));
				if (FAILED(hr))
				{
					fprintf(gpFile, "\nResize() failed");
					fflush(gpFile);
					DestroyWindow(ghwnd);
					return hr;
				}
				else
				{
					fprintf(gpFile, "\nResize() successfull");
					fflush(gpFile);
				}
			}
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				if (gbEscapeKeyIsPressed == false)
				{
					gbEscapeKeyIsPressed = true;
				}
				//DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_CHAR:
			switch (wParam)
			{
			case 'a':
				gbAnimate = true;
				break;
			case 'A':
				gbAnimate = false;
				break;
			case 'l':
			case 'L':
				if (gbLight)
				{
					gbLight = false;
					isPerVertexLight = 0;
					isPerPixelLight = 0;
					angle_for_light = 0.0f;
				}
				else
				{
					key_x = 0;
					key_y = 0;
					key_z = 0;
					gbLight = true;
				}
				break;
			case 'v':
			case 'V':
				if (isPerVertexLight == 1)
				{
					isPerVertexLight = 0;
					gbLight = false;
				}
				else
				{
					isPerVertexLight = 1;
					isPerPixelLight = 0;
					gbLight = true;
					angle_for_light = 0.0f;
				}
				break;
			case 'p':
			case 'P':
				if (isPerPixelLight == 0)
				{
					isPerPixelLight = 1;
					isPerVertexLight = 0;
					gbLight = true;
					angle_for_light = 0.0f;
				}
				else
				{
					isPerPixelLight = 0;
					gbLight = 0;
				}
				break;
			case 'x':
			case 'X':
				key_x = 1;
				key_y = 0;
				key_z = 0;
				break;
			case 'y':
			case 'Y':
				key_x = 0;
				key_y = 1;
				key_z = 0;
				break;
			case 'z':
			case 'Z':
				key_x = 0;
				key_y = 0;
				key_z = 1;
				break;
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

HRESULT Initialize() {
	// function declaration
	HRESULT Resize(int, int);
	void getSphereVertexData(float spherePositionCoords[1146], float sphereNormalCoords[1146], float sphereTexCoords[764], unsigned short sphereElements[2280]);
	unsigned int getNumberOfSphereVertices(void);
	unsigned int getNumberOfSphereElements(void);
	void Uninitialize();

	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE, D3D_DRIVER_TYPE_WARP, D3D_DRIVER_TYPE_REFERENCE
	};

	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0; // default, lowest

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;	//based upon d3dFeatureLevel_required

	// code
	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]); // calculation size of array
	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;

	ZeroMemory((void*)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	dxgiSwapChainDesc.BufferCount = 1;
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd;
	// MSAA - Multi Sampling Ani Alizing 
	dxgiSwapChainDesc.SampleDesc.Count = 1; // Max 8 2^8 sampling
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,								// adapter
			d3dDriverType,						// Driver Type
			NULL,								// Software
			createDeviceFlags,					// Flags
			&d3dFeatureLevel_required,			// Feature Levels
			numFeatureLevels,					// Num Feature Levels
			D3D11_SDK_VERSION,					// sdk verstion
			&dxgiSwapChainDesc,					// Swap Chain Desc
			&gpIDXGISwapChain,					// Swap chain
			&gpID3D11Device,					// Device
			&d3dFeatureLevel_acquired,			// Feature Level
			&gpID3D11DeviceContext				// device conext
		);

		if (SUCCEEDED(hr))
		{
			break;
		}
	}

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nD3D11CretaeDeviceAndSwapChain() Failed.");
		fflush(gpFile);
		//DestroyWindow(ghwnd);
		return hr;
	}
	else
	{
		fprintf(gpFile, "\nD3D11CreateDeviceAndSwapChain() Successded");
		fprintf(gpFile, "\nThe chosen driver is of");
		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf(gpFile, "\nHardware type.");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf(gpFile, "\nWarp type");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf(gpFile, "\nUnkown type");
		}

		fprintf(gpFile, "\nThe supported highest feature level is ");
		if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf(gpFile, "11.0");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf(gpFile, "10.1");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf(gpFile, "10.0");
		}
		else
		{
			fprintf(gpFile, "Unknown version");
		}
		fflush(gpFile);
	} // else closed

	hr = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&pIDXGIFactory);
	if (FAILED(hr))
	{
		fprintf(gpFile, "\nFailed to get pIDXGIFactory interface");
		fflush(gpFile);
		return hr;
	}

	if (pIDXGIFactory->EnumAdapters(1, &pIDXGIAdapter) == DXGI_ERROR_NOT_FOUND)
	{
		fprintf(gpFile, "\nFailed to get dxgi adapter");
		fflush(gpFile);
		return hr;
	}

	memset((void**)&dxgiAdapterDesc, 0, sizeof(DXGI_ADAPTER_DESC));

	hr = pIDXGIAdapter->GetDesc(&dxgiAdapterDesc);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\ndxgiAdapterDesc Failed to get");
		fflush(gpFile);
		return hr;
	}

	WideCharToMultiByte(CP_ACP, 0, dxgiAdapterDesc.Description, 255, str, 255, NULL, NULL);

	fprintf(gpFile, "\nGraphics card name is %s", str);
	fprintf(gpFile, "\nVideo Memory Size of graphics card is %d gb", int(ceil(dxgiAdapterDesc.DedicatedVideoMemory / 1024.0 / 1024.0 / 1024.0)));
	fflush(gpFile);

	/////////////////////////////////////////////// Per Vertex //////////////////////////////////////////////////////

	// 1: declare vertex shader source code

	const char* vertexShaderSourceCode_PerVertex =
		" cbuffer ConstantBuffer																																			\n " \
		" {																																									\n " \
		"		float4x4 worldMatrix;																																		\n " \
		"		float4x4 viewMatrix;																																		\n " \
		"		float4x4 projectionMatrix;																																	\n " \
		"		float4 lightAmbient;																																			\n " \
		"		float4 lightDiffuse;																																		\n " \
		"		float4 lightSpecular;																																		\n " \
		"		float4 lightPosition;																																		\n " \
		"		float4 materialAmbient;																																		\n " \
		"		float4 materialDiffuse;																																		\n " \
		"		float4 materialSpecular;																																	\n " \
		"		float materialShininess;																																	\n " \
		"		uint keyPressed;																																			\n " \
		" }																																									\n " \
		" struct vertex_out																																					\n " \
		" {																																									\n " \
		"		float4 position : SV_POSITION;																																\n " \
		"		float3 phoung_ads_light : COLOR;																															\n " \
		" };																																								\n " \
		" vertex_out main(float4 pos: POSITION, float3 normals : NORMAL)																									\n " \
		" {																																									\n " \
		"		struct vertex_out output;																																	\n " \
		"		if(keyPressed == 1)																																			\n " \
		"		{																																							\n " \
		"			float4 eye_position = mul( worldMatrix , pos);																											\n " \
		"			eye_position = mul(viewMatrix, eye_position);																											\n " \
		"			float3 transformed_normals = normalize(  mul ( (float3x3) mul ( viewMatrix, worldMatrix) , (float3) normals) );											\n " \
		"			float3 light_direction = normalize ( (float3) (lightPosition - eye_position) );																			\n " \
		"			float3 reflection_vector = reflect( -light_direction, transformed_normals);																				\n " \
		"			float3 viewVector = normalize ( -eye_position.xyz);																										\n " \
		"			float3 ambient = (float3) lightAmbient * (float3) materialAmbient;																						\n " \
		"			float3 diffuse = (float3) lightDiffuse * (float3) materialDiffuse * max( dot( light_direction, transformed_normals), 0.0) ;								\n " \
		"			float3 specular = (float3) lightSpecular * (float3) materialSpecular * pow ( max ( dot( reflection_vector, viewVector) , 0.0), materialShininess);		\n " \
		"			output.phoung_ads_light = ambient + diffuse + specular;																									\n " \
		"		}																																							\n " \
		"		else																																						\n " \
		"		{																																							\n " \
		"			output.phoung_ads_light = float3(1.0, 1.0, 1.0);																										\n " \
		"		}																																							\n " \
		"		float4 position = mul(worldMatrix, pos);																													\n " \
		"		position = mul ( viewMatrix, position);																														\n " \
		"		position = mul ( projectionMatrix, position); 																												\n " \
		"		output.position = position;																																	\n " \
		"		return (output);																																			\n " \
		" }																																									\n " ;

	ID3DBlob* pID3DBlob_Error = NULL;

	// 2. compile vertex shader source code and get compiler converted vertex
	// D3DCompile not only compile shaders but also compiles
	hr = D3DCompile(
		vertexShaderSourceCode_PerVertex,
		lstrlenA(vertexShaderSourceCode_PerVertex) + 1,
		"VS",
		NULL,									// pass MACRO OF D3D_SHADER_MACRO*
		D3D_COMPILE_STANDARD_FILE_INCLUDE,		// take your standard file which neeed, we are not passing 
		"main",									// entry point function name
		"vs_5_0",								// vertex shader feature level is vs_5_0
		0,										//	no deugging , profileing , optimization, row column measurement
		0,										// don't consider it as special effect (effect is advance version of shader)
		&pID3DBlob_VertexShaderCode_PerVertex,	// consider as shader
		&pID3DBlob_Error						// get errors if any
	);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fprintf(gpFile, "\nD3DCompile() Failed for vertex shader for per vertex light : %s.\n",(char *)pID3DBlob_Error->GetBufferPointer());
			fflush(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return hr;
		}
		else
		{
			// com error
			fprintf(gpFile, "\nSome COM error");
			fflush(gpFile);
		}
	}
	
	fprintf(gpFile, "\nD3DCompile() successfull for Vertex Shader for per vertex light");
	fflush(gpFile);
	
	hr = gpID3D11Device->CreateVertexShader(
		pID3DBlob_VertexShaderCode_PerVertex->GetBufferPointer(),
		pID3DBlob_VertexShaderCode_PerVertex->GetBufferSize(),
		NULL,											// ID3D11ClassInstance*
		&gpID3D11VertexShader_PerVertex
		);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreateVertexShader() Failed for per vertex light\n");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nID3D11Device::CreateVertexShader() Successfull for per vertex light");
	fflush(gpFile);

	

	const char* pixelShaderSourceCode_PerVertex =
		" struct vertex_out													\n " \
		" {																	\n " \
		"		float4 position : SV_POSITION;								\n " \
		"		float3 phoung_ads_light : COLOR;							\n " \
		" };																\n " \
		" float4 main(struct vertex_out input) : SV_TARGET					\n " \
		" {																	\n " \
		"		return (float4(input.phoung_ads_light, 1.0f));				\n " \
		" }																	\n ";

	ID3DBlob* pID3DBlob_PixelShaderCode_PerVertex = NULL;
	pID3DBlob_Error = NULL;
	hr = D3DCompile(pixelShaderSourceCode_PerVertex,
		lstrlenA(pixelShaderSourceCode_PerVertex) + 1,
		"PS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&pID3DBlob_PixelShaderCode_PerVertex,
		&pID3DBlob_Error
	);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fprintf(gpFile, "D3DCompile() Failed For Pixel Shader for per vertex light: %s \n", (char *)pID3DBlob_Error->GetBufferPointer());
			fflush(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return hr;
		}
		else
		{
			// com error
			fprintf(gpFile, "\nSome COM error");
			fflush(gpFile);
		}
	}

	fprintf(gpFile, "\nD3DCompile() Successful For Pixel Shader per vertex light");

	hr = gpID3D11Device->CreatePixelShader(
		pID3DBlob_PixelShaderCode_PerVertex->GetBufferPointer(),
		pID3DBlob_PixelShaderCode_PerVertex->GetBufferSize(),
		NULL,
		&gpID3D11PixelShader_PerVertex
		);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreatePixelShader() Failed for per vertex light.");
		fflush(gpFile);
		return hr;
	}

	if (pID3DBlob_PixelShaderCode_PerVertex)
	{
		pID3DBlob_PixelShaderCode_PerVertex->Release();
		pID3DBlob_PixelShaderCode_PerVertex = NULL;
	}

	if (pID3DBlob_Error)
	{
		pID3DBlob_Error->Release();
		pID3DBlob_Error = NULL;
	}

	fprintf(gpFile, "\nID3D11Device::CreatePixelShader() Successfull for per vertex light.");
	fflush(gpFile);
	
	/////////////////////////////////////////////// Per Pixel //////////////////////////////////////////////////////

	fprintf(gpFile, "\n\nPer Pixel Start Here \n\n");
	fflush(gpFile);

	// vertex shader
	const char* vertexShaderSourceCode_PerPixel =
		" cbuffer ConstantBuffer																							\n " \
		" {																													\n " \
		"		float4x4 worldMatrix;																						\n " \
		"		float4x4 viewMatrix;																						\n " \
		"		float4x4 projectionMatrix;																					\n " \
		"		float4 lightAmbient;																						\n " \
		"		float4 lightDiffuse;																						\n " \
		"		float4 lightSpecular;																						\n " \
		"		float4 lightPosition;																						\n " \
		"		float4 materialAmbient;																						\n " \
		"		float4 materialDiffuse;																						\n " \
		"		float4 materialSpecular;																					\n " \
		"		float materialShininess;																					\n " \
		"		uint keyPressed;																							\n " \
		" }																													\n " \
		" struct vertex_out																									\n " \
		" {																													\n " \
		"		float4 position : SV_POSITION;																				\n " \
		"		float3 transformed_normals : NORMAL0;																		\n " \
		"		float3 light_direction : NORMAL1;																			\n " \
		"		float3 viewer_vector : NORMAL2;																				\n " \
		" };																												\n " \
		" vertex_out main(float4 pos: POSITION, float3 normals: NORMAL)														\n " \
		" {																													\n " \
		"		struct vertex_out output;																					\n " \
		"		if( keyPressed == 1)																						\n " \
		"		{																											\n " \
		"			float4 eye_position = mul ( worldMatrix, pos); 															\n " \
		"			eye_position = mul(viewMatrix, eye_position);															\n " \
		"			output.transformed_normals =  mul ( (float3x3) mul ( viewMatrix, worldMatrix) , (float3) normals);		\n " \
		"			output.light_direction = (float3) lightPosition - eye_position.xyz;										\n " \
		"			output.viewer_vector = -eye_position.xyz;																\n " \
		"		}																											\n " \
		"		float4 position = mul ( worldMatrix, pos);																	\n " \
		"		position = mul ( viewMatrix, position);																		\n " \
		"		position = mul ( projectionMatrix, position);																\n " \
		"		output.position = position;																					\n " \
		"		return output;																								\n " \
		" }																													\n " \
		"																													\n " ;
		
	pID3DBlob_Error = NULL;
	hr = D3DCompile(vertexShaderSourceCode_PerPixel,
		lstrlenA(vertexShaderSourceCode_PerPixel) + 1,
		"VS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"vs_5_0",
		0,
		0,
		&pID3DBlob_VertexShaderCode_PerPixel,
		&pID3DBlob_Error
	);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fprintf(gpFile, "D3DCompile() Failed For vertex Shader per pixel light: %s \n", (char*)pID3DBlob_Error->GetBufferPointer());
			fflush(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return hr;
		}
		else
		{
			// com error
			fprintf(gpFile, "\nSome COM error");
			fflush(gpFile);
		}
	}

	fprintf(gpFile, "\nD3DCompile() Successful For vertex Shader for per pixel light");

	hr = gpID3D11Device->CreateVertexShader(
		pID3DBlob_VertexShaderCode_PerPixel->GetBufferPointer(),
		pID3DBlob_VertexShaderCode_PerPixel->GetBufferSize(),
		NULL,
		&gpID3D11VertexShader_PerPixel
	);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreateVertexShader() Failed for per pixel light.");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nID3D11Device::CreateVertexShader() Successfull for per pixel light.");
	fflush(gpFile);
	
	if (pID3DBlob_Error)
	{
		pID3DBlob_Error->Release();
		pID3DBlob_Error = NULL;
	}

	// Pixel shader

	const char* pixelShaderSourceCode_PerPixel =
		" cbuffer ConstantBuffer																																				\n " \
		" {																																										\n " \
		"		float4x4 worldMatrix;																																			\n " \
		"		float4x4 viewMatrix;																																			\n " \
		"		float4x4 projectionMatrix;																																		\n " \
		"		float4 lightAmbient;																																			\n " \
		"		float4 lightDiffuse;																																			\n " \
		"		float4 lightSpecular;																																			\n " \
		"		float4 lightPosition;																																			\n " \
		"		float4 materialAmbient;																																			\n " \
		"		float4 materialDiffuse;																																			\n " \
		"		float4 materialSpecular;																																		\n " \
		"		float materialShininess;																																		\n " \
		"		uint keyPressed;																																				\n " \
		" }																																										\n " \
		" struct vertex_out																																						\n " \
		" {																																										\n " \
		"		float4 position : SV_POSITION;																																	\n " \
		"		float3 transformed_normals : NORMAL0;																															\n " \
		"		float3 light_direction : NORMAL1;																																\n " \
		"		float3 viewer_vector : NORMAL2;																																	\n " \
		" };																																									\n " \
		" float4 main(struct vertex_out input) : SV_TARGET																														\n " \
		" {																																										\n " \
		"		float3 phoung_ads_light ;																																		\n " \
		"		if(keyPressed == 1)																																				\n " \
		"		{																																								\n " \
		"			float3 normalized_transformed_normals = normalize ( input.transformed_normals);																				\n " \
		"			float3 normalized_light_direction = normalize ( input.light_direction );																					\n " \
		"			float3 normalized_viewer_vector = normalize ( input.viewer_vector);																							\n " \
		"			float3 ambient = (float3) lightAmbient * (float3) materialAmbient;																							\n " \
		"			float3 diffuse = (float3) lightDiffuse * (float3) materialDiffuse * max ( dot( normalized_light_direction, normalized_transformed_normals), 0.0);				\n " \
		"			float3 reflection_vector = reflect ( -normalized_light_direction, normalized_transformed_normals);															\n " \
		"			float3 specular = (float3) (lightSpecular * materialSpecular) * pow ( max ( dot ( reflection_vector, normalized_viewer_vector), 0.0), materialShininess);	\n " \
		"			phoung_ads_light = ambient + diffuse + specular;																											\n " \
		"		}																																								\n " \
		"		else																																							\n " \
		"		{																																								\n " \
		"			phoung_ads_light = float3( 1.0, 1.0, 1.0);																												\n " \
		"		}																																								\n " \
		"		float4 color = float4(phoung_ads_light, 1.0);																													\n " \
		"		return color;																																					\n " \
		" }																																										\n " ;

	ID3DBlob* pID3DBlob_PixelShaderCode_PerPixel = NULL;
	pID3DBlob_Error = NULL;
	hr = D3DCompile(pixelShaderSourceCode_PerPixel,
		lstrlenA(pixelShaderSourceCode_PerPixel) + 1,
		"PS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&pID3DBlob_PixelShaderCode_PerPixel,
		&pID3DBlob_Error
	);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fprintf(gpFile, "\nD3DCompile() Failed For Pixel Shader for per pixel light : %s \n", (char*)pID3DBlob_Error->GetBufferPointer());
			fflush(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return hr;
		}
		else
		{
			// com error
			fprintf(gpFile, "\nSome COM error");
			fflush(gpFile);
		}
	}

	fprintf(gpFile, "\nD3DCompile() Successful For Pixel Shader for per pixel light");

	hr = gpID3D11Device->CreatePixelShader(
		pID3DBlob_PixelShaderCode_PerPixel->GetBufferPointer(),
		pID3DBlob_PixelShaderCode_PerPixel->GetBufferSize(),
		NULL,
		&gpID3D11PixelShader_PerPixel
	);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreatePixelShader() Failed for per pixel light.");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nID3D11Device::CreatePixelShader() Successfull for per pixel light.");
	fflush(gpFile);
	
	if (pID3DBlob_PixelShaderCode_PerPixel)
	{
		pID3DBlob_PixelShaderCode_PerPixel->Release();
		pID3DBlob_PixelShaderCode_PerPixel = NULL;
	}

	if (pID3DBlob_Error)
	{
		pID3DBlob_Error->Release();
		pID3DBlob_Error = NULL;
	}

	////////////////////////////////////////////////// End OF Shaders ///////////////////////////////////////////////////////

	// create input layout globally
	
	//ZeroMemory((void*)&inputElementDesc, sizeof(D3D11_INPUT_ELEMENT_DESC));

	inputElementDesc[0].SemanticName = "POSITION";
	inputElementDesc[0].SemanticIndex = 0;
	inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[0].InputSlot = 0;
	inputElementDesc[0].AlignedByteOffset = 0;
	inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[0].InstanceDataStepRate = 0;

	// 2nd

	inputElementDesc[1].SemanticName = "NORMAL";
	inputElementDesc[1].SemanticIndex = 0;
	inputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[1].InputSlot = 1;
	inputElementDesc[1].AlignedByteOffset = 0;
	inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[1].InstanceDataStepRate = 0;
		
	/////////////////////////////////////////////// SPHERE ////////////////////////////////////////////////////

	getSphereVertexData(sphere_vertices, Sphere_normals, Sphere_texture, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	D3D11_BUFFER_DESC bufferDesc;
	ZeroMemory((void*)&bufferDesc, sizeof(D3D11_BUFFER_DESC));
	
	bufferDesc.ByteWidth = gNumVertices * 3 * sizeof(float);
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11Buffer_Sphere_VertexBuffer_position);
	
	if (FAILED(hr))
	{
		fprintf(gpFile, "\n CreateBuffer() for rectangle position failed");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\n CreateBuffer() successfull for rectangle position");
	fflush(gpFile);

	ZeroMemory((void*)&bufferDesc, sizeof(D3D11_BUFFER_DESC));

	// cube normals
	bufferDesc.ByteWidth = 3 * gNumVertices * sizeof(float);
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11Buffer_Sphere_VertexBuffer_normals);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\n CreateBuffer() for rectangle normals failed");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\n CreateBuffer() for rectangle normals Successfull");
	fflush(gpFile);
	
	D3D11_MAPPED_SUBRESOURCE mappedSubresource;

	// pass rectangle vertices and color to gpu
	ZeroMemory((void*)&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	gpID3D11DeviceContext->Map(gpID3D11Buffer_Sphere_VertexBuffer_position, NULL, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_vertices, gNumVertices * 3 * sizeof(float));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_Sphere_VertexBuffer_position, NULL);

	// cube normals
	ZeroMemory((void*)&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	gpID3D11DeviceContext->Map(gpID3D11Buffer_Sphere_VertexBuffer_normals, NULL, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, Sphere_normals, gNumVertices * 3 * sizeof(float));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_Sphere_VertexBuffer_normals, NULL);

	// index buffer

	ZeroMemory((void*)&bufferDesc, sizeof(D3D11_BUFFER_DESC));

	bufferDesc.ByteWidth = gNumElements * sizeof(short);
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11Buffer_Sphere_IndexBuffer);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\n CreateBuffer() for sphere index buffer failed");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\n CreateBuffer() for sphere index buffer Successfull");
	fflush(gpFile);

	ZeroMemory((void*)&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	gpID3D11DeviceContext->Map(gpID3D11Buffer_Sphere_IndexBuffer, NULL, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_elements, gNumElements * sizeof(short));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_Sphere_IndexBuffer, NULL);

	// define and set constant buffer
	// uniform buffer
	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
	ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(D3D11_BUFFER_DESC));
	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	
	hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer, nullptr, &gpID3D11Buffer_ConstantBuffer);
	
	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreateBuffer() Failed For Constant Buffer.");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nID3D11Device::CreateBuffer() Successfull For Constant Buffer.");
	fflush(gpFile);
	

	// CREATE AND SET RASTERIZER STATE

	D3D11_RASTERIZER_DESC d3d11_rasterizer_desc;

	ZeroMemory((void*)&d3d11_rasterizer_desc, sizeof(D3D11_RASTERIZER_DESC));

	d3d11_rasterizer_desc.AntialiasedLineEnable = FALSE;
	d3d11_rasterizer_desc.CullMode = D3D11_CULL_NONE;
	d3d11_rasterizer_desc.DepthBias = 0;
	d3d11_rasterizer_desc.DepthBiasClamp = 0.0f;
	d3d11_rasterizer_desc.DepthClipEnable = TRUE;
	d3d11_rasterizer_desc.FillMode = D3D11_FILL_SOLID;
	d3d11_rasterizer_desc.FrontCounterClockwise = FALSE;
	d3d11_rasterizer_desc.MultisampleEnable = FALSE;
	d3d11_rasterizer_desc.ScissorEnable = FALSE;
	d3d11_rasterizer_desc.SlopeScaledDepthBias = 0.0f;

	hr = gpID3D11Device->CreateRasterizerState(&d3d11_rasterizer_desc, &gpID3D11RasterizerState);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\n CreateRasterizerState() Failed");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\n CreateRasterizerState() sucsess");
	fflush(gpFile);

	gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState);

	gClearColor[0] = 0.25f;
	gClearColor[1] = 0.25f;
	gClearColor[2] = 0.25f;
	gClearColor[3] = 1.0f;

	gPerspectiveProjectionMatrix = XMMatrixIdentity();

	hr = Resize(WIN_WIDTH, WIN_HEIGHT);
	if (FAILED(hr))
	{
		fprintf(gpFile, "\nResize() Failed");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nResize successfull");
	fflush(gpFile);

	return S_OK;
}

HRESULT Resize(int width, int height) 
{
	HRESULT hr = S_OK;

	if (height == 0)
	{
		height = 1;
	}

	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	gpIDXGISwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

	ID3D11Texture2D* pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*) &pID3D11Texture2D_BackBuffer);

	// again get render target view from d3d11 using above back buffer
	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL, &gpID3D11RenderTargetView);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreateTargetView() Failed.");
		fflush(gpFile);
		return hr;
	}
	else
	{
		fprintf(gpFile, "\nID3D11Device::CreateTargetView() Successded.");
		fflush(gpFile);
	}

	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	// just like RTV ,  DSV also need texture buffer

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	// create and intialize
	D3D11_TEXTURE2D_DESC d3d11_texture2D_desc;

	ZeroMemory((void*)&d3d11_texture2D_desc, sizeof(D3D11_TEXTURE2D_DESC));

	d3d11_texture2D_desc.Width = (UINT) width;
	d3d11_texture2D_desc.Height = (UINT) height;
	d3d11_texture2D_desc.Format = DXGI_FORMAT_D32_FLOAT;
	d3d11_texture2D_desc.Usage = D3D11_USAGE_DEFAULT;
	d3d11_texture2D_desc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	d3d11_texture2D_desc.SampleDesc.Count = 1;	// this can be 1-4 as per needed capacity
	d3d11_texture2D_desc.SampleDesc.Quality = 0; // default
	d3d11_texture2D_desc.ArraySize = 1;
	d3d11_texture2D_desc.MipLevels = 1; // default;
	d3d11_texture2D_desc.CPUAccessFlags = 0; // default
	d3d11_texture2D_desc.MiscFlags = 0;

	// create texture2D as depth buffer
	ID3D11Texture2D* pID3D11Texture2D_depthbuffer;

	hr = gpID3D11Device->CreateTexture2D(&d3d11_texture2D_desc, NULL, &pID3D11Texture2D_depthbuffer);
	if (FAILED(hr))
	{
		fprintf(gpFile, "\n CreateTexture2D failed for depth");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\n CreateTexture2D successfully for depth");
	fflush(gpFile);

	// intialize depth stencil view desc
	D3D11_DEPTH_STENCIL_VIEW_DESC d3d11_depth_stencil_view_desc;

	ZeroMemory((void*)&d3d11_depth_stencil_view_desc, sizeof(d3d11_depth_stencil_view_desc));

	d3d11_depth_stencil_view_desc.Format = DXGI_FORMAT_D32_FLOAT;
	d3d11_depth_stencil_view_desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS; // TEXTURE2D MS - multi sample

	// create dsv
	hr = gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D_depthbuffer, &d3d11_depth_stencil_view_desc, &gpID3D11DepthStencilView);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\n CretaeDepthstencilView() failed");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\n CretaeDepthstencilView() successfully");
	fflush(gpFile);

	// set render target view as render target
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, gpID3D11DepthStencilView);

	// set viewport target view
	D3D11_VIEWPORT d3dViewPort;
	
	ZeroMemory((void*)&d3dViewPort, sizeof(D3D11_VIEWPORT));

	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f; // glClearDepth(1.0)
	
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);
	
	// set orthographic matrix
	gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)width / (float)height, 0.1f, 100.0f);

	return hr;
}

void Display()
{
	// function declaration
	void Uninitialize();

	// variable declarations
	HRESULT hr;
	float scaleSphere = 0.5f, sphere_translate_x = 0.0f, sphere_translate_y = 4.0f, sphere_translate_z = 0.0f, translate_z = 8.0f, diiference_y = 1.5f;
	
	// Code
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);
	gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, D3D11_CLEAR_DEPTH);

	/////////////////////////////// SPHERE /////////////////////////////
	
	if (isPerVertexLight)
	{
		hr = gpID3D11Device->CreateInputLayout(
			inputElementDesc,
			_ARRAYSIZE(inputElementDesc),
			pID3DBlob_VertexShaderCode_PerVertex->GetBufferPointer(),
			pID3DBlob_VertexShaderCode_PerVertex->GetBufferSize(),
			&gpID3D11InputLayout
		);

		if (FAILED(hr))
		{
			fprintf(gpFile, "\nID3D11Device::CreateInputLayout() Failed for per vertex light");
			fflush(gpFile);

			pID3DBlob_VertexShaderCode_PerVertex->Release();
			pID3DBlob_VertexShaderCode_PerVertex = NULL;

			Uninitialize();
		}

		fprintf(gpFile, "\nUID3D11Device::CreateInputLayout() Successfull for per vertex light");
		fflush(gpFile);

		gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);
		gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader_PerVertex, 0, 0);
		gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader_PerVertex, 0, 0);
		gpID3D11DeviceContext->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);
	}
	else
	{
		hr = gpID3D11Device->CreateInputLayout(
			inputElementDesc,
			_ARRAYSIZE(inputElementDesc),
			pID3DBlob_VertexShaderCode_PerPixel->GetBufferPointer(),
			pID3DBlob_VertexShaderCode_PerPixel->GetBufferSize(),
			&gpID3D11InputLayout
		);

		if (FAILED(hr))
		{
			fprintf(gpFile, "\nID3D11Device::CreateInputLayout() Failed for per pixel light");
			fflush(gpFile);

			pID3DBlob_VertexShaderCode_PerPixel->Release();
			pID3DBlob_VertexShaderCode_PerPixel = NULL;

			Uninitialize();
		}

		fprintf(gpFile, "\nUID3D11Device::CreateInputLayout() Successfull for per pixel light");
		fflush(gpFile);

		gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);
		gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader_PerPixel, 0, 0);
		gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader_PerPixel, 0, 0);
		gpID3D11DeviceContext->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);
		gpID3D11DeviceContext->PSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);
	}

	UINT stride = sizeof(float) * 3;
	UINT offset = 0;
	gpID3D11DeviceContext->IASetVertexBuffers(0, 1, &gpID3D11Buffer_Sphere_VertexBuffer_position, &stride, &offset);

	stride = sizeof(float) * 3;
	offset = 0;
	gpID3D11DeviceContext->IASetVertexBuffers(1, 1, &gpID3D11Buffer_Sphere_VertexBuffer_normals, &stride, &offset);

	gpID3D11DeviceContext->IASetIndexBuffer(gpID3D11Buffer_Sphere_IndexBuffer, DXGI_FORMAT_R16_UINT, 0);

	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	
	XMMATRIX viewMatrix = XMMatrixLookAtLH(
		XMVectorSet(0.0f, 0.0f, translate_z, 0.0f),
		XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f),
		XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f)
		);

	XMMATRIX worldMatrix = XMMatrixIdentity();

	CBUFFER constantBuffer;

	ZeroMemory((void*)&constantBuffer, sizeof(CBUFFER));

	sin_angle = radius * sin(angle_for_light * RADIAN_VALUE);
	cos_angle = radius * cos(angle_for_light * RADIAN_VALUE);

	if (key_x == 1)
	{
		LightPosition[0] = 0.0f;
		LightPosition[1] = sin_angle;
		LightPosition[2] = translate_z + cos_angle;
		LightPosition[3] = 1.0f;
	}
	else if (key_y == 1)
	{
		LightPosition[0] = sin_angle;
		LightPosition[1] = 0.0f;
		LightPosition[2] = translate_z + cos_angle;
		LightPosition[3] = 1.0f;
	}
	else if (key_z == 1)
	{
		LightPosition[0] = sin_angle;
		LightPosition[1] = cos_angle;
		LightPosition[2] = translate_z;
		LightPosition[3] = 1.0f;
	}

	if (key_x == 1 || key_y == 1 || key_z == 1)
	{
		if (angle_for_light < 0.0f)
		{
			angle_for_light = 360.0f;
		}
		angle_for_light = angle_for_light - 0.1f;
	}

	if (gbLight == true)
	{
		constantBuffer.KeyPressed = 1;

		constantBuffer.LightAmbient = XMVectorSet(LightAmbient[0], LightAmbient[1], LightAmbient[2], LightAmbient[3]);
		constantBuffer.LightDiffuse = XMVectorSet(LightDiffuse[0], LightDiffuse[1], LightDiffuse[2], LightDiffuse[3]);
		constantBuffer.LightSpecular = XMVectorSet(LightSpecular[0], LightSpecular[1], LightSpecular[2], LightSpecular[3]);
		constantBuffer.LightPosition = XMVectorSet(LightPosition[0], LightPosition[1], LightPosition[2], LightPosition[3]);		
	}
	else
	{
		constantBuffer.KeyPressed = 0;
	}

	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;
	
	/********************************************First Row**************************************************************/
	// first sphere
	if (gbLight == true)
	{
		constantBuffer.MaterialAmbient = XMVectorSet(materialAmbient_emerald[0], materialAmbient_emerald[1], materialAmbient_emerald[2], MaterialAmbient[3]);
		constantBuffer.MaterialDiffuse = XMVectorSet(materialDefuse_emerald[0], materialDefuse_emerald[1], materialDefuse_emerald[2], materialDefuse_emerald[3]);
		constantBuffer.MaterialSpecular = XMVectorSet(materialSpecular_emerald[0], materialSpecular_emerald[1], materialSpecular_emerald[2], MaterialSpecular[3]);
		constantBuffer.MaterialShininess = materialShinyness_emerald;
	}

	XMMATRIX translationMatrix = XMMatrixTranslation(sphere_translate_x - 3.0f, sphere_translate_y, sphere_translate_z);
	XMMATRIX scaleMatrix = XMMatrixScaling(scaleSphere, scaleSphere, scaleSphere);
	worldMatrix = translationMatrix * scaleMatrix;

	constantBuffer.WorldMatrix = worldMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	// second sphere
	if (gbLight == true)
	{
		constantBuffer.MaterialAmbient = XMVectorSet(materialAmbient_jade[0], materialAmbient_jade[1], materialAmbient_jade[2], materialAmbient_jade[3]);
		constantBuffer.MaterialDiffuse = XMVectorSet(materialDefuse_jade[0], materialDefuse_jade[1], materialDefuse_jade[2], materialDefuse_jade[3]);
		constantBuffer.MaterialSpecular = XMVectorSet(materialSpecular_jade[0], materialSpecular_jade[1], materialSpecular_jade[2], MaterialSpecular[3]);
		constantBuffer.MaterialShininess = materialShinyness_jade;
	}

	translationMatrix = XMMatrixTranslation(sphere_translate_x - 1.0f, sphere_translate_y, sphere_translate_z);
	scaleMatrix = XMMatrixScaling(scaleSphere, scaleSphere, scaleSphere);
	worldMatrix = translationMatrix * scaleMatrix;

	constantBuffer.WorldMatrix = worldMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	// third sphere
	if (gbLight == true)
	{
		constantBuffer.MaterialAmbient = XMVectorSet(materialAmbient_obsidian[0], materialAmbient_obsidian[1], materialAmbient_obsidian[2], materialAmbient_obsidian[3]);
		constantBuffer.MaterialDiffuse = XMVectorSet(materialDefuse_obsidian[0], materialDefuse_obsidian[1], materialDefuse_obsidian[2], materialDefuse_obsidian[3]);
		constantBuffer.MaterialSpecular = XMVectorSet(materialSpecular_obsidian[0], materialSpecular_obsidian[1], materialSpecular_obsidian[2], materialSpecular_obsidian[3]);
		constantBuffer.MaterialShininess = materialShinyness_obsidian;
	}

	translationMatrix = XMMatrixTranslation(sphere_translate_x + 1.0f, sphere_translate_y, sphere_translate_z);
	scaleMatrix = XMMatrixScaling(scaleSphere, scaleSphere, scaleSphere);
	worldMatrix = translationMatrix * scaleMatrix;

	constantBuffer.WorldMatrix = worldMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	// forth sphere
	if (gbLight == true)
	{
		constantBuffer.MaterialAmbient = XMVectorSet(materialAmbient_pearl[0], materialAmbient_pearl[1], materialAmbient_pearl[2], materialAmbient_pearl[3]);
		constantBuffer.MaterialDiffuse = XMVectorSet(materialDefuse_pearl[0], materialDefuse_pearl[1], materialDefuse_pearl[2], materialDefuse_pearl[3]);
		constantBuffer.MaterialSpecular = XMVectorSet(materialSpecular_pearl[0], materialSpecular_pearl[1], materialSpecular_pearl[2], materialSpecular_pearl[3]);
		constantBuffer.MaterialShininess = materialShinyness_pearl;
	}

	translationMatrix = XMMatrixTranslation(sphere_translate_x + 3.0f, sphere_translate_y, sphere_translate_z);
	scaleMatrix = XMMatrixScaling(scaleSphere, scaleSphere, scaleSphere);
	worldMatrix = translationMatrix * scaleMatrix;

	constantBuffer.WorldMatrix = worldMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	/********************************************Second Row**************************************************************/
	// first sphere
	if (gbLight == true)
	{
		constantBuffer.MaterialAmbient = XMVectorSet(materialAmbient_ruby[0], materialAmbient_ruby[1], materialAmbient_ruby[2], materialAmbient_ruby[3]);
		constantBuffer.MaterialDiffuse = XMVectorSet(materialDefuse_ruby[0], materialDefuse_ruby[1], materialDefuse_ruby[2], materialDefuse_ruby[3]);
		constantBuffer.MaterialSpecular = XMVectorSet(materialSpecular_ruby[0], materialSpecular_ruby[1], materialSpecular_ruby[2], materialSpecular_ruby[3]);
		constantBuffer.MaterialShininess = materialShinyness_ruby;
	}

	translationMatrix = XMMatrixTranslation(sphere_translate_x - 3.0f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = XMMatrixScaling(scaleSphere, scaleSphere, scaleSphere);
	worldMatrix = translationMatrix * scaleMatrix;

	constantBuffer.WorldMatrix = worldMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	// second sphere
	if (gbLight == true)
	{
		constantBuffer.MaterialAmbient = XMVectorSet(materialAmbient_turquoise[0], materialAmbient_turquoise[1], materialAmbient_turquoise[2], materialAmbient_turquoise[3]);
		constantBuffer.MaterialDiffuse = XMVectorSet(materialDefuse_turquoise[0], materialDefuse_turquoise[1], materialDefuse_turquoise[2], materialDefuse_turquoise[3]);
		constantBuffer.MaterialSpecular = XMVectorSet(materialSpecular_turquoise[0], materialSpecular_turquoise[1], materialSpecular_turquoise[2], materialSpecular_turquoise[3]);
		constantBuffer.MaterialShininess = materialShinyness_turquoise;
	}

	translationMatrix = XMMatrixTranslation(sphere_translate_x - 1.0f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = XMMatrixScaling(scaleSphere, scaleSphere, scaleSphere);
	worldMatrix = translationMatrix * scaleMatrix;

	constantBuffer.WorldMatrix = worldMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	// third sphere
	if (gbLight == true)
	{
		constantBuffer.MaterialAmbient = XMVectorSet(materialAmbient_brass[0], materialAmbient_brass[1], materialAmbient_brass[2], materialAmbient_brass[3]);
		constantBuffer.MaterialDiffuse = XMVectorSet(materialDefuse_brass[0], materialDefuse_brass[1], materialDefuse_brass[2], materialDefuse_brass[3]);
		constantBuffer.MaterialSpecular = XMVectorSet(materialSpecular_brass[0], materialSpecular_brass[1], materialSpecular_brass[2], materialSpecular_brass[3]);
		constantBuffer.MaterialShininess = materialShinyness_brass;
	}

	translationMatrix = XMMatrixTranslation(sphere_translate_x + 1.0f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = XMMatrixScaling(scaleSphere, scaleSphere, scaleSphere);
	worldMatrix = translationMatrix * scaleMatrix;

	constantBuffer.WorldMatrix = worldMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	// forth sphere
	if (gbLight == true)
	{
		constantBuffer.MaterialAmbient = XMVectorSet(materialAmbient_bronze[0], materialAmbient_bronze[1], materialAmbient_bronze[2], materialAmbient_bronze[3]);
		constantBuffer.MaterialDiffuse = XMVectorSet(materialDefuse_bronze[0], materialDefuse_bronze[1], materialDefuse_bronze[2], materialDefuse_bronze[3]);
		constantBuffer.MaterialSpecular = XMVectorSet(materialSpecular_bronze[0], materialSpecular_bronze[1], materialSpecular_bronze[2], materialSpecular_bronze[3]);
		constantBuffer.MaterialShininess = materialShinyness_bronze;
	}

	translationMatrix = XMMatrixTranslation(sphere_translate_x + 3.0f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = XMMatrixScaling(scaleSphere, scaleSphere, scaleSphere);
	worldMatrix = translationMatrix * scaleMatrix;

	constantBuffer.WorldMatrix = worldMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	/********************************************THIRD ROW**************************************************************/
	diiference_y = diiference_y + 1.5f;
	
	// first sphere
	if (gbLight == true)
	{
		constantBuffer.MaterialAmbient = XMVectorSet(materialAmbient_chrome[0], materialAmbient_chrome[1], materialAmbient_chrome[2], materialAmbient_chrome[3]);
		constantBuffer.MaterialDiffuse = XMVectorSet(materialDefuse_chrome[0], materialDefuse_chrome[1], materialDefuse_chrome[2], materialDefuse_chrome[3]);
		constantBuffer.MaterialSpecular = XMVectorSet(materialSpecular_chrome[0], materialSpecular_chrome[1], materialSpecular_chrome[2], materialSpecular_chrome[3]);
		constantBuffer.MaterialShininess = materialShinyness_chrome;
	}

	translationMatrix = XMMatrixTranslation(sphere_translate_x - 3.0f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = XMMatrixScaling(scaleSphere, scaleSphere, scaleSphere);
	worldMatrix = translationMatrix * scaleMatrix;

	constantBuffer.WorldMatrix = worldMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	// second sphere
	if (gbLight == true)
	{
		constantBuffer.MaterialAmbient = XMVectorSet(materialAmbient_copper[0], materialAmbient_copper[1], materialAmbient_copper[2], materialAmbient_copper[3]);
		constantBuffer.MaterialDiffuse = XMVectorSet(materialDefuse_copper[0], materialDefuse_copper[1], materialDefuse_copper[2], materialDefuse_copper[3]);
		constantBuffer.MaterialSpecular = XMVectorSet(materialSpecular_copper[0], materialSpecular_copper[1], materialSpecular_copper[2], materialSpecular_copper[3]);
		constantBuffer.MaterialShininess = materialShinyness_copper;
	}

	translationMatrix = XMMatrixTranslation(sphere_translate_x - 1.0f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = XMMatrixScaling(scaleSphere, scaleSphere, scaleSphere);
	worldMatrix = translationMatrix * scaleMatrix;

	constantBuffer.WorldMatrix = worldMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	// third sphere
	if (gbLight == true)
	{
		constantBuffer.MaterialAmbient = XMVectorSet(materialAmbient_gold[0], materialAmbient_gold[1], materialAmbient_gold[2], materialAmbient_gold[3]);
		constantBuffer.MaterialDiffuse = XMVectorSet(materialDefuse_gold[0], materialDefuse_gold[1], materialDefuse_gold[2], materialDefuse_gold[3]);
		constantBuffer.MaterialSpecular = XMVectorSet(materialSpecular_gold[0], materialSpecular_gold[1], materialSpecular_gold[2], materialSpecular_gold[3]);
		constantBuffer.MaterialShininess = materialShinyness_gold;
	}

	translationMatrix = XMMatrixTranslation(sphere_translate_x + 1.0f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = XMMatrixScaling(scaleSphere, scaleSphere, scaleSphere);
	worldMatrix = translationMatrix * scaleMatrix;

	constantBuffer.WorldMatrix = worldMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	// forth sphere
	if (gbLight == true)
	{
		constantBuffer.MaterialAmbient = XMVectorSet(materialAmbient_silver[0], materialAmbient_silver[1], materialAmbient_silver[2], materialAmbient_silver[3]);
		constantBuffer.MaterialDiffuse = XMVectorSet(materialDefuse_silver[0], materialDefuse_silver[1], materialDefuse_silver[2], materialDefuse_silver[3]);
		constantBuffer.MaterialSpecular = XMVectorSet(materialSpecular_silver[0], materialSpecular_silver[1], materialSpecular_silver[2], materialSpecular_silver[3]);
		constantBuffer.MaterialShininess = materialShinyness_silver;
	}

	translationMatrix = XMMatrixTranslation(sphere_translate_x + 3.0f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = XMMatrixScaling(scaleSphere, scaleSphere, scaleSphere);
	worldMatrix = translationMatrix * scaleMatrix;

	constantBuffer.WorldMatrix = worldMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	/********************************************FORTH ROW**************************************************************/
	diiference_y = diiference_y + 1.5f;

	// first sphere
	if (gbLight == true)
	{
		constantBuffer.MaterialAmbient = XMVectorSet(materialAmbient_black[0], materialAmbient_black[1], materialAmbient_black[2], materialAmbient_black[3]);
		constantBuffer.MaterialDiffuse = XMVectorSet(materialDefuse_black[0], materialDefuse_black[1], materialDefuse_black[2], materialDefuse_black[3]);
		constantBuffer.MaterialSpecular = XMVectorSet(materialSpecular_black[0], materialSpecular_black[1], materialSpecular_black[2], materialSpecular_black[3]);
		constantBuffer.MaterialShininess = materialShinyness_black;
	}

	translationMatrix = XMMatrixTranslation(sphere_translate_x - 3.0f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = XMMatrixScaling(scaleSphere, scaleSphere, scaleSphere);
	worldMatrix = translationMatrix * scaleMatrix;

	constantBuffer.WorldMatrix = worldMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	// second sphere
	if (gbLight == true)
	{
		constantBuffer.MaterialAmbient = XMVectorSet(materialAmbient_cyan[0], materialAmbient_cyan[1], materialAmbient_cyan[2], materialAmbient_cyan[3]);
		constantBuffer.MaterialDiffuse = XMVectorSet(materialDefuse_cyan[0], materialDefuse_cyan[1], materialDefuse_cyan[2], materialDefuse_cyan[3]);
		constantBuffer.MaterialSpecular = XMVectorSet(materialSpecular_cyan[0], materialSpecular_cyan[1], materialSpecular_cyan[2], materialSpecular_cyan[3]);
		constantBuffer.MaterialShininess = materialShinyness_cyan;
	}

	translationMatrix = XMMatrixTranslation(sphere_translate_x - 1.0f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = XMMatrixScaling(scaleSphere, scaleSphere, scaleSphere);
	worldMatrix = translationMatrix * scaleMatrix;

	constantBuffer.WorldMatrix = worldMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	// third sphere
	if (gbLight == true)
	{
		constantBuffer.MaterialAmbient = XMVectorSet(materialAmbient_green[0], materialAmbient_green[1], materialAmbient_green[2], materialAmbient_green[3]);
		constantBuffer.MaterialDiffuse = XMVectorSet(materialDefuse_green[0], materialDefuse_green[1], materialDefuse_green[2], materialDefuse_green[3]);
		constantBuffer.MaterialSpecular = XMVectorSet(materialSpecular_green[0], materialSpecular_green[1], materialSpecular_green[2], materialSpecular_green[3]);
		constantBuffer.MaterialShininess = materialShinyness_green;
	}

	translationMatrix = XMMatrixTranslation(sphere_translate_x + 1.0f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = XMMatrixScaling(scaleSphere, scaleSphere, scaleSphere);
	worldMatrix = translationMatrix * scaleMatrix;

	constantBuffer.WorldMatrix = worldMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	// forth sphere
	if (gbLight == true)
	{
		constantBuffer.MaterialAmbient = XMVectorSet(materialAmbient_red[0], materialAmbient_red[1], materialAmbient_red[2], materialAmbient_red[3]);
		constantBuffer.MaterialDiffuse = XMVectorSet(materialDefuse_red[0], materialDefuse_red[1], materialDefuse_red[2], materialDefuse_red[3]);
		constantBuffer.MaterialSpecular = XMVectorSet(materialSpecular_red[0], materialSpecular_red[1], materialSpecular_red[2], materialSpecular_red[3]);
		constantBuffer.MaterialShininess = materialShinyness_red;
	}

	translationMatrix = XMMatrixTranslation(sphere_translate_x + 3.0f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = XMMatrixScaling(scaleSphere, scaleSphere, scaleSphere);
	worldMatrix = translationMatrix * scaleMatrix;

	constantBuffer.WorldMatrix = worldMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	/********************************************FIVE ROW**************************************************************/
	diiference_y = diiference_y + 1.5f;

	// first sphere
	if (gbLight == true)
	{
		constantBuffer.MaterialAmbient = XMVectorSet(materialAmbient_white[0], materialAmbient_white[1], materialAmbient_white[2], materialAmbient_white[3]);
		constantBuffer.MaterialDiffuse = XMVectorSet(materialDefuse_white[0], materialDefuse_white[1], materialDefuse_white[2], materialDefuse_white[3]);
		constantBuffer.MaterialSpecular = XMVectorSet(materialSpecular_white[0], materialSpecular_white[1], materialSpecular_white[2], materialSpecular_white[3]);
		constantBuffer.MaterialShininess = materialShinyness_white;
	}

	translationMatrix = XMMatrixTranslation(sphere_translate_x - 3.0f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = XMMatrixScaling(scaleSphere, scaleSphere, scaleSphere);
	worldMatrix = translationMatrix * scaleMatrix;

	constantBuffer.WorldMatrix = worldMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	// second sphere
	if (gbLight == true)
	{
		constantBuffer.MaterialAmbient = XMVectorSet(materialAmbient_plastic[0], materialAmbient_plastic[1], materialAmbient_plastic[2], materialAmbient_plastic[3]);
		constantBuffer.MaterialDiffuse = XMVectorSet(materialDefuse_plastic[0], materialDefuse_plastic[1], materialDefuse_plastic[2], materialDefuse_plastic[3]);
		constantBuffer.MaterialSpecular = XMVectorSet(materialSpecular_plastic[0], materialSpecular_plastic[1], materialSpecular_plastic[2], materialSpecular_plastic[3]);
		constantBuffer.MaterialShininess = materialShinyness_plastic;
	}

	translationMatrix = XMMatrixTranslation(sphere_translate_x - 1.0f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = XMMatrixScaling(scaleSphere, scaleSphere, scaleSphere);
	worldMatrix = translationMatrix * scaleMatrix;

	constantBuffer.WorldMatrix = worldMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	// third sphere
	if (gbLight == true)
	{
		constantBuffer.MaterialAmbient = XMVectorSet(materialAmbient_black_2[0], materialAmbient_black_2[1], materialAmbient_black_2[2], materialAmbient_black_2[3]);
		constantBuffer.MaterialDiffuse = XMVectorSet(materialDefuse_black_2[0], materialDefuse_black_2[1], materialDefuse_black_2[2], materialDefuse_black_2[3]);
		constantBuffer.MaterialSpecular = XMVectorSet(materialSpecular_black_2[0], materialSpecular_black_2[1], materialSpecular_black_2[2], materialSpecular_black_2[3]);
		constantBuffer.MaterialShininess = materialShinyness_black_2;
	}

	translationMatrix = XMMatrixTranslation(sphere_translate_x + 1.0f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = XMMatrixScaling(scaleSphere, scaleSphere, scaleSphere);
	worldMatrix = translationMatrix * scaleMatrix;

	constantBuffer.WorldMatrix = worldMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	// forth sphere
	if (gbLight == true)
	{
		constantBuffer.MaterialAmbient = XMVectorSet(materialAmbient_cyan_2[0], materialAmbient_cyan_2[1], materialAmbient_cyan_2[2], materialAmbient_cyan_2[3]);
		constantBuffer.MaterialDiffuse = XMVectorSet(materialDefuse_cyan_2[0], materialDefuse_cyan_2[1], materialDefuse_cyan_2[2], materialDefuse_cyan_2[3]);
		constantBuffer.MaterialSpecular = XMVectorSet(materialSpecular_cyan_2[0], materialSpecular_cyan_2[1], materialSpecular_cyan_2[2], materialSpecular_cyan_2[3]);
		constantBuffer.MaterialShininess = materialShinyness_cyan_2;
	}

	translationMatrix = XMMatrixTranslation(sphere_translate_x + 3.0f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = XMMatrixScaling(scaleSphere, scaleSphere, scaleSphere);
	worldMatrix = translationMatrix * scaleMatrix;

	constantBuffer.WorldMatrix = worldMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	/********************************************SIX ROW**************************************************************/
	diiference_y = diiference_y + 1.5f;

	// first sphere
	if (gbLight == true)
	{
		constantBuffer.MaterialAmbient = XMVectorSet(materialAmbient_green_2[0], materialAmbient_green_2[1], materialAmbient_green_2[2], materialAmbient_green_2[3]);
		constantBuffer.MaterialDiffuse = XMVectorSet(materialDefuse_green_2[0], materialDefuse_green_2[1], materialDefuse_green_2[2], materialDefuse_green_2[3]);
		constantBuffer.MaterialSpecular = XMVectorSet(materialSpecular_green_2[0], materialSpecular_green_2[1], materialSpecular_green_2[2], materialSpecular_green_2[3]);
		constantBuffer.MaterialShininess = materialShinyness_green_2;
	}

	translationMatrix = XMMatrixTranslation(sphere_translate_x - 3.0f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = XMMatrixScaling(scaleSphere, scaleSphere, scaleSphere);
	worldMatrix = translationMatrix * scaleMatrix;

	constantBuffer.WorldMatrix = worldMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	// second sphere
	if (gbLight == true)
	{
		constantBuffer.MaterialAmbient = XMVectorSet(materialAmbient_red_2[0], materialAmbient_red_2[1], materialAmbient_red_2[2], materialAmbient_red_2[3]);
		constantBuffer.MaterialDiffuse = XMVectorSet(materialDefuse_red_2[0], materialDefuse_red_2[1], materialDefuse_red_2[2], materialDefuse_red_2[3]);
		constantBuffer.MaterialSpecular = XMVectorSet(materialSpecular_red_2[0], materialSpecular_red_2[1], materialSpecular_red_2[2], materialSpecular_red_2[3]);
		constantBuffer.MaterialShininess = materialShinyness_red_2;
	}

	translationMatrix = XMMatrixTranslation(sphere_translate_x - 1.0f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = XMMatrixScaling(scaleSphere, scaleSphere, scaleSphere);
	worldMatrix = translationMatrix * scaleMatrix;

	constantBuffer.WorldMatrix = worldMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	// third sphere
	if (gbLight == true)
	{
		constantBuffer.MaterialAmbient = XMVectorSet(materialAmbient_white_2[0], materialAmbient_white_2[1], materialAmbient_white_2[2], materialAmbient_white_2[3]);
		constantBuffer.MaterialDiffuse = XMVectorSet(materialDefuse_white_2[0], materialDefuse_white_2[1], materialDefuse_white_2[2], materialDefuse_white_2[3]);
		constantBuffer.MaterialSpecular = XMVectorSet(materialSpecular_white_2[0], materialSpecular_white_2[1], materialSpecular_white_2[2], materialSpecular_white_2[3]);
		constantBuffer.MaterialShininess = materialShinyness_white_2;
	}

	translationMatrix = XMMatrixTranslation(sphere_translate_x + 1.0f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = XMMatrixScaling(scaleSphere, scaleSphere, scaleSphere);
	worldMatrix = translationMatrix * scaleMatrix;

	constantBuffer.WorldMatrix = worldMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	// forth sphere
	if (gbLight == true)
	{
		constantBuffer.MaterialAmbient = XMVectorSet(materialAmbient_rubber[0], materialAmbient_rubber[1], materialAmbient_rubber[2], materialAmbient_rubber[3]);
		constantBuffer.MaterialDiffuse = XMVectorSet(materialDefuse_rubber[0], materialDefuse_rubber[1], materialDefuse_rubber[2], materialDefuse_rubber[3]);
		constantBuffer.MaterialSpecular = XMVectorSet(materialSpecular_rubber[0], materialSpecular_rubber[1], materialSpecular_rubber[2], materialSpecular_rubber[3]);
		constantBuffer.MaterialShininess = materialShinyness_rubber;
	}

	translationMatrix = XMMatrixTranslation(sphere_translate_x + 3.0f, sphere_translate_y - diiference_y, sphere_translate_z);
	scaleMatrix = XMMatrixScaling(scaleSphere, scaleSphere, scaleSphere);
	worldMatrix = translationMatrix * scaleMatrix;

	constantBuffer.WorldMatrix = worldMatrix;

	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	gpIDXGISwapChain->Present(0, 0);
}

void Uninitialize() {

	//code
	if (pID3DBlob_VertexShaderCode_PerPixel)
	{
		pID3DBlob_VertexShaderCode_PerPixel->Release();
		pID3DBlob_VertexShaderCode_PerPixel = NULL;
	}

	if (pID3DBlob_VertexShaderCode_PerVertex)
	{
		pID3DBlob_VertexShaderCode_PerVertex->Release();
		pID3DBlob_VertexShaderCode_PerVertex = NULL;
	}
	if (gpID3D11Buffer_ConstantBuffer)
	{
		gpID3D11Buffer_ConstantBuffer->Release();
		gpID3D11Buffer_ConstantBuffer = NULL;
	}

	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}

	if (gpID3D11Buffer_Sphere_IndexBuffer)
	{
		gpID3D11Buffer_Sphere_IndexBuffer->Release();
		gpID3D11Buffer_Sphere_IndexBuffer = NULL;
	}

	if (gpID3D11Buffer_Sphere_VertexBuffer_normals)
	{
		gpID3D11Buffer_Sphere_VertexBuffer_normals->Release();
		gpID3D11Buffer_Sphere_VertexBuffer_normals = NULL;
	}

	if (gpID3D11Buffer_Sphere_VertexBuffer_position)
	{
		gpID3D11Buffer_Sphere_VertexBuffer_position->Release();
		gpID3D11Buffer_Sphere_VertexBuffer_position = NULL;
	}

	if (gpID3D11PixelShader_PerPixel)
	{
		gpID3D11PixelShader_PerPixel->Release();
		gpID3D11PixelShader_PerPixel = NULL;
	}

	if (gpID3D11VertexShader_PerPixel)
	{
		gpID3D11VertexShader_PerPixel->Release();
		gpID3D11VertexShader_PerPixel = NULL;
	}

	if (gpID3D11PixelShader_PerVertex)
	{
		gpID3D11PixelShader_PerVertex->Release();
		gpID3D11PixelShader_PerVertex = NULL;
	}

	if (gpID3D11VertexShader_PerVertex)
	{
		gpID3D11VertexShader_PerVertex->Release();
		gpID3D11VertexShader_PerVertex = NULL;
	}

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	if (pIDXGIAdapter)
	{
		pIDXGIAdapter->Release();
		pIDXGIAdapter = NULL;
	}

	if (pIDXGIFactory)
	{
		pIDXGIFactory->Release();
		pIDXGIFactory = NULL;
	}

	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}

	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}

	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
	}

	if (gpFile) {
		fprintf(gpFile, "\nClosing File Successfully");
		fclose(gpFile);
	}

}

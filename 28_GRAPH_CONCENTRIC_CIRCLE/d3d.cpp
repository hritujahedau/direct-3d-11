// header file declaration
#include <windows.h>
#include <stdio.h>
#include <math.h>

#include "d3d.h"
#include <d3d11.h>
#include <d3dcompiler.h>

#pragma warning(disable: 4838)
#include "XNAMath/xnamath.h"

#pragma comment(lib , "d3d11.lib")
#pragma comment(lib , "dxgi.lib")
#pragma comment(lib, "D3dcompiler.lib")

#define WIN_WIDTH 1000
#define WIN_HEIGHT 800

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int width, hight;

float gCleargraphColor[4]; // RGBA
IDXGISwapChain* gpIDXGISwapChain = NULL;
ID3D11Device* gpID3D11Device = NULL;
ID3D11DeviceContext* gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView* gpID3D11RenderTargetView = NULL;

ID3D11VertexShader* gpID3D11VertexShader = NULL;
ID3D11PixelShader* gpID3D11PixelShader = NULL;
ID3D11Buffer* gpID3D11Buffer_Line_VertexBuffer_position = NULL;
ID3D11Buffer* gpID3D11Buffer_Line_VertexBuffer_Color = NULL;

ID3D11InputLayout* gpID3D11InputLayout = NULL;
ID3D11Buffer* gpID3D11Buffer_ConstantBuffer = NULL;

ID3D11RasterizerState* gpID3D11RasterizerState = NULL;

ID3D11DepthStencilView* gpID3D11DepthStencilView = NULL;

struct CBUFFER
{
	XMMATRIX WorldViewProjectionMatrix;			//XMMATRIX - xna math matrix 
};

XMMATRIX gPerspectiveProjectionMatrix;

IDXGIFactory* pIDXGIFactory = NULL;
IDXGIAdapter* pIDXGIAdapter = NULL;

DXGI_ADAPTER_DESC dxgiAdapterDesc;
CHAR str[255];

float graphColor[] = { 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f };
float vertices[] = { 1.0f, 0.0, 0.0f, -1.0f, 0.0f, 0.0f };

//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	HRESULT Initialize();
	void Uninitialize();
	void Display();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("h_direct3D11");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 
	if (fopen_s(&gpFile, "windows.log", "w") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("Direct3D11 Window"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIN_WIDTH/2 ,
		monitorRC.bottom / 2 - WIN_HEIGHT/2 ,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	HRESULT hr;
	hr = Initialize();
	if (FAILED(hr))
	{
		fprintf(gpFile, "\ninitialize() failed.Exiting Now..\n");
		fclose(gpFile);
		DestroyWindow(ghwnd);
		hwnd = NULL;
	}

	// game loop
	while (gbDone == false) 
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 
		{
			if (msg.message == WM_QUIT) 
			{
				gbDone = true;
			}
			else 
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else 
		{
			Display();
			if (gbActiveWindow) 
			{
				if (gbEscapeKeyIsPressed == true)
				{
					gbDone = true;
				}
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	HRESULT Resize(int,int);

	HRESULT hr;

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			if (gpID3D11DeviceContext)
			{
				hr = Resize(LOWORD(lParam), HIWORD(lParam));
				if (FAILED(hr))
				{
					fprintf(gpFile, "\nResize() failed");
					fflush(gpFile);
					DestroyWindow(ghwnd);
					return hr;
				}
				else
				{
					fprintf(gpFile, "\nResize() successfull");
					fflush(gpFile);
				}
			}
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				if (gbEscapeKeyIsPressed == false)
				{
					gbEscapeKeyIsPressed = true;
				}
				//DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

HRESULT Initialize() {
	// function declaration
	HRESULT Resize(int, int);
	void Uninitialize();

	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE, D3D_DRIVER_TYPE_WARP, D3D_DRIVER_TYPE_REFERENCE
	};

	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0; // default, lowest

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;	//based upon d3dFeatureLevel_required

	// code
	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]); // calculation size of array
	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;

	ZeroMemory((void*)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	dxgiSwapChainDesc.BufferCount = 1;
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd;
	// MSAA - Multi Sampling Ani Alizing 
	dxgiSwapChainDesc.SampleDesc.Count = 1; // Max 8 2^8 sampling
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,								// adapter
			d3dDriverType,						// Driver Type
			NULL,								// Software
			createDeviceFlags,					// Flags
			&d3dFeatureLevel_required,			// Feature Levels
			numFeatureLevels,					// Num Feature Levels
			D3D11_SDK_VERSION,					// sdk verstion
			&dxgiSwapChainDesc,					// Swap Chain Desc
			&gpIDXGISwapChain,					// Swap chain
			&gpID3D11Device,					// Device
			&d3dFeatureLevel_acquired,			// Feature Level
			&gpID3D11DeviceContext				// device conext
		);

		if (SUCCEEDED(hr))
		{
			break;
		}
	}

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nD3D11CretaeDeviceAndSwapChain() Failed.");
		fflush(gpFile);
		//DestroyWindow(ghwnd);
		return hr;
	}
	else
	{
		fprintf(gpFile, "\nD3D11CreateDeviceAndSwapChain() Successded");
		fprintf(gpFile, "\nThe chosen driver is of");
		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf(gpFile, "\nHardware type.");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf(gpFile, "\nWarp type");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf(gpFile, "\nUnkown type");
		}

		fprintf(gpFile, "\nThe supported highest feature level is ");
		if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf(gpFile, "11.0");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf(gpFile, "10.1");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf(gpFile, "10.0");
		}
		else
		{
			fprintf(gpFile, "Unknown version");
		}
		fflush(gpFile);
	} // else closed

	hr = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&pIDXGIFactory);
	if (FAILED(hr))
	{
		fprintf(gpFile, "\nFailed to get pIDXGIFactory interface");
		fflush(gpFile);
		return hr;
	}

	if (pIDXGIFactory->EnumAdapters(1, &pIDXGIAdapter) == DXGI_ERROR_NOT_FOUND)
	{
		fprintf(gpFile, "\nFailed to get dxgi adapter");
		fflush(gpFile);
		return hr;
	}

	memset((void**)&dxgiAdapterDesc, 0, sizeof(DXGI_ADAPTER_DESC));

	hr = pIDXGIAdapter->GetDesc(&dxgiAdapterDesc);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\ndxgiAdapterDesc Failed to get");
		fflush(gpFile);
		return hr;
	}

	WideCharToMultiByte(CP_ACP, 0, dxgiAdapterDesc.Description, 255, str, 255, NULL, NULL);

	fprintf(gpFile, "\nGraphics card name is %s", str);
	fprintf(gpFile, "\nVideo Memory Size of graphics card is %d gb", int(ceil(dxgiAdapterDesc.DedicatedVideoMemory / 1024.0 / 1024.0 / 1024.0)));
	fflush(gpFile);

	// 1: declare vertex shader source code

	const char* vertexShaderSourceCode =
		" cbuffer ConstantBuffer																					" \
		" {																											" \
		"		float4x4 worldViewProjectionMatrix;																	" \
		" }																											" \
		" struct vertex_out{																						" \
		"		float4 position: SV_POSITION ;																		" \
		"		float4 graphColor: graphColor;																				" \
		" };																										" \
		" vertex_out main(float4 pos: POSITION, float4 col: graphColor)													" \
		" {																											" \
		"		vertex_out output;																					" \
		"       output.position = mul(worldViewProjectionMatrix, pos);												" \
		"		output.graphColor = col;																					" \
		"		return (output);																					" \
		" }																											" ;
	//float4 position = mul(worldViewProjectionMatrix, pos);
	ID3DBlob* pID3DBlob_VertexShaderCode = NULL;			//irrespective version
	ID3DBlob* pID3DBlob_Error = NULL;

	// 2. compile vertex shader source code and get compiler converted vertex
	// D3DCompile not only compile shaders but also compiles
	hr = D3DCompile(
		vertexShaderSourceCode,
		lstrlenA(vertexShaderSourceCode) + 1,
		"VS",
		NULL,									// pass MACRO OF D3D_SHADER_MACRO*
		D3D_COMPILE_STANDARD_FILE_INCLUDE,		// take your standard file which neeed, we are not passing 
		"main",									// entry point function name
		"vs_5_0",								// vertex shader feature level is vs_5_0
		0,										//	no deugging , profileing , optimization, row column measurement
		0,										// don't consider it as special effect (effect is advance version of shader)
		&pID3DBlob_VertexShaderCode,			// consider as shader
		&pID3DBlob_Error						// get errors if any
	);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fprintf(gpFile, "\nD3DCompile() Failed for vertex shader : %s.\n",(char *)pID3DBlob_Error->GetBufferPointer());
			fflush(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return hr;
		}
		else
		{
			// com error
			fprintf(gpFile, "\nSome COM error");
			fflush(gpFile);
		}
	}
	
	fprintf(gpFile, "\nD3DCompile() successfull for Vertex Shader");
	fflush(gpFile);
	
	hr = gpID3D11Device->CreateVertexShader(
		pID3DBlob_VertexShaderCode->GetBufferPointer(),
		pID3DBlob_VertexShaderCode->GetBufferSize(),
		NULL,											// ID3D11ClassInstance*
		&gpID3D11VertexShader
		);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreateVertexShader() Failed\n");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nID3D11Device::CreateVertexShader() Successfull");
	fflush(gpFile);

	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader, 0, 0);
	// 2 - ID3D11ClassInstance how many elements
	// 3 - if 2 array give count

	const char* pixelShaderSourceCode =
		" struct vertex_out{																		" \
		"		float4 position: SV_POSITION ;														" \
		"		float4 graphColor: graphColor;																" \
		" };																						" \
		" float4 main(vertex_out input) : SV_TARGET													" \
		" {																							" \
		"		float4 graphColor = input.graphColor;															" \
		"		return graphColor;																		" \
		" }																							" ;

	ID3DBlob* pID3DBlob_PixelShaderCode = NULL;
	pID3DBlob_Error = NULL;
	hr = D3DCompile(pixelShaderSourceCode,
		lstrlenA(pixelShaderSourceCode) + 1,
		"PS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&pID3DBlob_PixelShaderCode,
		&pID3DBlob_Error
	);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fprintf(gpFile, "D3DCompile() Failed For Pixel Shader : %s \n", (char *)pID3DBlob_Error->GetBufferPointer());
			fflush(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return hr;
		}
		else
		{
			// com error
			fprintf(gpFile, "\nSome COM error");
			fflush(gpFile);
		}
	}

	fprintf(gpFile, "\nD3DCompile() Successful For Pixel Shader");

	hr = gpID3D11Device->CreatePixelShader(
		pID3DBlob_PixelShaderCode->GetBufferPointer(),
		pID3DBlob_PixelShaderCode->GetBufferSize(),
		NULL,
		&gpID3D11PixelShader
		);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreatePixelShader() Failed.");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nID3D11Device::CreatePixelShader() Successfull.");
	fflush(gpFile);

	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader, 0, 0);
	if (pID3DBlob_PixelShaderCode)
	{
		pID3DBlob_PixelShaderCode->Release();
		pID3DBlob_PixelShaderCode = NULL;
	}

	if (pID3DBlob_Error)
	{
		pID3DBlob_Error->Release();
		pID3DBlob_Error = NULL;
	}

	// create and set input layout 
	D3D11_INPUT_ELEMENT_DESC inputElementDesc[2];

	//ZeroMemory((void*)&inputElementDesc, sizeof(D3D11_INPUT_ELEMENT_DESC));

	inputElementDesc[0].SemanticName = "POSITION";
	inputElementDesc[0].SemanticIndex = 0;
	inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[0].InputSlot = 0;
	inputElementDesc[0].AlignedByteOffset = 0;
	inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[0].InstanceDataStepRate = 0;

	// 2nd

	inputElementDesc[1].SemanticName = "graphColor";
	inputElementDesc[1].SemanticIndex = 0;
	inputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[1].InputSlot = 1;
	inputElementDesc[1].AlignedByteOffset = 0;
	inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[1].InstanceDataStepRate = 0;

	hr = gpID3D11Device->CreateInputLayout(
		inputElementDesc,
		_ARRAYSIZE(inputElementDesc),
		pID3DBlob_VertexShaderCode->GetBufferPointer(),
		pID3DBlob_VertexShaderCode->GetBufferSize(),
		&gpID3D11InputLayout
	);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreateInputLayout() Failed");
		fflush(gpFile);

		pID3DBlob_VertexShaderCode->Release();
		pID3DBlob_VertexShaderCode = NULL;

		return hr;
	}

	fprintf(gpFile, "\nUID3D11Device::CreateInputLayout() Successfull");
	fflush(gpFile);

	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);
	pID3DBlob_VertexShaderCode->Release();
	pID3DBlob_VertexShaderCode = NULL;

	/////////////////////////////////////////////// TRIANGLE ////////////////////////////////////////////////////
	
	// vertex and graphColor buffer
	

	// create vertex buffer
	D3D11_BUFFER_DESC bufferDesc;
	ZeroMemory(&bufferDesc, sizeof(D3D11_BUFFER_DESC));	
	bufferDesc.ByteWidth = sizeof(float) * 6;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11Buffer_Line_VertexBuffer_position);
	
	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreateBuffer() Failed For Vertex Buffer for position\n");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nID3D11Device::CreateBuffer() successfull For Vertex Buffer for position\n");
	fflush(gpFile);

	ZeroMemory((void**)&bufferDesc, sizeof(D3D11_BUFFER_DESC));
	bufferDesc.ByteWidth = sizeof(float) * 6;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11Buffer_Line_VertexBuffer_Color);
	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreateBuffer() Failed For Vertex Buffer for graphColor");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nID3D11Device::CreateBuffer() successfull For Vertex Buffer for graphColor\n");
	fflush(gpFile);

	// define and set constant buffer
	// uniform buffer
	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
	ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(D3D11_BUFFER_DESC));
	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	
	hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer, nullptr, &gpID3D11Buffer_ConstantBuffer);
	
	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreateBuffer() Failed For Constant Buffer.");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nID3D11Device::CreateBuffer() Successfull For Constant Buffer.");
	fflush(gpFile);

	gpID3D11DeviceContext->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);

	// CREATE AND SET RASTERIZER STATE

	D3D11_RASTERIZER_DESC d3d11_rasterizer_desc;

	ZeroMemory((void*)&d3d11_rasterizer_desc, sizeof(D3D11_RASTERIZER_DESC));

	d3d11_rasterizer_desc.AntialiasedLineEnable = FALSE;
	d3d11_rasterizer_desc.CullMode = D3D11_CULL_NONE;
	d3d11_rasterizer_desc.DepthBias = 0;
	d3d11_rasterizer_desc.DepthBiasClamp = 0.0f;
	d3d11_rasterizer_desc.DepthClipEnable = TRUE;
	d3d11_rasterizer_desc.FillMode = D3D11_FILL_SOLID;
	d3d11_rasterizer_desc.FrontCounterClockwise = FALSE;
	d3d11_rasterizer_desc.MultisampleEnable = FALSE;
	d3d11_rasterizer_desc.ScissorEnable = FALSE;
	d3d11_rasterizer_desc.SlopeScaledDepthBias = 0.0f;

	hr = gpID3D11Device->CreateRasterizerState(&d3d11_rasterizer_desc, &gpID3D11RasterizerState);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\n CreateRasterizerState() Failed");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\n CreateRasterizerState() sucsess");
	fflush(gpFile);

	gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState);

	gCleargraphColor[0] = 0.0f;
	gCleargraphColor[1] = 0.0f;
	gCleargraphColor[2] = 0.0f;
	gCleargraphColor[3] = 1.0f;

	gPerspectiveProjectionMatrix = XMMatrixIdentity();

	hr = Resize(WIN_WIDTH, WIN_HEIGHT);
	if (FAILED(hr))
	{
		fprintf(gpFile, "\nResize() Failed");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nResize successfull");
	fflush(gpFile);

	return S_OK;
}

HRESULT Resize(int width, int height) 
{
	HRESULT hr = S_OK;

	if (height == 0)
	{
		height = 1;
	}

	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	gpIDXGISwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

	ID3D11Texture2D* pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*) &pID3D11Texture2D_BackBuffer);

	// again get render target view from d3d11 using above back buffer
	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL, &gpID3D11RenderTargetView);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreateTargetView() Failed.");
		fflush(gpFile);
		return hr;
	}
	else
	{
		fprintf(gpFile, "\nID3D11Device::CreateTargetView() Successded.");
		fflush(gpFile);
	}

	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	// just like RTV ,  DSV also need texture buffer

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	// create and intialize
	D3D11_TEXTURE2D_DESC d3d11_texture2D_desc;

	ZeroMemory((void*)&d3d11_texture2D_desc, sizeof(D3D11_TEXTURE2D_DESC));

	d3d11_texture2D_desc.Width = (UINT) width;
	d3d11_texture2D_desc.Height = (UINT) height;
	d3d11_texture2D_desc.Format = DXGI_FORMAT_D32_FLOAT;
	d3d11_texture2D_desc.Usage = D3D11_USAGE_DEFAULT;
	d3d11_texture2D_desc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	d3d11_texture2D_desc.SampleDesc.Count = 1;	// this can be 1-4 as per needed capacity
	d3d11_texture2D_desc.SampleDesc.Quality = 0; // default
	d3d11_texture2D_desc.ArraySize = 1;
	d3d11_texture2D_desc.MipLevels = 1; // default;
	d3d11_texture2D_desc.CPUAccessFlags = 0; // default
	d3d11_texture2D_desc.MiscFlags = 0;

	// create texture2D as depth buffer
	ID3D11Texture2D* pID3D11Texture2D_depthbuffer;

	hr = gpID3D11Device->CreateTexture2D(&d3d11_texture2D_desc, NULL, &pID3D11Texture2D_depthbuffer);
	if (FAILED(hr))
	{
		fprintf(gpFile, "\n CreateTexture2D failed for depth");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\n CreateTexture2D successfully for depth");
	fflush(gpFile);

	// intialize depth stencil view desc
	D3D11_DEPTH_STENCIL_VIEW_DESC d3d11_depth_stencil_view_desc;

	ZeroMemory((void*)&d3d11_depth_stencil_view_desc, sizeof(d3d11_depth_stencil_view_desc));

	d3d11_depth_stencil_view_desc.Format = DXGI_FORMAT_D32_FLOAT;
	d3d11_depth_stencil_view_desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS; // TEXTURE2D MS - multi sample

	// create dsv
	hr = gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D_depthbuffer, &d3d11_depth_stencil_view_desc, &gpID3D11DepthStencilView);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\n CretaeDepthstencilView() failed");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\n CretaeDepthstencilView() successfully");
	fflush(gpFile);

	// set render target view as render target
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, gpID3D11DepthStencilView);

	// set viewport target view
	D3D11_VIEWPORT d3dViewPort;
	
	ZeroMemory((void*)&d3dViewPort, sizeof(D3D11_VIEWPORT));

	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f; // glClearDepth(1.0)
	
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);
	
	// set orthographic matrix
	gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)width / (float)height, 0.1f, 100.0f);

	return hr;
}

void Display()
{
	// function declarations
	void graph();
	void hallowCircle();

	// code
	static float rotateAngle = 0.0f;

	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gCleargraphColor);
	gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, D3D11_CLEAR_DEPTH);

	/////////////////////////////// TRIANGLE /////////////////////////////
	
	// select which vertex buffer to display
	UINT stride = sizeof(float) * 3;																					// jump
	UINT offset = 0;
	gpID3D11DeviceContext->IASetVertexBuffers(0, 1, &gpID3D11Buffer_Line_VertexBuffer_position, &stride, &offset);

	stride = sizeof(float) * 3;
	offset = 0;
	gpID3D11DeviceContext->IASetVertexBuffers(1,  1, &gpID3D11Buffer_Line_VertexBuffer_Color, &stride, &offset);

	// select geometry primitive
	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP);

	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX translateMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.0f);
	XMMATRIX viewMatrix = translateMatrix;
		
	// final world view projection matrix
	XMMATRIX wvpMatrix = worldMatrix * viewMatrix * gPerspectiveProjectionMatrix;

	// load the data into constant buffer
	CBUFFER	 constantBuffer;
	
	ZeroMemory((void*)&constantBuffer, sizeof(CBUFFER));

	constantBuffer.WorldViewProjectionMatrix = wvpMatrix;
	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);  // glUniformMatrix4fv()

	hallowCircle();
	graph();

	gpIDXGISwapChain->Present(0, 0);
}

void hallowCircle()
{
	// variable declarations
	float circleLines[] = {
							0.0f, 0.0f, 0.0f,
							0.0f, 0.0f, 0.0f
						};

	float circleColor[] = { 
							1.0f, 1.0f, 0.0f, 
							1.0f, 1.0f, 0.0f,
						};
	float circle_points = 1500, angle, radius = 0.0f;

	D3D11_MAPPED_SUBRESOURCE mappedSubresource;

	// code
	ZeroMemory((void**)&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_Line_VertexBuffer_Color, NULL, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, circleColor, sizeof(circleColor));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_Line_VertexBuffer_Color, NULL);

	for (radius = 0.04; radius <= 0.4; radius += 0.04f)
	{
		for (int i = 1; i < circle_points; i++)
		{
			angle = (i - 1) * 0.01745329;
			circleLines[0] = radius * cos(angle);
			circleLines[1] = radius * sin(angle);
			circleLines[2] = 0.0f;
			angle = i * 0.01745329;
			circleLines[3] = radius * cos(angle);
			circleLines[4] = radius * sin(angle);
			circleLines[5] = 0.0f;

			ZeroMemory((void**)&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
			gpID3D11DeviceContext->Map(gpID3D11Buffer_Line_VertexBuffer_position, NULL, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
			memcpy(mappedSubresource.pData, circleLines, sizeof(circleLines));
			gpID3D11DeviceContext->Unmap(gpID3D11Buffer_Line_VertexBuffer_position, NULL);

			gpID3D11DeviceContext->Draw(2, 0);
		}
	}
}

void graph()
{
	// variable declarations
	D3D11_MAPPED_SUBRESOURCE mappedSubresource;
	float distanceForX = 0, distanceForY = 0;

	// set green graphColor
	graphColor[0] = 0.0f;
	graphColor[1] = 1.0f;
	graphColor[2] = 0.0f;
	graphColor[3] = 0.0f;
	graphColor[4] = 1.0f;
	graphColor[5] = 0.0f;

	ZeroMemory((void**)&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_Line_VertexBuffer_Color, NULL, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, graphColor, sizeof(graphColor));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_Line_VertexBuffer_Color, NULL);

	// horizontal green light
	vertices[0] = 1.5f;
	vertices[1] = 0.0f;
	vertices[2] = 0.0f;

	vertices[3] = -1.5f;
	vertices[4] = 0.0f;
	vertices[5] = 0.0f;

	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_Line_VertexBuffer_position, NULL, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, vertices, sizeof(vertices));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_Line_VertexBuffer_position, NULL);

	gpID3D11DeviceContext->Draw(2, 0);

	// verticle green line
	vertices[0] = 0.0f;
	vertices[1] = 1.5f;
	vertices[2] = 0.0f;

	vertices[3] = 0.0f;
	vertices[4] = -1.5f;
	vertices[5] = 0.0f;

	ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_Line_VertexBuffer_position, NULL, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, vertices, sizeof(vertices));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_Line_VertexBuffer_position, NULL);

	gpID3D11DeviceContext->Draw(2, 0);

	// set blue graphColor
	graphColor[0] = 0.0f;
	graphColor[1] = 0.0f;
	graphColor[2] = 1.0f;
	graphColor[3] = 0.0f;
	graphColor[4] = 0.0f;
	graphColor[5] = 1.0f;

	ZeroMemory((void**)&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_Line_VertexBuffer_Color, NULL, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, graphColor, sizeof(graphColor));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_Line_VertexBuffer_Color, NULL);

	for (int i = 0; i < 20; i++) {

		// verticle up
		vertices[0] = 1.5f;
		vertices[1] = distanceForX;
		vertices[2] = 0.0f;

		vertices[3] = -1.5f;
		vertices[4] = distanceForX;
		vertices[5] = 0.0f;

		ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
		gpID3D11DeviceContext->Map(gpID3D11Buffer_Line_VertexBuffer_position, NULL, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
		memcpy(mappedSubresource.pData, vertices, sizeof(vertices));
		gpID3D11DeviceContext->Unmap(gpID3D11Buffer_Line_VertexBuffer_position, NULL);

		gpID3D11DeviceContext->Draw(2, 0);

		// verticle down		

		vertices[0] = 1.5f;
		vertices[1] = -distanceForX;
		vertices[2] = 0.0f;

		vertices[3] = -1.5f;
		vertices[4] = -distanceForX;
		vertices[5] = 0.0f;

		ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
		gpID3D11DeviceContext->Map(gpID3D11Buffer_Line_VertexBuffer_position, NULL, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
		memcpy(mappedSubresource.pData, vertices, sizeof(vertices));
		gpID3D11DeviceContext->Unmap(gpID3D11Buffer_Line_VertexBuffer_position, NULL);

		gpID3D11DeviceContext->Draw(2, 0);

		// horizontal right	

		vertices[0] = -distanceForY;
		vertices[1] = 1.5;
		vertices[2] = 0.0f;

		vertices[3] = -distanceForY;
		vertices[4] = -1.5f;
		vertices[5] = 0.0f;

		ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
		gpID3D11DeviceContext->Map(gpID3D11Buffer_Line_VertexBuffer_position, NULL, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
		memcpy(mappedSubresource.pData, vertices, sizeof(vertices));
		gpID3D11DeviceContext->Unmap(gpID3D11Buffer_Line_VertexBuffer_position, NULL);

		gpID3D11DeviceContext->Draw(2, 0);

		// horizontal left
		vertices[0] = distanceForY;
		vertices[1] = 1.5;
		vertices[2] = 0.0f;

		vertices[3] = distanceForY;
		vertices[4] = -1.5f;
		vertices[5] = 0.0f;

		ZeroMemory(&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));
		gpID3D11DeviceContext->Map(gpID3D11Buffer_Line_VertexBuffer_position, NULL, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
		memcpy(mappedSubresource.pData, vertices, sizeof(vertices));
		gpID3D11DeviceContext->Unmap(gpID3D11Buffer_Line_VertexBuffer_position, NULL);

		gpID3D11DeviceContext->Draw(2, 0);

		distanceForX += (float)1 / 46;
		distanceForY += (float)1 / 25;

	}
}

void Uninitialize() {

	//code

	if (gpID3D11Buffer_ConstantBuffer)
	{
		gpID3D11Buffer_ConstantBuffer->Release();
		gpID3D11Buffer_ConstantBuffer = NULL;
	}

	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}

	if (gpID3D11Buffer_Line_VertexBuffer_Color)
	{
		gpID3D11Buffer_Line_VertexBuffer_Color->Release();
		gpID3D11Buffer_Line_VertexBuffer_Color = NULL;
	}

	if (gpID3D11Buffer_Line_VertexBuffer_position)
	{
		gpID3D11Buffer_Line_VertexBuffer_position->Release();
		gpID3D11Buffer_Line_VertexBuffer_position = NULL;
	}

	if (gpID3D11PixelShader)
	{
		gpID3D11PixelShader->Release();
		gpID3D11PixelShader = NULL;
	}

	if (gpID3D11VertexShader)
	{
		gpID3D11VertexShader->Release();
		gpID3D11VertexShader = NULL;
	}

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	if (pIDXGIAdapter)
	{
		pIDXGIAdapter->Release();
		pIDXGIAdapter = NULL;
	}

	if (pIDXGIFactory)
	{
		pIDXGIFactory->Release();
		pIDXGIFactory = NULL;
	}

	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}

	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}

	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
	}

	if (gpFile) {
		fprintf(gpFile, "\nClosing File Successfully");
		fclose(gpFile);
	}

}

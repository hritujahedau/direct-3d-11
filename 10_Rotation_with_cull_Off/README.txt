1. Copy Trianlge rotation code

Global Variable Changes

1. ID3D11RasterizerState *gpID3D11RasterizerState = NULL;

In Initialization()

1. create D3D11_RASTERIZER_DESC object and intialize it as follow:

	d3d11_rasterizer_desc.AntialiasedLineEnable = FALSE;
	d3d11_rasterizer_desc.CullMode = D3D11_CULL_NONE;
	d3d11_rasterizer_desc.DepthBias = 0;
	d3d11_rasterizer_desc.DepthBiasClamp = 0.0f;
	d3d11_rasterizer_desc.DepthClipEnable = TRUE;
	d3d11_rasterizer_desc.FillMode = D3D11_FILL_SOLID;
	d3d11_rasterizer_desc.FrontCounterClockwise = FALSE;
	d3d11_rasterizer_desc.MultisampleEnable = FALSE;
	d3d11_rasterizer_desc.ScissorEnable = FALSE;
	d3d11_rasterizer_desc.SlopeScaledDepthBias = 0.0f;

2. call CreateRasterizerState and pass D3D11_RASTERIZER_DESC object and empty ID3D11RasterizerState Interface pointer
	gpID3D11Device->CreateRasterizerState(&d3d11_rasterizer_desc, &gpID3D11RasterizerState);
	
3. Set rasterizer as following
	gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState);
	
In Unintialize()

1. Release  gpID3D11RasterizerState
	

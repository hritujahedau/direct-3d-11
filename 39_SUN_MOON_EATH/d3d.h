#pragma once
#include <windows.h>

#define MYICON 1287
#define IDI_MYICON MAKEINTRESOURCE(MYICON)

typedef struct node
{
	XMMATRIX matrix;
	struct node* prev;
} node;

node* head = NULL;

1. Copy Colored triangle program

----------Global Changes---------

1. Rename gpID3D11Buffer_VertexBuffer_position and gpID3D11Buffer_VertexBuffer_color to 
		  gpID3D11Buffer_Triangle_VertexBuffer_position and gpID3D11Buffer_Triangle_VertexBuffer_color
		  
2. Add new 2 variables 

ID3D11Buffer* gpID3D11Buffer_Rectangle_VertexBuffer_position = NULL;
ID3D11Buffer* gpID3D11Buffer_Rectangle_VertexBuffer_color = NULL;

In Inititalization()

1. Create vertex buffer for position and color and copy data to buffer
2. Do 3rd step for both triangle and rectangle


-----------In Display()--------
For Triangle
1. translate triangle x to -1.5

For Rectangle
1. Set vertexbuffer(position and color) 
2. IASetVertexBuffers()
3. IASetPrimitiveTopology TO D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST
4. Set translation x to 1.5
5. gpID3D11DeviceContext->Draw(6, 0);

----------In Uninitialize()-----
1. Release  following interfaces
	gpID3D11Buffer_Triangle_VertexBuffer_position
	gpID3D11Buffer_Triangle_VertexBuffer_color
	gpID3D11Buffer_Rectangle_VertexBuffer_position
	gpID3D11Buffer_Rectangle_VertexBuffer_color

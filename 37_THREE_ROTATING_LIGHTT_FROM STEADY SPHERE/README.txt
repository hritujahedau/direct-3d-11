---- Global changes ------

1. Change in CBUFFER as below:

		struct CBUFFER
		{
			XMMATRIX WorldMatrix;			
			XMMATRIX ViewMatrix;
			XMMATRIX ProjectionMatrix;
		
			XMVECTOR LightAmbient;
			XMVECTOR LightDiffuse;
			XMVECTOR LightSpecular;
			XMVECTOR LightPosition;
		
			XMVECTOR MaterialAmbient;
			XMVECTOR MaterialDiffuse;
			XMVECTOR MaterialSpecular;
			float MaterialShininess;
		
			unsigned int KeyPressed;
		};

2. add boolean variable for light and animation

		bool gbLight = false, gbAnimate = false;

3. Add light and material array values as below

		float LightAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
		float LightDiffuse[] = { 0.5f, 0.2f, 0.7f, 1.0f };
		float LightSpecular[] = { 0.7f, 0.7f, 0.7f, 1.0f };
		float LightPosition[] = { 100.0f, 100.0f , 100.0f, 1.0f };
		
		float MaterialAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
		float MaterialDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		float MaterialSpecular[] = { 1.0f, 1.0f, 1.0f , 1.0f};
		float MaterialShininess = 58.0f;
		
4. rename gpID3D11VertexShader and gpID3D11PixelShader to gpID3D11VertexShader_PerVertex and gpID3D11PixelShader_PerVertex respectively
----------------------------------- changes in WndProc -----------------------------------		

1. toggle light and animation on 'l' and 'a' keys

----------------------------------- changes in Initialization -----------------------------------

1. changes in vertex shader

		--- global changes ----
		
		i) change in cbuffer ConstantBuffer as below
		
		cbuffer ConstantBuffer																																
		{																																					
				float4x4 worldMatrix;																														
				float4x4 viewMatrix;																														
				float4x4 projectionMatrix;																													
				float4 lightAmbient;																														
				float4 lightDiffuse;																														
				float4 lightSpecular;																														
				float4 lightPosition;																														
				float4 materialAmbient;																														
				float4 materialDiffuse;																														
				float4 materialSpecular;																													
				float materialShininess;																													
				uint keyPressed;																															
		}																																					
		
		ii) Add in vertex_out
		
				struct vertex_out							
				{											
					float4 position : SV_POSITION;			
					float3 phoung_ads_light : COLOR;		
				};											
		
		
		iii) Change in main() declaration, now accept normals as second parameter as below
		
				vertex_out main(float4 pos: POSITION, float3 normals : NORMAL)
		
		----- below changes are in main() ---------
		iv) declare vertex_out variable
		
				struct vertex_out output;
		
		v) do light calculation as per keyPressed value as below
		
		if( keyPressed == 1)																															
		{																																				
			float4 eye_position = mul ( worldMatrix , pos);																							
			eye_position = mul( viewMatrix, eye_position);																							
			float3 transformed_normals = normalize ( mul ((float3x3) mul ( viewMatrix,  worldMatrix ), (float3)  normals) ) ;						
			float3 light_direction = normalize ( (float3) (lightPosition - eye_position) );															
			float3 reflection_vector =  reflect ( -light_direction , transformed_normals) ;															
			float3 viewVector = normalize ( -eye_position.xyz);																						
			float3 ambient = (float3) lightAmbient *  (float3) materialAmbient;																						
			float3 diffuse = (float3) lightDiffuse * (float3) materialDiffuse * max ( dot ( light_direction, transformed_normals), 0.0);							
			float3 specular = (float3)lightSpecular * (float3) materialSpecular * pow ( max ( dot ( reflection_vector, viewVector) , 0.0 ) , materialShininess )  ;	
			output.phoung_ads_light = ambient + diffuse + specular;																					
		}																																				
		else																																			
		{																																				
			output.phoung_ads_light = float3(1.0, 1.0, 1.0);																						
		}																																				
		
		
		vi) calculate position 
				projectionMatrix * viewMatrix * worldMatrix * position 
		
		float4 position = mul(worldMatrix, pos);			
		position = mul ( viewMatrix, position);		
		position = mul ( projectionMatrix, position);
		output.position = position;					
															
		vii) return output

2) changes in pixel shader

		--------- global changes ---------
		
		1. Copy vertex_out from vertex shader
		2. Changes in main declarations
		
		float4 main(struct vertex_out input) : SV_TARGET
		
		3. float4 color = float4 ( input.phoung_ads_light, 1.0) ;
		4. return color


-------------- change in Display() -------------- 
1. pass light value as per key state
		if (gbLight == true)
		{
			constantBuffer.KeyPressed = 1;
	
			constantBuffer.LightAmbient = XMVectorSet(LightAmbient[0], LightAmbient[1], LightAmbient[2], LightAmbient[3]);
			constantBuffer.LightDiffuse = XMVectorSet(LightDiffuse[0], LightDiffuse[1], LightDiffuse[2], LightDiffuse[3]);
			constantBuffer.LightSpecular = XMVectorSet(LightSpecular[0], LightSpecular[1], LightSpecular[2], LightSpecular[3]);
			constantBuffer.LightPosition = XMVectorSet(LightPosition[0], LightPosition[1], LightPosition[2], LightPosition[3]);
	
			constantBuffer.MaterialAmbient = XMVectorSet(MaterialAmbient[0], MaterialAmbient[1], MaterialAmbient[2], MaterialAmbient[3]);
			constantBuffer.MaterialDiffuse = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
			constantBuffer.MaterialSpecular = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
			constantBuffer.MaterialShininess = MaterialShininess;
		}
		else
		{
			constantBuffer.KeyPressed = 0;
		}

2. set worldMatrix, viewMatrix, projectionMatrix matrix

-------------- In Unintialize() --------------
1. No change


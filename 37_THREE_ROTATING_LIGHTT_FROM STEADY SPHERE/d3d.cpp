// header file declaration
#include <windows.h>
#include <stdio.h>
#include <math.h>

#include "d3d.h"
#include <d3d11.h>
#include <d3dcompiler.h>

#pragma warning(disable: 4838)
#include "XNAMath/xnamath.h"

#include "Sphere.h"

#pragma comment(lib , "d3d11.lib")
#pragma comment(lib , "dxgi.lib")
#pragma comment(lib, "D3dcompiler.lib")
#pragma comment(lib, "Sphere.lib")

#define WIN_WIDTH 1000
#define WIN_HEIGHT 800

//global function declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
FILE *gpFile = NULL;
HWND ghwnd = NULL;
bool gbDone = false;
bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
DWORD dwStyle;
bool gbFullScreen = false;
HDC ghdc;
HGLRC ghrc;
int width, hight;

float gClearColor[4]; // RGBA
IDXGISwapChain* gpIDXGISwapChain = NULL;
ID3D11Device* gpID3D11Device = NULL;
ID3D11DeviceContext* gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView* gpID3D11RenderTargetView = NULL;

ID3D11VertexShader* gpID3D11VertexShader_PerVertex = NULL;
ID3D11PixelShader* gpID3D11PixelShader_PerVertex = NULL;

ID3D11VertexShader* gpID3D11VertexShader_PerPixel = NULL;
ID3D11PixelShader* gpID3D11PixelShader_PerPixel = NULL;

D3D11_INPUT_ELEMENT_DESC inputElementDesc[2];

ID3DBlob* pID3DBlob_VertexShaderCode_PerPixel = NULL;
ID3DBlob* pID3DBlob_VertexShaderCode_PerVertex = NULL;


ID3D11Buffer* gpID3D11Buffer_Sphere_VertexBuffer_position = NULL;
ID3D11Buffer* gpID3D11Buffer_Sphere_VertexBuffer_normals = NULL;
ID3D11Buffer* gpID3D11Buffer_Sphere_IndexBuffer = NULL;

ID3D11InputLayout* gpID3D11InputLayout = NULL;
ID3D11Buffer* gpID3D11Buffer_ConstantBuffer = NULL;

ID3D11RasterizerState* gpID3D11RasterizerState = NULL;

ID3D11DepthStencilView* gpID3D11DepthStencilView = NULL;

struct CBUFFER
{
	XMMATRIX WorldMatrix;
	XMMATRIX ViewMatrix;
	XMMATRIX ProjectionMatrix;
	
	XMVECTOR LightAmbient[3];
	XMVECTOR LightDiffuse[3];
	XMVECTOR LightSpecular[3];
	XMVECTOR LightPosition[3];

	XMVECTOR MaterialAmbient;
	XMVECTOR MaterialDiffuse;
	XMVECTOR MaterialSpecular;
	float MaterialShininess;

	unsigned int KeyPressed;
};

bool gbLight = false, gbAnimate = false;
bool isPerVertexLight = true, isPerPixelLight = false;

XMMATRIX gPerspectiveProjectionMatrix;

IDXGIFactory* pIDXGIFactory = NULL;
IDXGIAdapter* pIDXGIAdapter = NULL;

DXGI_ADAPTER_DESC dxgiAdapterDesc;
CHAR str[255];

float sphere_vertices[1146];
float Sphere_normals[1146];
float Sphere_texture[764];
unsigned short sphere_elements[2280];

int gNumVertices;
int gNumElements;

float LightAmbient [] = { 
							0.0f, 0.0f, 0.0f, 1.0f,
							0.0f, 0.0f, 0.0f, 1.0f,
							0.0f, 0.0f, 0.0f, 1.0f,
						};

float LightDiffuse [] = { 
							1.0f, 0.0f, 0.0f, 1.0f,
							0.0f, 1.0f, 0.0f, 1.0f,
							0.0f, 0.0f, 1.0f, 1.0f,
						};

float LightSpecular[]  = { 
							1.0f, 0.0f, 0.0f , 1.0f,
							0.0f, 1.0f, 0.0f , 1.0f,
							0.0f, 0.0f, 1.0f , 1.0f,
						};

float LightPosition[]  = { 
							0.0f, 0.0f, 0.0f, 1.0f,
							0.0f, 0.0f, 0.0f, 1.0f,
							0.0f, 0.0f, 0.0f, 1.0f
						};

float MaterialAmbient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
float MaterialDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
float MaterialSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
float MaterialShininess = 128.0f;

float sin_angle = 0.0f, cos_angle = 0.0f, radius = 2;

//Entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR szCmdName, int iCmdShow) {
	// function declaration
	HRESULT Initialize();
	void Uninitialize();
	void Display();

	//variable declaration
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("h_direct3D11");
	WNDCLASSEX wndClass;
	RECT monitorRC;

	//code 
	if (fopen_s(&gpFile, "windows.log", "w") != 0) {
		MessageBox(NULL, TEXT("Failed To Open File"), TEXT("ERROR"), MB_OK);
		exit(0);
	}

	wndClass.cbSize = sizeof(WNDCLASSEX);
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hIconSm = LoadIcon(hInstance, IDI_MYICON);
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.lpszClassName = szClassName;
	wndClass.lpszMenuName = NULL;
	wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	wndClass.lpfnWndProc = WndProc;

	RegisterClassEx(&wndClass);
	SystemParametersInfo(SPI_GETWORKAREA, 0, &monitorRC, 0);	
	hwnd = CreateWindowEx( WS_EX_APPWINDOW,
		szClassName,
		TEXT("Direct3D11 Window"),
		WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
		monitorRC.right / 2 - WIN_WIDTH/2 ,
		monitorRC.bottom / 2 - WIN_HEIGHT/2 ,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL
	);
	ghwnd = hwnd;

	SetForegroundWindow(hwnd);
	SetFocus(hwnd);
	ShowWindow(hwnd, iCmdShow);

	HRESULT hr;
	hr = Initialize();
	if (FAILED(hr))
	{
		fprintf(gpFile, "\ninitialize() failed.Exiting Now..\n");
		fclose(gpFile);
		DestroyWindow(ghwnd);
		hwnd = NULL;
	}

	// game loop
	while (gbDone == false) 
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 
		{
			if (msg.message == WM_QUIT) 
			{
				gbDone = true;
			}
			else 
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else 
		{
			Display();
			if (gbActiveWindow) 
			{
				if (gbEscapeKeyIsPressed == true)
				{
					gbDone = true;
				}
			}
		}
	}

	return ((int)(msg.wParam));
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	// function declaration
	void ToggleFullScreen();
	void Uninitialize();
	HRESULT Resize(int,int);

	HRESULT hr;

	// code
	switch (uMsg) {
		case WM_SETFOCUS:
			gbActiveWindow = true;
			break;
		case WM_KILLFOCUS:
			gbActiveWindow = false;
			break;
		case WM_ERASEBKGND:
			return 0L;
		case WM_SIZE:
			if (gpID3D11DeviceContext)
			{
				hr = Resize(LOWORD(lParam), HIWORD(lParam));
				if (FAILED(hr))
				{
					fprintf(gpFile, "\nResize() failed");
					fflush(gpFile);
					DestroyWindow(ghwnd);
					return hr;
				}
				else
				{
					fprintf(gpFile, "\nResize() successfull");
					fflush(gpFile);
				}
			}
			break;
		case WM_KEYDOWN:
			switch (wParam) {
			case VK_ESCAPE:
				if (gbEscapeKeyIsPressed == false)
				{
					gbEscapeKeyIsPressed = true;
				}
				//DestroyWindow(hwnd);
				break;
			case 0x46:
			case 0x66:
				ToggleFullScreen();
			}
			break;
		case WM_CHAR:
			switch (wParam)
			{
			case 'a':
				gbAnimate = true;
				break;
			case 'A':
				gbAnimate = false;
				break;
			case 'l':
				gbLight = !gbLight;
				break;
			case 'v':
			case 'V':
				isPerVertexLight = true;
				isPerPixelLight = false;
				break;
			case 'p':
			case 'P':
				isPerPixelLight = true;
				isPerVertexLight = false;
				break;
			}
			break;
		case WM_CLOSE:
			DestroyWindow(hwnd);
		case WM_DESTROY:
			Uninitialize();
			PostQuitMessage(0);
			break;
	}
	return (DefWindowProc(hwnd, uMsg, wParam, lParam));
}

void ToggleFullScreen() {

	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW) {
			if (GetWindowPlacement(ghwnd, &wpPrev) && 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)) {
				SetWindowLong(ghwnd, GWL_STYLE, (dwStyle & ~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd, HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED
				);
				ShowCursor(FALSE);
				gbFullScreen = true;
			}
		}
	}
	else {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED
		);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

HRESULT Initialize() {
	// function declaration
	HRESULT Resize(int, int);
	void getSphereVertexData(float spherePositionCoords[1146], float sphereNormalCoords[1146], float sphereTexCoords[764], unsigned short sphereElements[2280]);
	unsigned int getNumberOfSphereVertices(void);
	unsigned int getNumberOfSphereElements(void);
	void Uninitialize();

	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE, D3D_DRIVER_TYPE_WARP, D3D_DRIVER_TYPE_REFERENCE
	};

	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0; // default, lowest

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;	//based upon d3dFeatureLevel_required

	// code
	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]); // calculation size of array
	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;

	ZeroMemory((void*)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	dxgiSwapChainDesc.BufferCount = 1;
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	dxgiSwapChainDesc.OutputWindow = ghwnd;
	// MSAA - Multi Sampling Ani Alizing 
	dxgiSwapChainDesc.SampleDesc.Count = 1; // Max 8 2^8 sampling
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE;

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,								// adapter
			d3dDriverType,						// Driver Type
			NULL,								// Software
			createDeviceFlags,					// Flags
			&d3dFeatureLevel_required,			// Feature Levels
			numFeatureLevels,					// Num Feature Levels
			D3D11_SDK_VERSION,					// sdk verstion
			&dxgiSwapChainDesc,					// Swap Chain Desc
			&gpIDXGISwapChain,					// Swap chain
			&gpID3D11Device,					// Device
			&d3dFeatureLevel_acquired,			// Feature Level
			&gpID3D11DeviceContext				// device conext
		);

		if (SUCCEEDED(hr))
		{
			break;
		}
	}

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nD3D11CretaeDeviceAndSwapChain() Failed.");
		fflush(gpFile);
		//DestroyWindow(ghwnd);
		return hr;
	}
	else
	{
		fprintf(gpFile, "\nD3D11CreateDeviceAndSwapChain() Successded");
		fprintf(gpFile, "\nThe chosen driver is of");
		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf(gpFile, "\nHardware type.");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf(gpFile, "\nWarp type");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf(gpFile, "\nUnkown type");
		}

		fprintf(gpFile, "\nThe supported highest feature level is ");
		if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf(gpFile, "11.0");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf(gpFile, "10.1");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf(gpFile, "10.0");
		}
		else
		{
			fprintf(gpFile, "Unknown version");
		}
		fflush(gpFile);
	} // else closed

	hr = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&pIDXGIFactory);
	if (FAILED(hr))
	{
		fprintf(gpFile, "\nFailed to get pIDXGIFactory interface");
		fflush(gpFile);
		return hr;
	}

	if (pIDXGIFactory->EnumAdapters(1, &pIDXGIAdapter) == DXGI_ERROR_NOT_FOUND)
	{
		fprintf(gpFile, "\nFailed to get dxgi adapter");
		fflush(gpFile);
		return hr;
	}

	memset((void**)&dxgiAdapterDesc, 0, sizeof(DXGI_ADAPTER_DESC));

	hr = pIDXGIAdapter->GetDesc(&dxgiAdapterDesc);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\ndxgiAdapterDesc Failed to get");
		fflush(gpFile);
		return hr;
	}

	WideCharToMultiByte(CP_ACP, 0, dxgiAdapterDesc.Description, 255, str, 255, NULL, NULL);

	fprintf(gpFile, "\nGraphics card name is %s", str);
	fprintf(gpFile, "\nVideo Memory Size of graphics card is %d gb", int(ceil(dxgiAdapterDesc.DedicatedVideoMemory / 1024.0 / 1024.0 / 1024.0)));
	fflush(gpFile);

	/////////////////////////////////////////////// Per Vertex //////////////////////////////////////////////////////

	// 1: declare vertex shader source code

	const char* vertexShaderSourceCode_PerVertex =
		" cbuffer ConstantBuffer																																			\n " \
		" {																																									\n " \
		"		float4x4 worldMatrix;																																		\n " \
		"		float4x4 viewMatrix;																																		\n " \
		"		float4x4 projectionMatrix;																																	\n " \
		"		float4 lightAmbient[3];																																			\n " \
		"		float4 lightDiffuse[3];																																		\n " \
		"		float4 lightSpecular[3];																																		\n " \
		"		float4 lightPosition[3];																																		\n " \
		"		float4 materialAmbient;																																		\n " \
		"		float4 materialDiffuse;																																		\n " \
		"		float4 materialSpecular;																																	\n " \
		"		float materialShininess;																																	\n " \
		"		uint keyPressed;																																			\n " \
		" }																																									\n " \
		" struct vertex_out																																					\n " \
		" {																																									\n " \
		"		float4 position : SV_POSITION;																																\n " \
		"		float3 phoung_ads_light : COLOR;																															\n " \
		" };																																								\n " \
		" vertex_out main(float4 pos: POSITION, float3 normals : NORMAL)																									\n " \
		" {																																									\n " \
		"		struct vertex_out output;																																	\n " \
		"		if(keyPressed == 1)																																			\n " \
		"		{																																							\n " \
		"			float3 light_direction , reflection_vector , viewVector , ambient , diffuse , specular ; " \
		"			output.phoung_ads_light = float3(0.0, 0.0, 0.0 ); " \
		"			float4 eye_position = mul( worldMatrix , pos);																											\n " \
		"			eye_position = mul(viewMatrix, eye_position);																											\n " \
		"			float3 transformed_normals = normalize(  mul ( (float3x3) mul ( viewMatrix, worldMatrix) , (float3) normals) );											\n " \
		"			for ( int i = 0 ; i < 3 ; i++ )																															\n " \
		"			{																																						\n " \
		"				light_direction = normalize ( (float3) (lightPosition[i] - eye_position) );																			\n " \
		"				reflection_vector = reflect( -light_direction, transformed_normals);																				\n " \
		"				viewVector = normalize ( -eye_position.xyz);																										\n " \
		"				ambient = (float3) lightAmbient[i] * (float3) materialAmbient;																						\n " \
		"				diffuse = (float3) lightDiffuse[i] * (float3) materialDiffuse * max( dot( light_direction, transformed_normals), 0.0) ;								\n " \
		"				specular = (float3) lightSpecular[i] * (float3) materialSpecular * pow ( max ( dot( reflection_vector, viewVector) , 0.0), materialShininess);		\n " \
		"				output.phoung_ads_light = output.phoung_ads_light + ambient + diffuse + specular;																	\n " \
		"			}																																						\n " \
		"		}																																							\n " \
		"		else																																						\n " \
		"		{																																							\n " \
		"			output.phoung_ads_light = float3(1.0, 1.0, 1.0);																										\n " \
		"		}																																							\n " \
		"		float4 position = mul(worldMatrix, pos);																													\n " \
		"		position = mul ( viewMatrix, position);																														\n " \
		"		position = mul ( projectionMatrix, position); 																												\n " \
		"		output.position = position;																																	\n " \
		"		return (output);																																			\n " \
		" }																																									\n " ;

	ID3DBlob* pID3DBlob_Error = NULL;

	// 2. compile vertex shader source code and get compiler converted vertex
	// D3DCompile not only compile shaders but also compiles
	hr = D3DCompile(
		vertexShaderSourceCode_PerVertex,
		lstrlenA(vertexShaderSourceCode_PerVertex) + 1,
		"VS",
		NULL,									// pass MACRO OF D3D_SHADER_MACRO*
		D3D_COMPILE_STANDARD_FILE_INCLUDE,		// take your standard file which neeed, we are not passing 
		"main",									// entry point function name
		"vs_5_0",								// vertex shader feature level is vs_5_0
		0,										//	no deugging , profileing , optimization, row column measurement
		0,										// don't consider it as special effect (effect is advance version of shader)
		&pID3DBlob_VertexShaderCode_PerVertex,	// consider as shader
		&pID3DBlob_Error						// get errors if any
	);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fprintf(gpFile, "\nD3DCompile() Failed for vertex shader for per vertex light : %s.\n",(char *)pID3DBlob_Error->GetBufferPointer());
			fflush(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return hr;
		}
		else
		{
			// com error
			fprintf(gpFile, "\nSome COM error");
			fflush(gpFile);
		}
	}
	
	fprintf(gpFile, "\nD3DCompile() successfull for Vertex Shader for per vertex light");
	fflush(gpFile);
	
	hr = gpID3D11Device->CreateVertexShader(
		pID3DBlob_VertexShaderCode_PerVertex->GetBufferPointer(),
		pID3DBlob_VertexShaderCode_PerVertex->GetBufferSize(),
		NULL,											// ID3D11ClassInstance*
		&gpID3D11VertexShader_PerVertex
		);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreateVertexShader() Failed for per vertex light\n");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nID3D11Device::CreateVertexShader() Successfull for per vertex light");
	fflush(gpFile);

	

	const char* pixelShaderSourceCode_PerVertex =
		" struct vertex_out													\n " \
		" {																	\n " \
		"		float4 position : SV_POSITION;								\n " \
		"		float3 phoung_ads_light : COLOR;							\n " \
		" };																\n " \
		" float4 main(struct vertex_out input) : SV_TARGET					\n " \
		" {																	\n " \
		"		return (float4(input.phoung_ads_light, 1.0f));				\n " \
		" }																	\n ";

	ID3DBlob* pID3DBlob_PixelShaderCode_PerVertex = NULL;
	pID3DBlob_Error = NULL;
	hr = D3DCompile(pixelShaderSourceCode_PerVertex,
		lstrlenA(pixelShaderSourceCode_PerVertex) + 1,
		"PS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&pID3DBlob_PixelShaderCode_PerVertex,
		&pID3DBlob_Error
	);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fprintf(gpFile, "D3DCompile() Failed For Pixel Shader for per vertex light: %s \n", (char *)pID3DBlob_Error->GetBufferPointer());
			fflush(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return hr;
		}
		else
		{
			// com error
			fprintf(gpFile, "\nSome COM error");
			fflush(gpFile);
		}
	}

	fprintf(gpFile, "\nD3DCompile() Successful For Pixel Shader per vertex light");

	hr = gpID3D11Device->CreatePixelShader(
		pID3DBlob_PixelShaderCode_PerVertex->GetBufferPointer(),
		pID3DBlob_PixelShaderCode_PerVertex->GetBufferSize(),
		NULL,
		&gpID3D11PixelShader_PerVertex
		);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreatePixelShader() Failed for per vertex light.");
		fflush(gpFile);
		return hr;
	}

	if (pID3DBlob_PixelShaderCode_PerVertex)
	{
		pID3DBlob_PixelShaderCode_PerVertex->Release();
		pID3DBlob_PixelShaderCode_PerVertex = NULL;
	}

	if (pID3DBlob_Error)
	{
		pID3DBlob_Error->Release();
		pID3DBlob_Error = NULL;
	}

	fprintf(gpFile, "\nID3D11Device::CreatePixelShader() Successfull for per vertex light.");
	fflush(gpFile);
	
	/////////////////////////////////////////////// Per Pixel //////////////////////////////////////////////////////

	fprintf(gpFile, "\n\nPer Pixel Start Here \n\n");
	fflush(gpFile);

	// vertex shader
	const char* vertexShaderSourceCode_PerPixel =
		" cbuffer ConstantBuffer																							\n " \
		" {																													\n " \
		"		float4x4 worldMatrix;																						\n " \
		"		float4x4 viewMatrix;																						\n " \
		"		float4x4 projectionMatrix;																					\n " \
		"		float4 lightAmbient[3];																						\n " \
		"		float4 lightDiffuse[3];																						\n " \
		"		float4 lightSpecular[3];																					\n " \
		"		float4 lightPosition[3];																					\n " \
		"		float4 materialAmbient;																						\n " \
		"		float4 materialDiffuse;																						\n " \
		"		float4 materialSpecular;																					\n " \
		"		float materialShininess;																					\n " \
		"		uint keyPressed;																							\n " \
		" }																													\n " \
		" struct vertex_out																									\n " \
		" {																													\n " \
		"		float4 position : SV_POSITION;																				\n " \
		"		float3 transformed_normals : NORMAL0;																		\n " \
		"		float4 eye_position : NORMAL1;																				\n " \
		"		float3 viewer_vector : NORMAL2;																				\n " \
		" };																												\n " \
		" vertex_out main(float4 pos: POSITION, float3 normals: NORMAL)														\n " \
		" {																													\n " \
		"		struct vertex_out output;																					\n " \
		"		if( keyPressed == 1)																						\n " \
		"		{																											\n " \
		"			float4 eye_position = mul ( worldMatrix, pos); 															\n " \
		"			eye_position = mul(viewMatrix, eye_position);															\n " \
		"			output.transformed_normals =  mul ( (float3x3) mul ( viewMatrix, worldMatrix) , (float3) normals);		\n " \
		"			output.eye_position = eye_position ;																	\n " \
		"			output.viewer_vector = -eye_position.xyz;																\n " \
		"		}																											\n " \
		"		float4 position = mul ( worldMatrix, pos);																	\n " \
		"		position = mul ( viewMatrix, position);																		\n " \
		"		position = mul ( projectionMatrix, position);																\n " \
		"		output.position = position;																					\n " \
		"		return output;																								\n " \
		" }																													\n " \
		"																													\n " ;
		
	pID3DBlob_Error = NULL;
	hr = D3DCompile(vertexShaderSourceCode_PerPixel,
		lstrlenA(vertexShaderSourceCode_PerPixel) + 1,
		"VS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"vs_5_0",
		0,
		0,
		&pID3DBlob_VertexShaderCode_PerPixel,
		&pID3DBlob_Error
	);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fprintf(gpFile, "D3DCompile() Failed For vertex Shader per pixel light: %s \n", (char*)pID3DBlob_Error->GetBufferPointer());
			fflush(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return hr;
		}
		else
		{
			// com error
			fprintf(gpFile, "\nSome COM error");
			fflush(gpFile);
		}
	}

	fprintf(gpFile, "\nD3DCompile() Successful For vertex Shader for per pixel light");

	hr = gpID3D11Device->CreateVertexShader(
		pID3DBlob_VertexShaderCode_PerPixel->GetBufferPointer(),
		pID3DBlob_VertexShaderCode_PerPixel->GetBufferSize(),
		NULL,
		&gpID3D11VertexShader_PerPixel
	);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreateVertexShader() Failed for per pixel light.");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nID3D11Device::CreateVertexShader() Successfull for per pixel light.");
	fflush(gpFile);
	
	if (pID3DBlob_Error)
	{
		pID3DBlob_Error->Release();
		pID3DBlob_Error = NULL;
	}

	// Pixel shader

	const char* pixelShaderSourceCode_PerPixel =
		" cbuffer ConstantBuffer																																				\n " \
		" {																																										\n " \
		"		float4x4 worldMatrix;																																			\n " \
		"		float4x4 viewMatrix;																																			\n " \
		"		float4x4 projectionMatrix;																																		\n " \
		"		float4 lightAmbient[3];																																			\n " \
		"		float4 lightDiffuse[3];																																			\n " \
		"		float4 lightSpecular[3];																																			\n " \
		"		float4 lightPosition[3];																																			\n " \
		"		float4 materialAmbient;																																			\n " \
		"		float4 materialDiffuse;																																			\n " \
		"		float4 materialSpecular;																																		\n " \
		"		float materialShininess;																																		\n " \
		"		uint keyPressed;																																				\n " \
		" }																																										\n " \
		" struct vertex_out																																						\n " \
		" {																																										\n " \
		"		float4 position : SV_POSITION;																																	\n " \
		"		float3 transformed_normals : NORMAL0;																															\n " \
		"		float4 eye_position : NORMAL1;																																	\n " \
		"		float3 viewer_vector : NORMAL2;																																	\n " \
		" };																																									\n " \
		" float4 main(struct vertex_out input) : SV_TARGET																														\n " \
		" {																																										\n " \
		"		float3 phoung_ads_light ;																																		\n " \
		"		float3 ambient , diffuse , reflection_vector , specular, normalized_light_direction , light_direction;															\n " \
		"		if(keyPressed == 1)																																				\n " \
		"		{																																								\n " \
		"			float3 normalized_transformed_normals = normalize ( input.transformed_normals);																				\n " \
		"			float3 normalized_viewer_vector = normalize ( input.viewer_vector);																							\n " \
		"			for ( int i = 0 ; i < 3; i++ )																																\n " \
		"			{																																							\n " \
		"				light_direction = (float3) lightPosition[i] - input.eye_position.xyz;																							\n " \
		"				normalized_light_direction = normalize ( light_direction );																								\n " \
		"				ambient = (float3) lightAmbient[i] * (float3) materialAmbient;																							\n " \
		"				diffuse = (float3) lightDiffuse[i] * (float3) materialDiffuse * max ( dot( normalized_light_direction, normalized_transformed_normals), 0.0);			\n " \
		"				reflection_vector = reflect ( -normalized_light_direction, normalized_transformed_normals);																\n " \
		"				specular = (float3) (lightSpecular[i] * materialSpecular) * pow ( max ( dot ( reflection_vector, normalized_viewer_vector), 0.0), materialShininess);	\n " \
		"				phoung_ads_light = phoung_ads_light + ambient + diffuse + specular;																						\n " \
		"			}																																							\n " \
		"		}																																								\n " \
		"		else																																							\n " \
		"		{																																								\n " \
		"			phoung_ads_light = float3( 1.0, 1.0, 1.0);																													\n " \
		"		}																																								\n " \
		"		float4 color = float4(phoung_ads_light, 1.0);																													\n " \
		"		return color;																																					\n " \
		" }																																										\n " ;

	ID3DBlob* pID3DBlob_PixelShaderCode_PerPixel = NULL;
	pID3DBlob_Error = NULL;
	hr = D3DCompile(pixelShaderSourceCode_PerPixel,
		lstrlenA(pixelShaderSourceCode_PerPixel) + 1,
		"PS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&pID3DBlob_PixelShaderCode_PerPixel,
		&pID3DBlob_Error
	);

	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fprintf(gpFile, "\nD3DCompile() Failed For Pixel Shader for per pixel light : %s \n", (char*)pID3DBlob_Error->GetBufferPointer());
			fflush(gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return hr;
		}
		else
		{
			// com error
			fprintf(gpFile, "\nSome COM error");
			fflush(gpFile);
		}
	}

	fprintf(gpFile, "\nD3DCompile() Successful For Pixel Shader for per pixel light");

	hr = gpID3D11Device->CreatePixelShader(
		pID3DBlob_PixelShaderCode_PerPixel->GetBufferPointer(),
		pID3DBlob_PixelShaderCode_PerPixel->GetBufferSize(),
		NULL,
		&gpID3D11PixelShader_PerPixel
	);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreatePixelShader() Failed for per pixel light.");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nID3D11Device::CreatePixelShader() Successfull for per pixel light.");
	fflush(gpFile);
	
	if (pID3DBlob_PixelShaderCode_PerPixel)
	{
		pID3DBlob_PixelShaderCode_PerPixel->Release();
		pID3DBlob_PixelShaderCode_PerPixel = NULL;
	}

	if (pID3DBlob_Error)
	{
		pID3DBlob_Error->Release();
		pID3DBlob_Error = NULL;
	}

	////////////////////////////////////////////////// End OF Shaders ///////////////////////////////////////////////////////

	// create input layout globally
	
	//ZeroMemory((void*)&inputElementDesc, sizeof(D3D11_INPUT_ELEMENT_DESC));

	inputElementDesc[0].SemanticName = "POSITION";
	inputElementDesc[0].SemanticIndex = 0;
	inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[0].InputSlot = 0;
	inputElementDesc[0].AlignedByteOffset = 0;
	inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[0].InstanceDataStepRate = 0;

	// 2nd

	inputElementDesc[1].SemanticName = "NORMAL";
	inputElementDesc[1].SemanticIndex = 0;
	inputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	inputElementDesc[1].InputSlot = 1;
	inputElementDesc[1].AlignedByteOffset = 0;
	inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	inputElementDesc[1].InstanceDataStepRate = 0;
		
	/////////////////////////////////////////////// SPHERE ////////////////////////////////////////////////////

	getSphereVertexData(sphere_vertices, Sphere_normals, Sphere_texture, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	D3D11_BUFFER_DESC bufferDesc;
	ZeroMemory((void*)&bufferDesc, sizeof(D3D11_BUFFER_DESC));
	
	bufferDesc.ByteWidth = gNumVertices * 3 * sizeof(float);
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11Buffer_Sphere_VertexBuffer_position);
	
	if (FAILED(hr))
	{
		fprintf(gpFile, "\n CreateBuffer() for rectangle position failed");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\n CreateBuffer() successfull for rectangle position");
	fflush(gpFile);

	ZeroMemory((void*)&bufferDesc, sizeof(D3D11_BUFFER_DESC));

	// cube normals
	bufferDesc.ByteWidth = 3 * gNumVertices * sizeof(float);
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11Buffer_Sphere_VertexBuffer_normals);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\n CreateBuffer() for rectangle normals failed");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\n CreateBuffer() for rectangle normals Successfull");
	fflush(gpFile);
	
	D3D11_MAPPED_SUBRESOURCE mappedSubresource;

	// pass rectangle vertices and color to gpu
	ZeroMemory((void*)&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	gpID3D11DeviceContext->Map(gpID3D11Buffer_Sphere_VertexBuffer_position, NULL, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_vertices, gNumVertices * 3 * sizeof(float));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_Sphere_VertexBuffer_position, NULL);

	// cube normals
	ZeroMemory((void*)&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	gpID3D11DeviceContext->Map(gpID3D11Buffer_Sphere_VertexBuffer_normals, NULL, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, Sphere_normals, gNumVertices * 3 * sizeof(float));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_Sphere_VertexBuffer_normals, NULL);

	// index buffer

	ZeroMemory((void*)&bufferDesc, sizeof(D3D11_BUFFER_DESC));

	bufferDesc.ByteWidth = gNumElements * sizeof(short);
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;

	hr = gpID3D11Device->CreateBuffer(&bufferDesc, NULL, &gpID3D11Buffer_Sphere_IndexBuffer);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\n CreateBuffer() for sphere index buffer failed");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\n CreateBuffer() for sphere index buffer Successfull");
	fflush(gpFile);

	ZeroMemory((void*)&mappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	gpID3D11DeviceContext->Map(gpID3D11Buffer_Sphere_IndexBuffer, NULL, D3D11_MAP_WRITE_DISCARD, 0, &mappedSubresource);
	memcpy(mappedSubresource.pData, sphere_elements, gNumElements * sizeof(short));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_Sphere_IndexBuffer, NULL);

	// define and set constant buffer
	// uniform buffer
	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
	ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(D3D11_BUFFER_DESC));
	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	
	hr = gpID3D11Device->CreateBuffer(&bufferDesc_ConstantBuffer, nullptr, &gpID3D11Buffer_ConstantBuffer);
	
	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreateBuffer() Failed For Constant Buffer.");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nID3D11Device::CreateBuffer() Successfull For Constant Buffer.");
	fflush(gpFile);
	

	// CREATE AND SET RASTERIZER STATE

	D3D11_RASTERIZER_DESC d3d11_rasterizer_desc;

	ZeroMemory((void*)&d3d11_rasterizer_desc, sizeof(D3D11_RASTERIZER_DESC));

	d3d11_rasterizer_desc.AntialiasedLineEnable = FALSE;
	d3d11_rasterizer_desc.CullMode = D3D11_CULL_NONE;
	d3d11_rasterizer_desc.DepthBias = 0;
	d3d11_rasterizer_desc.DepthBiasClamp = 0.0f;
	d3d11_rasterizer_desc.DepthClipEnable = TRUE;
	d3d11_rasterizer_desc.FillMode = D3D11_FILL_SOLID;
	d3d11_rasterizer_desc.FrontCounterClockwise = FALSE;
	d3d11_rasterizer_desc.MultisampleEnable = FALSE;
	d3d11_rasterizer_desc.ScissorEnable = FALSE;
	d3d11_rasterizer_desc.SlopeScaledDepthBias = 0.0f;

	hr = gpID3D11Device->CreateRasterizerState(&d3d11_rasterizer_desc, &gpID3D11RasterizerState);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\n CreateRasterizerState() Failed");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\n CreateRasterizerState() sucsess");
	fflush(gpFile);

	gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState);

	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 0.0f;
	gClearColor[3] = 1.0f;

	gPerspectiveProjectionMatrix = XMMatrixIdentity();

	hr = Resize(WIN_WIDTH, WIN_HEIGHT);
	if (FAILED(hr))
	{
		fprintf(gpFile, "\nResize() Failed");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\nResize successfull");
	fflush(gpFile);

	return S_OK;
}

HRESULT Resize(int width, int height) 
{
	HRESULT hr = S_OK;

	if (height == 0)
	{
		height = 1;
	}

	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	gpIDXGISwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

	ID3D11Texture2D* pID3D11Texture2D_BackBuffer;
	gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*) &pID3D11Texture2D_BackBuffer);

	// again get render target view from d3d11 using above back buffer
	hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL, &gpID3D11RenderTargetView);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\nID3D11Device::CreateTargetView() Failed.");
		fflush(gpFile);
		return hr;
	}
	else
	{
		fprintf(gpFile, "\nID3D11Device::CreateTargetView() Successded.");
		fflush(gpFile);
	}

	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	// just like RTV ,  DSV also need texture buffer

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	// create and intialize
	D3D11_TEXTURE2D_DESC d3d11_texture2D_desc;

	ZeroMemory((void*)&d3d11_texture2D_desc, sizeof(D3D11_TEXTURE2D_DESC));

	d3d11_texture2D_desc.Width = (UINT) width;
	d3d11_texture2D_desc.Height = (UINT) height;
	d3d11_texture2D_desc.Format = DXGI_FORMAT_D32_FLOAT;
	d3d11_texture2D_desc.Usage = D3D11_USAGE_DEFAULT;
	d3d11_texture2D_desc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	d3d11_texture2D_desc.SampleDesc.Count = 1;	// this can be 1-4 as per needed capacity
	d3d11_texture2D_desc.SampleDesc.Quality = 0; // default
	d3d11_texture2D_desc.ArraySize = 1;
	d3d11_texture2D_desc.MipLevels = 1; // default;
	d3d11_texture2D_desc.CPUAccessFlags = 0; // default
	d3d11_texture2D_desc.MiscFlags = 0;

	// create texture2D as depth buffer
	ID3D11Texture2D* pID3D11Texture2D_depthbuffer;

	hr = gpID3D11Device->CreateTexture2D(&d3d11_texture2D_desc, NULL, &pID3D11Texture2D_depthbuffer);
	if (FAILED(hr))
	{
		fprintf(gpFile, "\n CreateTexture2D failed for depth");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\n CreateTexture2D successfully for depth");
	fflush(gpFile);

	// intialize depth stencil view desc
	D3D11_DEPTH_STENCIL_VIEW_DESC d3d11_depth_stencil_view_desc;

	ZeroMemory((void*)&d3d11_depth_stencil_view_desc, sizeof(d3d11_depth_stencil_view_desc));

	d3d11_depth_stencil_view_desc.Format = DXGI_FORMAT_D32_FLOAT;
	d3d11_depth_stencil_view_desc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS; // TEXTURE2D MS - multi sample

	// create dsv
	hr = gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D_depthbuffer, &d3d11_depth_stencil_view_desc, &gpID3D11DepthStencilView);

	if (FAILED(hr))
	{
		fprintf(gpFile, "\n CretaeDepthstencilView() failed");
		fflush(gpFile);
		return hr;
	}

	fprintf(gpFile, "\n CretaeDepthstencilView() successfully");
	fflush(gpFile);

	// set render target view as render target
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, gpID3D11DepthStencilView);

	// set viewport target view
	D3D11_VIEWPORT d3dViewPort;
	
	ZeroMemory((void*)&d3dViewPort, sizeof(D3D11_VIEWPORT));

	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f; // glClearDepth(1.0)
	
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);
	
	// set orthographic matrix
	gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f), (float)width / (float)height, 0.1f, 100.0f);

	return hr;
}

void Display()
{
	// function declaration
	void Uninitialize();

	// variable declarations
	HRESULT hr;
	static float angle_red = 360.0f, angle_blue = 0.0f, angle_green = 360.0f;

	// Code
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);
	gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, D3D11_CLEAR_DEPTH);

	////////////////////////////////////////////////////////////// SPHERE ////////////////////////////////////////////////////////////
	
	if (isPerVertexLight)
	{
		hr = gpID3D11Device->CreateInputLayout(
			inputElementDesc,
			_ARRAYSIZE(inputElementDesc),
			pID3DBlob_VertexShaderCode_PerVertex->GetBufferPointer(),
			pID3DBlob_VertexShaderCode_PerVertex->GetBufferSize(),
			&gpID3D11InputLayout
		);

		if (FAILED(hr))
		{
			fprintf(gpFile, "\nID3D11Device::CreateInputLayout() Failed for per vertex light");
			fflush(gpFile);

			pID3DBlob_VertexShaderCode_PerVertex->Release();
			pID3DBlob_VertexShaderCode_PerVertex = NULL;

			Uninitialize();
		}

		fprintf(gpFile, "\nUID3D11Device::CreateInputLayout() Successfull for per vertex light");
		fflush(gpFile);

		gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);
		gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader_PerVertex, 0, 0);
		gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader_PerVertex, 0, 0);
		gpID3D11DeviceContext->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);
	}
	else
	{
		hr = gpID3D11Device->CreateInputLayout(
			inputElementDesc,
			_ARRAYSIZE(inputElementDesc),
			pID3DBlob_VertexShaderCode_PerPixel->GetBufferPointer(),
			pID3DBlob_VertexShaderCode_PerPixel->GetBufferSize(),
			&gpID3D11InputLayout
		);

		if (FAILED(hr))
		{
			fprintf(gpFile, "\nID3D11Device::CreateInputLayout() Failed for per pixel light");
			fflush(gpFile);

			pID3DBlob_VertexShaderCode_PerPixel->Release();
			pID3DBlob_VertexShaderCode_PerPixel = NULL;

			Uninitialize();
		}

		fprintf(gpFile, "\nUID3D11Device::CreateInputLayout() Successfull for per pixel light");
		fflush(gpFile);

		gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);
		gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader_PerPixel, 0, 0);
		gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader_PerPixel, 0, 0);
		gpID3D11DeviceContext->VSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);
		gpID3D11DeviceContext->PSSetConstantBuffers(0, 1, &gpID3D11Buffer_ConstantBuffer);
	}

	UINT stride = sizeof(float) * 3;
	UINT offset = 0;
	gpID3D11DeviceContext->IASetVertexBuffers(0, 1, &gpID3D11Buffer_Sphere_VertexBuffer_position, &stride, &offset);

	stride = sizeof(float) * 3;
	offset = 0;
	gpID3D11DeviceContext->IASetVertexBuffers(1, 1, &gpID3D11Buffer_Sphere_VertexBuffer_normals, &stride, &offset);

	gpID3D11DeviceContext->IASetIndexBuffer(gpID3D11Buffer_Sphere_IndexBuffer, DXGI_FORMAT_R16_UINT, 0);

	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	
	XMMATRIX worldMatrix = XMMatrixTranslation(0.0f, 0.0f, 1.5f);
	XMMATRIX viewMatrix = XMMatrixIdentity();

	CBUFFER constantBuffer;

	ZeroMemory((void*)&constantBuffer, sizeof(CBUFFER));

	if (gbLight == true)
	{
		sin_angle = radius * sin(XMConvertToRadians(angle_red));
		cos_angle = radius * cos(XMConvertToRadians(angle_red));

		LightPosition[1] = sin_angle;
		LightPosition[2] = cos_angle;

		sin_angle = radius * sin(XMConvertToRadians(angle_blue));
		cos_angle = radius * cos(XMConvertToRadians(angle_blue));

		LightPosition[4] = sin_angle;
		LightPosition[6] = cos_angle;

		sin_angle = radius * sin(XMConvertToRadians(angle_green));
		cos_angle = radius * cos(XMConvertToRadians(angle_green));

		LightPosition[8] = sin_angle;
		LightPosition[9] = cos_angle;

		constantBuffer.LightAmbient[0] = XMVectorSet(LightAmbient[0], LightAmbient[1], LightAmbient[2], LightAmbient[3]);
		constantBuffer.LightDiffuse[0] = XMVectorSet(LightDiffuse[0], LightDiffuse[1], LightDiffuse[2], LightDiffuse[3]);
		constantBuffer.LightSpecular[0] = XMVectorSet(LightSpecular[0], LightSpecular[1], LightSpecular[2], LightSpecular[3]);
		constantBuffer.LightPosition[0] = XMVectorSet(LightPosition[0], LightPosition[1], LightPosition[2], LightPosition[3]);

		constantBuffer.LightAmbient[1] = XMVectorSet(  LightAmbient[4],  LightAmbient[5],  LightAmbient[6],  LightAmbient[7]);
		constantBuffer.LightDiffuse[1] = XMVectorSet(  LightDiffuse[4],  LightDiffuse[5],  LightDiffuse[6],  LightDiffuse[7]);
		constantBuffer.LightSpecular[1] = XMVectorSet(LightSpecular[4], LightSpecular[5], LightSpecular[6], LightSpecular[7]);
		constantBuffer.LightPosition[1] = XMVectorSet(LightPosition[4], LightPosition[5], LightPosition[6], LightPosition[7]);

		constantBuffer.LightAmbient[2] = XMVectorSet(  LightAmbient[8],  LightAmbient[9],  LightAmbient[10],  LightAmbient[11]);
		constantBuffer.LightDiffuse[2] = XMVectorSet(  LightDiffuse[8],  LightDiffuse[9],  LightDiffuse[10],  LightDiffuse[11]);
		constantBuffer.LightSpecular[2] = XMVectorSet(LightSpecular[8], LightSpecular[9], LightSpecular[10], LightSpecular[11]);
		constantBuffer.LightPosition[2] = XMVectorSet(LightPosition[8], LightPosition[9], LightPosition[10], LightPosition[11]);

		constantBuffer.MaterialAmbient = XMVectorSet(MaterialAmbient[0], MaterialAmbient[1], MaterialAmbient[2], MaterialAmbient[3]);
		constantBuffer.MaterialDiffuse = XMVectorSet(MaterialDiffuse[0], MaterialDiffuse[1], MaterialDiffuse[2], MaterialDiffuse[3]);
		constantBuffer.MaterialSpecular = XMVectorSet(MaterialSpecular[0], MaterialSpecular[1], MaterialSpecular[2], MaterialSpecular[3]);
		constantBuffer.MaterialShininess = MaterialShininess;
		
		constantBuffer.KeyPressed = 1;
	}
	else
	{
		constantBuffer.KeyPressed = 0;
	}

	constantBuffer.WorldMatrix = worldMatrix;
	constantBuffer.ViewMatrix = viewMatrix;
	constantBuffer.ProjectionMatrix = gPerspectiveProjectionMatrix;
	gpID3D11DeviceContext->UpdateSubresource(gpID3D11Buffer_ConstantBuffer, 0, NULL, &constantBuffer, 0, 0);
	
	gpID3D11DeviceContext->DrawIndexed(gNumElements, 0, 0);

	if (gbAnimate == true)
	{
		angle_red = angle_red - 0.05;
		angle_blue = angle_blue + 0.05;
		angle_green = angle_green - 0.05;
		if (angle_red <= 0)
		{
			angle_red = 360;
		}
		if (angle_blue >= 360)
		{
			angle_blue = 1.5;
		}
		if (angle_green <= 0)
		{
			angle_green = 360;
		}
	}

	gpIDXGISwapChain->Present(0, 0);
}

void Uninitialize() {

	//code
	if (pID3DBlob_VertexShaderCode_PerPixel)
	{
		pID3DBlob_VertexShaderCode_PerPixel->Release();
		pID3DBlob_VertexShaderCode_PerPixel = NULL;
	}

	if (pID3DBlob_VertexShaderCode_PerVertex)
	{
		pID3DBlob_VertexShaderCode_PerVertex->Release();
		pID3DBlob_VertexShaderCode_PerVertex = NULL;
	}
	if (gpID3D11Buffer_ConstantBuffer)
	{
		gpID3D11Buffer_ConstantBuffer->Release();
		gpID3D11Buffer_ConstantBuffer = NULL;
	}

	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}

	if (gpID3D11Buffer_Sphere_IndexBuffer)
	{
		gpID3D11Buffer_Sphere_IndexBuffer->Release();
		gpID3D11Buffer_Sphere_IndexBuffer = NULL;
	}

	if (gpID3D11Buffer_Sphere_VertexBuffer_normals)
	{
		gpID3D11Buffer_Sphere_VertexBuffer_normals->Release();
		gpID3D11Buffer_Sphere_VertexBuffer_normals = NULL;
	}

	if (gpID3D11Buffer_Sphere_VertexBuffer_position)
	{
		gpID3D11Buffer_Sphere_VertexBuffer_position->Release();
		gpID3D11Buffer_Sphere_VertexBuffer_position = NULL;
	}

	if (gpID3D11PixelShader_PerPixel)
	{
		gpID3D11PixelShader_PerPixel->Release();
		gpID3D11PixelShader_PerPixel = NULL;
	}

	if (gpID3D11VertexShader_PerPixel)
	{
		gpID3D11VertexShader_PerPixel->Release();
		gpID3D11VertexShader_PerPixel = NULL;
	}

	if (gpID3D11PixelShader_PerVertex)
	{
		gpID3D11PixelShader_PerVertex->Release();
		gpID3D11PixelShader_PerVertex = NULL;
	}

	if (gpID3D11VertexShader_PerVertex)
	{
		gpID3D11VertexShader_PerVertex->Release();
		gpID3D11VertexShader_PerVertex = NULL;
	}

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	if (pIDXGIAdapter)
	{
		pIDXGIAdapter->Release();
		pIDXGIAdapter = NULL;
	}

	if (pIDXGIFactory)
	{
		pIDXGIFactory->Release();
		pIDXGIFactory = NULL;
	}

	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}

	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}

	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}

	// if fullscreen, then come out of full screen
	if (gbFullScreen) {
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
	}

	if (gpFile) {
		fprintf(gpFile, "\nClosing File Successfully");
		fclose(gpFile);
	}

}

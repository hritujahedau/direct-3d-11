
#include <stdio.h>
#include <d3d11.h> // <gl/gl.h>
#include <math.h>

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxgi.lib") // direct x graphics infrastructure like wgl

int main(void)
{
	// variable declarations
	IDXGIFactory* pIDXGIFactory = NULL;
	IDXGIAdapter* pIDXGIAdapter = NULL;

	DXGI_ADAPTER_DESC dxgiAdapterDesc;
	HRESULT hr;
	CHAR str[255];

	// code
	hr = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&pIDXGIFactory);
	if (FAILED(hr))
	{
		printf("\nFailed to get pIDXGIFactory interface");
		goto CLEANUP;
	}

	if (pIDXGIFactory->EnumAdapters(1, &pIDXGIAdapter) == DXGI_ERROR_NOT_FOUND)
	{
		printf("\nFailed to get dxgi adapter");
		goto CLEANUP;
	}

	memset((void**)&dxgiAdapterDesc, 0, sizeof(DXGI_ADAPTER_DESC));

	hr = pIDXGIAdapter->GetDesc(&dxgiAdapterDesc);

	if (FAILED(hr))
	{
		printf("\ndxgiAdapterDesc failed to get");
		goto CLEANUP;
	}

	WideCharToMultiByte(CP_ACP, 0, dxgiAdapterDesc.Description, 255, str, 255, NULL, NULL);

	printf("\nGraphics card name is %s", str);
	printf("\nVideo Memory Size of graphics card is %I64d bytes", (__int64)dxgiAdapterDesc.DedicatedVideoMemory);
	printf("\nVideo Memory Size of graphics card is %d gb\n", int(ceil(dxgiAdapterDesc.DedicatedVideoMemory/1024.0/1024.0/1024.0)) );

CLEANUP:

	if (pIDXGIAdapter)
	{
		pIDXGIAdapter->Release();
		pIDXGIAdapter = NULL;
	}

	if (pIDXGIFactory)
	{
		pIDXGIFactory->Release();
		pIDXGIFactory = NULL;
	}

	return 0;
}
